package net.foxgenesis.ashlynebot;
import static net.foxgenesis.ashlynebot.Bot.pushbullet;
import static net.foxgenesis.util.helper.DiscordHelper.ERROR;
import static net.foxgenesis.util.helper.DiscordHelper.INFO;
import static net.foxgenesis.util.helper.DiscordHelper.outputMessage;
import static net.foxgenesis.util.helper.DiscordHelper.outputMessageDelete;

import java.io.IOException;
import java.util.Date;
import java.util.EnumSet;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.github.sheigutn.pushbullet.items.push.sendable.defaults.SendableNotePush;

import net.foxgenesis.ashlynebot.events.BotReadyEvent;
import net.foxgenesis.discord.guild.GuildDataManager;
import net.foxgenesis.util.helper.DiscordHelper;
import sx.blah.discord.api.IDiscordClient;
import sx.blah.discord.api.events.EventSubscriber;
import sx.blah.discord.handle.impl.events.ReadyEvent;
import sx.blah.discord.handle.impl.events.guild.GuildCreateEvent;
import sx.blah.discord.handle.impl.events.guild.GuildLeaveEvent;
import sx.blah.discord.handle.impl.events.guild.channel.message.MentionEvent;
import sx.blah.discord.handle.impl.events.guild.channel.message.MessageReceivedEvent;
import sx.blah.discord.handle.impl.events.shard.DisconnectedEvent;
import sx.blah.discord.handle.obj.ActivityType;
import sx.blah.discord.handle.obj.IEmoji;
import sx.blah.discord.handle.obj.IMessage;
import sx.blah.discord.handle.obj.IUser;
import sx.blah.discord.handle.obj.Permissions;
import sx.blah.discord.handle.obj.StatusType;

public class AnnotationListener {
	public static final int NEEDED_PERMS = 0x00000400 | 0x00000040 | 0x00000800 | 0x00004000 | 0x00002000 | 0x00008000 | 0x04000000;
	public static final EnumSet<Permissions> NEEDED_PERMS_SET = Permissions.getAllowedPermissionsForNumber(NEEDED_PERMS);
	private static boolean ready = false;

	private static final Logger LOGGER = LoggerFactory.getLogger("AshlyneBot"); //$NON-NLS-1$

	@SuppressWarnings("static-method")
	@EventSubscriber
	public void onReadyEvent(ReadyEvent event) throws Exception {
		LOGGER.info("ReadyEvent recieved. Loading bot."); //$NON-NLS-1$

		IDiscordClient client = event.getClient();

		// set status to show the bot is loading
		client.changePresence(StatusType.DND, ActivityType.PLAYING, "Loading..."); //$NON-NLS-1$

		// register helper class
		client.getDispatcher().registerListener(DiscordHelper.class);

		// give the database manager the current list of guilds
		client.getGuilds().stream().forEach(((GuildDataManager) Bot.getData())::addGuild);

		// Initialize
		LOGGER.info("Initializing content..."); //$NON-NLS-1$
		PlusCommands.init();
		TextFilters.init();
		BotListeners.init();
		TimedEvents.init();

		// Signal database manager to read guild data
		LOGGER.info("Loading guild data..."); //$NON-NLS-1$
		((GuildDataManager) Bot.getData()).getData();


		// Initialize complete. Fire ready events and set status
		BotReadyEvent e = new BotReadyEvent();
		client.getDispatcher().dispatch(e);
		BotListeners.ready(e);
		ready = true;

		// Post initialization 
		LOGGER.info("Startup complete"); //$NON-NLS-1$
		System.out.println();	// Add spacing in logs
		System.out.println();
		client.changePresence(StatusType.ONLINE, ActivityType.PLAYING, "++help | foxgenesis.net/discord"); //$NON-NLS-1$

		// Notify author
		//pushbullet().push(new SendableNotePush("Ashlyne Bot","Startup completed")); //$NON-NLS-1$ //$NON-NLS-2$
	}

	@SuppressWarnings("static-method")
	@EventSubscriber
	public void onMessage(MessageReceivedEvent e) {
		if(e.getChannel().isPrivate() || !ready)
			return;
		try {
			IMessage message = e.getMessage();
			String content = message.getFormattedContent();
			if(!message.getAuthor().isBot() && content.length() > 0 && content.charAt(0) == '+') {
				try {
					int pos = content.indexOf(' ');
					String command = pos != -1 ? content.substring(1,pos) : content.substring(1);
					PlusCommands.handle(message, command.toLowerCase().trim(), pos != -1 ? content.substring(pos + 1).trim() : null);
				} catch(Exception e1) {
					e1.printStackTrace();
					String msg = String.format(InfoMessages.getString("AnnotationListener.PBErrOnMessage"),e.getGuild(),e.getMessage().getFormattedContent()); //$NON-NLS-1$
					pushbullet().push(new SendableNotePush(msg,DiscordHelper.dump(e1)));
					LOGGER.error(msg, e1);
					outputMessageDelete(e.getMessage(),InfoMessages.getString("AnnotationListener.ErrMsgOnMessage"),ERROR); //$NON-NLS-1$
					//DiscordHelper.dumpErrorToChannel(e1, e.getAuthor().getOrCreatePMChannel());
				}
			} else TextFilters.handle(message);
		} catch(Exception e1) {
			e1.printStackTrace();
			String msg = String.format(InfoMessages.getString("AnnotationListener.PBErrOnMessage"),e.getGuild(),e.getMessage().getFormattedContent()); //$NON-NLS-1$
			pushbullet().push(new SendableNotePush(msg,DiscordHelper.dump(e1)));
			LOGGER.error(msg, e1);
			//DiscordHelper.dumpErrorToChannel(e1, e.getAuthor().getOrCreatePMChannel());
		}
	}

	@SuppressWarnings("static-method")
	@EventSubscriber
	public void newGuild(GuildCreateEvent e) {
		((GuildDataManager) Bot.getData()).addGuild(e.getGuild());
	}

	@SuppressWarnings("static-method")
	@EventSubscriber
	public void removeGuild(GuildLeaveEvent e) {
		((GuildDataManager) Bot.getData()).removeGuild(e.getGuild());
	}

	@SuppressWarnings("static-method")
	@EventSubscriber
	public void onMessageReceivedEvent(MentionEvent event) { 
		IMessage message = event.getMessage();
		IUser author = event.getAuthor();
		if(message.mentionsEveryone() || message.mentionsHere() || !message.getMentions().contains(event.getClient().getOurUser()) || author.equals(event.getClient().getOurUser()))
			return;

		if(author.equals(event.getClient().getApplicationOwner())) {
			if(message.getFormattedContent().contains("shutdown")) { //$NON-NLS-1$
				LOGGER.warn("**** SHUTDOWN REQUESTED VIA COMMAND ****"); //$NON-NLS-1$
				outputMessage(event.getMessage(),InfoMessages.getString("AnnotationListener.7"),INFO); //$NON-NLS-1$
				TimedEvents.unload();
				Bot.getClient().logout();
				System.exit(0);
				return;
			} else if(message.getFormattedContent().contains("restart")) { //$NON-NLS-1$
				LOGGER.warn("**** RESTART REQUESTED VIA COMMAND ****"); //$NON-NLS-1$
				restart();
			}
		}
		IEmoji em = event.getGuild().getEmojiByName("RTSD"); //$NON-NLS-1$
		event.getMessage().reply((em != null? em : "") + "Fuck mlev, am I right?"); //$NON-NLS-1$ //$NON-NLS-2$
		if(em != null)
			DiscordHelper.addCustomEmojiReaction(event.getMessage(), "RTSD"); //$NON-NLS-1$
	}

	@SuppressWarnings("static-method")
	@EventSubscriber
	public void onDisconnect(DisconnectedEvent e) {
		switch(e.getReason()) {
		case ABNORMAL_CLOSE:
			LOGGER.warn("Shard closed. Restarting!"); //$NON-NLS-1$
			restart();
			break;
		default:
			break;
		}
	}


//	private void initGuild(IGuild g) {
//		try {
//			((GuildDataManager) Bot.getData()).addGuild(g);
//			//DiscordHelper.getMissingPerms(NEEDED_PERMS_SET, g).forEach(perm -> RequestBuffer.request(() -> g.getDefaultChannel().sendMessage(outputMessage(Messages.getString("AnnotationListener.8") + perm,ERROR)))); //$NON-NLS-1$
//		} catch(Exception e) {
//			e.printStackTrace();
//			pushbullet().push(new SendableNotePush(InfoMessages.getString("AnnotationListener.PBErrOnMessage"),DiscordHelper.dump(e))); //$NON-NLS-1$
//			//DiscordHelper.dumpErrorToChannel(e, g.getDefaultChannel());
//		}
//	}
	
	private static void restart() {
		Bot.closeResources();
		try {
			pushbullet().push(new SendableNotePush("Restarting",String.format("Bot set to restart at %s", new Date(System.currentTimeMillis())))); //$NON-NLS-1$ //$NON-NLS-2$
			Runtime.getRuntime().exec("systemctl restart discordbot"); //$NON-NLS-1$
			System.exit(0);
		} catch (IOException e) {
			e.printStackTrace();
		} 
	}
}