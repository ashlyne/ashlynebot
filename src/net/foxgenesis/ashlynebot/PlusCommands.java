package net.foxgenesis.ashlynebot;
import static net.foxgenesis.ashlynebot.Bot.delete;
import static net.foxgenesis.util.helper.DiscordHelper.ERROR;
import static net.foxgenesis.util.helper.DiscordHelper.INFO;
import static net.foxgenesis.util.helper.DiscordHelper.outputMessage;
import static net.foxgenesis.util.helper.DiscordHelper.outputMessageDelete;

import java.rmi.server.UID;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.SortedSet;
import java.util.TreeSet;
import java.util.function.Consumer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import net.foxgenesis.ashlynebot.commands.AFKCommand;
import net.foxgenesis.ashlynebot.commands.AnimePlanetCommand;
import net.foxgenesis.ashlynebot.commands.AvatarCommand;
import net.foxgenesis.ashlynebot.commands.BinaryCommand;
import net.foxgenesis.ashlynebot.commands.BirthdayCommand;
import net.foxgenesis.ashlynebot.commands.BotBanCommand;
import net.foxgenesis.ashlynebot.commands.CentCommand;
import net.foxgenesis.ashlynebot.commands.DankMemeCommands;
import net.foxgenesis.ashlynebot.commands.EmojiCommand;
import net.foxgenesis.ashlynebot.commands.EyeBleachCommand;
import net.foxgenesis.ashlynebot.commands.FancyTextCommand;
import net.foxgenesis.ashlynebot.commands.GizoogleCommand;
import net.foxgenesis.ashlynebot.commands.GraphCommand;
import net.foxgenesis.ashlynebot.commands.GraphCommandPolar;
import net.foxgenesis.ashlynebot.commands.ImageFilterCommand;
import net.foxgenesis.ashlynebot.commands.ImageStoreCommand;
import net.foxgenesis.ashlynebot.commands.InsultCommand;
import net.foxgenesis.ashlynebot.commands.MassMoveCommand;
import net.foxgenesis.ashlynebot.commands.Messages;
import net.foxgenesis.ashlynebot.commands.MissleCommand;
import net.foxgenesis.ashlynebot.commands.OffensiveMemeCommand;
import net.foxgenesis.ashlynebot.commands.OofCommand;
import net.foxgenesis.ashlynebot.commands.PickCommand;
import net.foxgenesis.ashlynebot.commands.QuoteCommand;
import net.foxgenesis.ashlynebot.commands.RateCommand;
import net.foxgenesis.ashlynebot.commands.RedditCommand;
import net.foxgenesis.ashlynebot.commands.ReminderCommand;
import net.foxgenesis.ashlynebot.commands.Rule34Command;
import net.foxgenesis.ashlynebot.commands.SexyFishCommand;
import net.foxgenesis.ashlynebot.commands.ShipCommand2;
import net.foxgenesis.ashlynebot.commands.SymbolCommand;
import net.foxgenesis.ashlynebot.commands.UDankCommand;
import net.foxgenesis.ashlynebot.commands.UrbanDicCommand;
import net.foxgenesis.ashlynebot.commands.VoteCommand;
import net.foxgenesis.ashlynebot.commands.WideCommand;
import net.foxgenesis.ashlynebot.commands.YTDLCommand;
import net.foxgenesis.ashlynebot.commands.YesNoCommand;
import net.foxgenesis.ashlynebot.commands.api.E621Command;
import net.foxgenesis.ashlynebot.config.BooleanField;
import net.foxgenesis.discord.command.Command;
import net.foxgenesis.discord.command.CommandStorage;
import net.foxgenesis.discord.command.IPlusCommand;
import net.foxgenesis.discord.guild.GuildDataManager;
import net.foxgenesis.discord.guild.JSONObjectAdv;
import net.foxgenesis.util.MethodTimer;
import net.foxgenesis.util.helper.DiscordHelper;
import net.foxgenesis.util.helper.DiscordHelper.BannedBotUsers.Ban;
import sx.blah.discord.handle.obj.IChannel;
import sx.blah.discord.handle.obj.IGuild;
import sx.blah.discord.handle.obj.IMessage;
import sx.blah.discord.handle.obj.IUser;
import sx.blah.discord.util.RequestBuffer;

public final class PlusCommands {
	private static final HashMap<String,IPlusCommand> commands = new HashMap<>();
	private static final BooleanField BOT_ONLY = new BooleanField("bot_channel_only",false,true); //$NON-NLS-1$
	public static final Pattern pattern = Pattern.compile("([^\"]\\S*|\".+?\")\\s*"); //$NON-NLS-1$
	
	private static final Logger LOGGER = LoggerFactory.getLogger("Bot Commands"); //$NON-NLS-1$

	private static final IPlusCommand[] c = {
			new GizoogleCommand(),
			new CentCommand(),
			new InsultCommand(), 
			new YTDLCommand(),
			new BinaryCommand(),
			new SymbolCommand(),
			new RateCommand(),
			new YesNoCommand(),
			new Rule34Command(),
			new ImageStoreCommand(),
			new QuoteCommand(),
			new BirthdayCommand(),
			new PickCommand(),
			new UDankCommand(),
			new ImageFilterCommand(),
			new EyeBleachCommand(),
			new VoteCommand(),
			new AvatarCommand(),
			new SexyFishCommand(),
			new MissleCommand(),
			new GraphCommand(),
			new GraphCommandPolar(),
			new EmojiCommand(),
			new AFKCommand(),
			new E621Command(),
			new OofCommand(),
			//new PornPicsCommand(),
			new WideCommand(),
			new DankMemeCommands(),
			new AnimePlanetCommand(),
			new OffensiveMemeCommand(),
			new RedditCommand(),
			new MassMoveCommand(),
			new ReminderCommand(),
			new ShipCommand2(),
			new UrbanDicCommand(),
			new FancyTextCommand()
	};

	static void init() {
		LOGGER.info("Adding default commands"); //$NON-NLS-1$
		addCommand(new HelpCommand());
		addCommand(new ShutdownCommand());
		addCommand(new PermCommand());
		addCommand(new BotBanCommand());
		addCommand(new DefaultChannelCommand());
		addCommand(new SetConfigCommand());
		addCommand(new GetConfigCommand());
		LOGGER.info(String.format("Added %s custom commands in: " + MethodTimer.runFormatMS(() -> addCommands(c)),c.length)); //$NON-NLS-1$
	}

	/**
	 * @param message
	 * @param command
	 * @param inputs
	 * @throws Exception
	 */
	static void handle(IMessage message, String command, String inputs) throws Exception {
		if(commands.containsKey(command)) {
			IGuild guild = message.getGuild();
			IUser user = message.getAuthor();
			IChannel chan = message.getChannel();


			/*
			 * Bot only channel check
			 */
			if(!chan.isPrivate() && !chan.getName().contains("bot") && BOT_ONLY.optFrom(guild)) { //$NON-NLS-1$
				message.delete();
				outputMessageDelete(message,InfoMessages.getString("PlusCommands.1"),ERROR); //$NON-NLS-1$ 
				return;
			}

			/*
			 * Ban check
			 */
			Ban ban = DiscordHelper.BannedBotUsers.isBanned(user);
			if(ban != null) {
				message.delete();
				outputMessageDelete(message,String.format(InfoMessages.getString("PlusCommands.3"),ban.creator,ban.reason,ban.date),ERROR); //$NON-NLS-1$ 
				return;
			}


			Command c = commands.get(command).getClass().getAnnotation(Command.class);
			boolean admin = Bot.isBotAdmin(user);
			boolean guildOwner = Bot.isGuildOwner(user, guild);
			boolean adminG = Bot.hasAdminPerms(user, guild);

			if(c.isSuperCommand() && c.isOwnerCommand()) {
				if(!(admin || adminG)) {
					outputMessageDelete(message,InfoMessages.getString("PlusCommands.5"),ERROR); //$NON-NLS-1$ 
					return;
				}
			} else if(c.isOwnerCommand() && c.allowAdminUse() && !adminG) {
				outputMessageDelete(message,InfoMessages.getString("PlusCommands.0"),ERROR); //$NON-NLS-1$
				return;
			} else if(c.isOwnerCommand() && !guildOwner) {
				outputMessageDelete(message,InfoMessages.getString("PlusCommands.5"),ERROR); //$NON-NLS-1$ 
				return;
			} else if(c.isSuperCommand() && !admin) {
				outputMessageDelete(message,InfoMessages.getString("PlusCommands.9"),ERROR); //$NON-NLS-1$ 
				return;
			}

			if(c.cooldown() && !DiscordHelper.CoolDown.valid(user, c.cooldownTime())) {
				outputMessageDelete(message,InfoMessages.getString("PlusCommands.11"),ERROR); //$NON-NLS-1$ 
				return;
			}


			if(c.isNSFW() && !message.getChannel().isNSFW()) {
				outputMessageDelete(message,InfoMessages.getString("PlusCommands.13"),ERROR); //$NON-NLS-1$ 
				return;
			}


			/*
			 * Set typing status
			 */
			UID id = new UID();
			DiscordHelper.TypingStatus.setTyping(message.getChannel(), id, true);
			//System.out.printf("{%s}[%s](%s)<%s>: %s\n",Thread.currentThread().getName(),guild.getName(),message.getChannel().getName(),user.getName(), "Executed command: " + message.getContent());
			try {
				commands.get(command).handleCommand(message, inputs);
			} catch (Exception e) {
				e.printStackTrace();
			}
			DiscordHelper.TypingStatus.setTyping(message.getChannel(), id, false);
		}
	}

	public static void addCommand(IPlusCommand command) {
		if(!command.getClass().isAnnotationPresent(Command.class))
			throw new UnsupportedOperationException("Plus command [" + command.getClass() + "] is missing Command annotation!"); //$NON-NLS-1$ //$NON-NLS-2$
		Command c = command.getClass().getAnnotation(Command.class);
		if(commands.containsKey(c.name().toLowerCase()))
			throw new UnsupportedOperationException("Plus command [" + c.name() + "] is already defined!"); //$NON-NLS-1$ //$NON-NLS-2$
		if(command instanceof CommandStorage) {
			GuildDataManager m = (GuildDataManager) Bot.getData();
			CommandStorage s = (CommandStorage)command;
			m.pushStorage(s.storage);
		}
		commands.put(c.name().toLowerCase(), command);
		LOGGER.debug("Added command: " + c.name()); //$NON-NLS-1$
	}

	public static void addCommands(IPlusCommand... commands) {
		Arrays.stream(commands).parallel().forEach(PlusCommands::addCommand);
	}

	@Command(name = "+help", help = "Exactly what you fucking think.")
	private static class HelpCommand implements IPlusCommand {
		private static final int SUPER = 3, OWNER = 2, ADMIN = 1, EVERYONE = 0;

		@Override
		public void handleCommand(IMessage message, String inputs) {
			IUser user = message.getAuthor();
			IGuild guild = message.getGuild();
			reduce(out -> user.getOrCreatePMChannel().sendMessage(out),Bot.isBotAdmin(user)? 
					SUPER : Bot.isGuildOwner(user, guild)? OWNER : Bot.hasAdminPerms(user, guild)? ADMIN : EVERYONE);
			DiscordHelper.tryToDelete(message);
		}

		private static void reduce(Consumer<String> c, int level) {
			StringBuilder b = new StringBuilder("++Help:" + System.lineSeparator()); //$NON-NLS-1$
			SortedSet<String> cmnds = new TreeSet<>(commands.keySet());
			for(String a : cmnds) {
				IPlusCommand co = commands.get(a);
				Command com = co.getClass().getAnnotation(Command.class);
				switch(level) {
				case EVERYONE:
					if(com.isHidden() || com.isOwnerCommand())
						continue;
				case ADMIN:
					if(!com.allowAdminUse())
						continue;
				case OWNER:
					if(com.isSuperCommand())
						continue;
					break;
				}
//				if(com.isHidden() && level < ADMIN)
//					continue;
//				if(com.allowAdminUse() && level < ADMIN)
//					if(com.isOwnerCommand() && level < OWNER)
//						continue;
//				if(com.isSuperCommand() && level < SUPER)
//					continue;
				String outt = ""; //$NON-NLS-1$
				if(com.isSuperCommand() && level == SUPER)
					outt += "/* SUPER */\n"; //$NON-NLS-1$
				else if(com.isOwnerCommand() && level >= (com.allowAdminUse()? ADMIN : OWNER))
					outt += "/*OWNER*\n"; //$NON-NLS-1$
				if(com.isNSFW())
					outt += "/* NSFW *\n"; //$NON-NLS-1$
				String out = String.format("# +%s %s%s%s<\t%s >", a, com.helpParams(), System.lineSeparator(), outt ,com.help()) + System.lineSeparator(); //$NON-NLS-1$
				if(b.length() + out.length() > 1950) {
					final String outtt = b.toString();
					RequestBuffer.request(() -> c.accept(DiscordHelper.captureInSyntax(outtt,"md"))); //$NON-NLS-1$
					b = new StringBuilder("++Help:" + System.lineSeparator()); //$NON-NLS-1$
				} 
				b.append(out);
			}
			final String outt = b.toString();
			RequestBuffer.request(() -> c.accept(DiscordHelper.captureInSyntax(outt,"md"))); //$NON-NLS-1$
		}
	}

	@Command(name = "perms", help = "Check bot perms", isHidden = true, isOwnerCommand = true, isSuperCommand = true)
	private static class PermCommand implements IPlusCommand {

		@Override
		public void handleCommand(IMessage message, String inputs) throws Exception {
			delete(message.reply(InfoMessages.getString("PlusCommands.28") + DiscordHelper.getPermsForGuild(message.getGuild()).toString())); //$NON-NLS-1$
		}
	}

	@Command(name = "defchan", help = "Set the default channel", isHidden = true, isOwnerCommand = true, isSuperCommand = true)
	private static class DefaultChannelCommand implements IPlusCommand {

		@Override
		public void handleCommand(IMessage message, String inputs) throws Exception {
			message.delete();
			long channelID = -1;
			try {
				channelID = Long.parseLong(inputs);
			} catch(Exception e) {
				outputMessageDelete(message,InfoMessages.getString("PlusCommands.30"),ERROR); //$NON-NLS-1$ 
				return;
			}
			Bot.getData().getDataForGuild(message.getGuild()).getConfig().put("default_channel", channelID); //$NON-NLS-1$
			outputMessageDelete(message,InfoMessages.getString("PlusCommands.32"),INFO); //$NON-NLS-1$ 
		}

	}

	@Command(name = "setcfg", help = "Set config value", isHidden = true, isSuperCommand = true)
	private static class SetConfigCommand implements IPlusCommand {

		@Override
		public void handleCommand(IMessage message, String inputs) throws Exception {
			List<String> list = new ArrayList<>();
			if(!inputs.isEmpty()) {
				Matcher m = pattern.matcher(inputs.trim());
				while(m.find())
					list.add(m.group(1).replace("\"","")); //$NON-NLS-1$ //$NON-NLS-2$
			}
			DiscordHelper.tryToDelete(message);

			String[] split = list.toArray(new String[]{});

			if(split != null && split.length == 2) {
				IGuild guild = message.getGuild();
				String key = split[0].toLowerCase();
				String val = split[1];
				boolean isNull = val.equalsIgnoreCase("null"); //$NON-NLS-1$

				JSONObjectAdv config = Bot.getData().getDataForGuild(guild).getConfig();

				if(isNull) {
					if(config.has(key)) {
						config.remove(key);
						outputMessageDelete(message,InfoMessages.getString("PlusCommands.33"),INFO); //$NON-NLS-1$ 
					}
					else
						outputMessageDelete(message,InfoMessages.getString("PlusCommands.35"),ERROR); //$NON-NLS-1$ 
				} else {
					config.put(key, val);
					outputMessageDelete(message,InfoMessages.getString("PlusCommands.37"),INFO); //$NON-NLS-1$ 
				}

				return;
			}
			outputMessageDelete(message,InfoMessages.getString("PlusCommands.39"),ERROR); //$NON-NLS-1$ 
		}

	}

	@Command(name = "getcfg", help = "Get config value", isHidden = true, isSuperCommand = true, isOwnerCommand = true)
	private static class GetConfigCommand implements IPlusCommand {

		@Override
		public void handleCommand(IMessage message, String inputs) throws Exception {
			StringBuilder build = new StringBuilder();
			delete(message);
			JSONObjectAdv config = Bot.getData().getDataForGuild(message.getGuild()).getConfig();
			config.keySet().stream().sorted(
					(a,b) -> a.compareTo(b)).forEachOrdered(entry -> {
						if(build.length() > 1800) {
							delete(message.reply(String.format("\n%s", build.toString()))); //$NON-NLS-1$
							build.setLength(0);
						}
						build.append(String.format("%s%10s\n", entry, config.opt(entry))); //$NON-NLS-1$
					});
			delete(message.reply(String.format("\n%s", build.toString()))); //$NON-NLS-1$
		}

	}
	
	
	@Command(name = "shutdown", isHidden = true, isSuperCommand = true)
	private static class ShutdownCommand implements IPlusCommand {

		@Override
		public void handleCommand(IMessage message, String inputs) throws Exception {
			if(message.getAuthor().equals(message.getClient().getApplicationOwner())) {
				outputMessage(message,Messages.getString("ShutdownCommand.0"),INFO); //$NON-NLS-1$
				Bot.shutdown();
			}
		}
	}
}
