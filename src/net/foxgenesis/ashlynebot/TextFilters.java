package net.foxgenesis.ashlynebot;
import static net.foxgenesis.util.helper.DiscordHelper.ERROR;
import static net.foxgenesis.util.helper.DiscordHelper.INFO;
import static net.foxgenesis.util.helper.DiscordHelper.outputMessageDelete;

import java.util.ArrayList;

import net.foxgenesis.ashlynebot.filters.AntiRacismFilter;
import net.foxgenesis.ashlynebot.filters.AppleFilter;
import net.foxgenesis.discord.command.Command;
import net.foxgenesis.discord.command.IPlusCommand;
import net.foxgenesis.discord.filters.Filter;
import net.foxgenesis.discord.filters.ITextFilter;
import net.foxgenesis.discord.guild.JSONObjectAdv;
import net.foxgenesis.util.helper.StringHelper;
import sx.blah.discord.handle.obj.IGuild;
import sx.blah.discord.handle.obj.IMessage;

public final class TextFilters {
	private static final String OUTPUT_FORMAT = "%s\tenabled: %s\n"; //$NON-NLS-1$
	private static final String KEY_FORMAT = "f.%s.enabled"; //$NON-NLS-1$
	private static final ArrayList<ITextFilter> filters = new ArrayList<>();
	private static final ArrayList<String> filterNames = new ArrayList<>();

	static void init() {
		addFilter(
				new AppleFilter(),
				new AntiRacismFilter()
				);
		PlusCommands.addCommand(new FilterCommand());
	}
	
	public static void addFilter(ITextFilter filter) {
		if(filter.getClass().isAnnotationPresent(Filter.class)) {
			filters.add(filter);
			Filter fi = filter.getClass().getAnnotation(Filter.class);
			filterNames.add(simplify(fi.name()));
		} else
			throw new RuntimeException(filter.getClass() + " is missing Filter annotation!"); //$NON-NLS-1$
	}

	public static void addFilter(ITextFilter... filters) {
		for(ITextFilter f : filters)
			addFilter(f);
	}
	
	private static String key(String name) {
		return String.format(KEY_FORMAT, simplify(name));
	}
	
	private static String simplify(String in) {
		return StringHelper.replaceAllWhiteSpace(StringHelper.removeAllNonAlphaNumeric(in)).toLowerCase();
	}

	static void handle(IMessage message) {
		for(ITextFilter filter : filters) {
			if(filter.isAllowed(message)) {
				Filter fi = filter.getClass().getAnnotation(Filter.class);

				// Check if text filter is enabled
				if(!Bot.getData().getDataForGuild(message.getGuild()).getConfig().
						optBoolean(key(fi.name()), fi.defaultState()))
					continue;

				// Check if the filter is allowed for bot messages
				if(message.getAuthor().isBot() && !fi.allowBotMessages())
					continue;

				// All checks passed. Pass the message to the filter
				filter.handle(message);
			}
		}
		
		// Old 
		//		filters.stream().filter(f -> message.getAuthor().isBot()? get(f).allowBot() : true).filter(f -> allowedForGuild(message.getGuild(),f))
		//		.filter(f -> get(f).useContains()? message.getFormattedContent().contains(f.getRegularExpression(message.getGuild(),message.getChannel()).pattern()) 
		//				: f.getRegularExpression(message.getGuild(),message.getChannel()).matcher(message.getFormattedContent().replaceAll(":b:", "b")).find()) //$NON-NLS-1$ //$NON-NLS-2$
		//		.forEach(f -> f.onFilter(message));
	}

	@Command(name = "filters", help = "Master command for filters.", helpParams = "[filter <enable | disable>]", isOwnerCommand = true, isSuperCommand = true)
	private static class FilterCommand implements IPlusCommand {
		@Override
		public void handleCommand(IMessage message, String inputs) throws Exception {
			IGuild guild = message.getGuild();
			JSONObjectAdv config = Bot.getData().getDataForGuild(guild).getConfig();
			
			if(inputs == null) {
				// Output current filter state
				StringBuilder output = new StringBuilder("Filters:```\n"); //$NON-NLS-1$
				for(ITextFilter a: filters) {
					Filter fi = a.getClass().getAnnotation(Filter.class);
					String simplified_name = simplify(fi.name());
					
					output.append(String.format(OUTPUT_FORMAT, simplified_name, 
							config.optBoolean(String.format(KEY_FORMAT, simplified_name), fi.defaultState()))); 
				}
				output.append("```"); //$NON-NLS-1$
				message.reply(output.toString());
			} else {
				// Set desired filter state based on input
				String[] d = inputs.split(" "); //$NON-NLS-1$
				if(d.length == 2 && d[1].matches("enable|disable")){ //$NON-NLS-1$
					if(filterNames.contains(d[0].toLowerCase())) {
						config.put(key(d[0]),d[1].equals("enable")); //$NON-NLS-1$
						outputMessageDelete(message,InfoMessages.getString("TextFilters.1"),INFO); //$NON-NLS-1$ 
					} else
						outputMessageDelete(message,InfoMessages.getString("TextFilters.3") + d[0],ERROR); //$NON-NLS-1$ 
				} else
					outputMessageDelete(message,InfoMessages.getString("TextFilters.5"),ERROR); //$NON-NLS-1$ 
			}
		}
	}
}
