package net.foxgenesis.ashlynebot.commands;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;

import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.message.BasicNameValuePair;

import net.foxgenesis.discord.command.Command;
import net.foxgenesis.discord.command.IPlusCommand;
import net.foxgenesis.util.helper.DiscordHelper;
import net.foxgenesis.util.helper.WebHelper;
import sx.blah.discord.handle.obj.IMessage;
import sx.blah.discord.util.DiscordException;
import sx.blah.discord.util.MissingPermissionsException;
import sx.blah.discord.util.RateLimitException;

@Command(name = "gizoogle", help = "Changes text to make it look like it's from the inner-city", helpParams = "<text...>", cooldown = true)
public class GizoogleCommand implements IPlusCommand {
	private static String GIZ_START = "<textarea type=\"text\" name=\"translatetext\" style=\"width: 600px; height:250px;\"/>"; //$NON-NLS-1$
	private static String GIZ_END = "</textarea>"; //$NON-NLS-1$

	@Override
	public void handleCommand(IMessage message, String inputs) {
		try {
			gizoogle(message::reply,inputs);
		} catch (DiscordException | RateLimitException | MissingPermissionsException | IOException e) {
			e.printStackTrace();
		}
	}

	private static void gizoogle(Consumer<String> c, String text) throws ClientProtocolException, IOException {
		List<NameValuePair> params = new ArrayList<>(2);
		params.add(new BasicNameValuePair("translate", "Tranzizzle Dis Shiznit")); //$NON-NLS-1$ //$NON-NLS-2$
		params.add(new BasicNameValuePair("translatetext", text)); //$NON-NLS-1$
		String output = WebHelper.readInputStream(WebHelper.sendPost("http://gizoogle.net/textilizer.php", params)); //$NON-NLS-1$
		if(output != null) {
			output = output.substring(output.indexOf(GIZ_START) + GIZ_START.length(), output.indexOf(GIZ_END));
			DiscordHelper.splitToMessages(output, 1900, c);
		}
	}
}
