package net.foxgenesis.ashlynebot.commands;

import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

import org.json.JSONArray;
import org.json.JSONObject;

import net.foxgenesis.ashlynebot.Bot;
import net.foxgenesis.discord.command.CommandStorage;
import net.foxgenesis.discord.guild.SQLDataType;
import net.foxgenesis.discord.guild.StorageHandler;
import net.foxgenesis.util.TimeFilterSelector;
import net.foxgenesis.util.helper.DiscordHelper;
import net.foxgenesis.util.helper.RedditHelper;
import net.foxgenesis.util.helper.RedditHelper.RedditPost;
import sx.blah.discord.handle.obj.IMessage;
import sx.blah.discord.handle.obj.IPrivateChannel;
import sx.blah.discord.handle.obj.IUser;
import sx.blah.discord.util.RequestBuffer;

public class CoinCommand extends CommandStorage {
	private final String r,name,names;
	private final String BLEACH_DATA, TRAIN_DATA_KEY;
	private final int starting;

	public CoinCommand(String r, String name, String p, int s) {
		super("coin." + name, SQLDataType.JSON.name,SQLDataType.JSON); //$NON-NLS-1$
		this.r = r;
		this.name = name;
		this.names = p;
		this.BLEACH_DATA = "coin." + name + ".filter"; //$NON-NLS-1$ //$NON-NLS-2$
		this.TRAIN_DATA_KEY = "coin." + name + ".data"; //$NON-NLS-1$ //$NON-NLS-2$
		this.starting = s;
	}

	@Override
	public void handleCommand(IMessage message, String inputs, StorageHandler h) throws Exception {
		DiscordHelper.tryToDelete(message);
		Bank b = new Bank(h);
		Wallet w = b.getWallet(message.getAuthor());

		if(message.getMentions().size() > 0) {
			String[] d = inputs.split(" "); //$NON-NLS-1$
			if(d.length > 0 && d[0].trim().equalsIgnoreCase("gen") && Bot.isBotAdmin(message.getAuthor())) { //$NON-NLS-1$
				message.getMentions().stream().filter(u -> !u.isBot()).forEach(u -> b.getWallet(u).add(this.starting));
				b.pushChanges();
				message.reply("Regened tokens."); //$NON-NLS-1$
				return;
			}

			if(w.getCash() <= 0) {
				message.reply("You have no " + this.names + " left!"); //$NON-NLS-1$ //$NON-NLS-2$
				return;
			}

			RedditHelper r = new RedditHelper(this.r);
			List<RedditPost> l = r.getPosts(100).filter(re -> !re.stickied).collect(Collectors.toList());
			TimeFilterSelector<RedditPost> s = DiscordHelper.getFilterForGuild(message.getGuild(), this.BLEACH_DATA);
			RedditPost re = s.getRandomItem(l);
			message.getMentions().stream().limit(w.getCash()).filter(u -> !u.isBot()).forEach(u -> {
				if(w.remove()) {
					b.getWallet(u).add();
					train(u,message,re);
				}
			});
			b.pushChanges();
		} else
			message.reply(String.format("You have %d " + this.names + " left",w.getCash())); //$NON-NLS-1$ //$NON-NLS-2$
	}

	private void train(IUser user, IMessage ch, RedditPost p) {
		RequestBuffer.request(() -> {
			ch.getChannel().sendMessage(ch.getAuthor().mention() + " sent a " + this.name + " to " + user.mention()); //$NON-NLS-1$ //$NON-NLS-2$
			IPrivateChannel c = user.getOrCreatePMChannel();
			c.sendMessage(p.url);
		});
	}

	private class Bank {
		private HashMap<Long,Wallet> data = new HashMap<>();
		private final StorageHandler h;
		public Bank(StorageHandler h) {
			this.h = h;
			JSONObject b = h.data == null? (JSONObject) (h.data = new JSONObject()) : (JSONObject) h.data;
			JSONArray a = b.optJSONArray(CoinCommand.this.TRAIN_DATA_KEY);
			if(a != null)
				for(int i=0; i<a.length(); i++) {
					try {
						Wallet t = new Wallet(a.getJSONObject(i));
						this.data.put(t.owner,t);
					} catch(Exception e) {
						e.printStackTrace();
					}
				}
		}

		public Wallet getWallet(IUser user) {
			this.data.putIfAbsent(user.getLongID(), new Wallet(user));
			return this.data.get(user.getLongID());
		}

		public void pushChanges() {
			((JSONObject) this.h.data).put(CoinCommand.this.TRAIN_DATA_KEY, this.toJSON());
			this.h.update();
		}

		public JSONArray toJSON() {
			JSONArray b = new JSONArray();
			this.data.forEach((id,wallet) -> b.put(wallet.toJSON()));
			return b;
		}
	}

	private class Wallet {
		public final long owner;
		private int cash;

		public Wallet(IUser user) {
			this.owner = user.getLongID();
			this.cash = CoinCommand.this.starting;
		}

		public Wallet(JSONObject obj) {
			this.owner = obj.getLong("owner"); //$NON-NLS-1$
			this.cash = obj.getInt("cash"); //$NON-NLS-1$
		}

		public int getCash() {
			return this.cash;
		}

		public void add() {
			this.cash++;
		}

		public void add(int amount) {
			this.cash+=amount;
		}

		public boolean remove() {
			if(this.cash > 0) {
				this.cash--;
				return true;
			}
			return false;
		}

		public JSONObject toJSON() {
			JSONObject b = new JSONObject();
			b.put("owner", this.owner); //$NON-NLS-1$
			b.put("cash", this.cash); //$NON-NLS-1$
			return b;
		}
	}
}
