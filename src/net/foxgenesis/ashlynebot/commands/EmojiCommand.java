package net.foxgenesis.ashlynebot.commands;

import static net.foxgenesis.util.helper.DiscordHelper.ERROR;
import static net.foxgenesis.util.helper.DiscordHelper.INFO;
import static net.foxgenesis.util.helper.DiscordHelper.WARN;
import static net.foxgenesis.util.helper.DiscordHelper.outputMessageDelete;
import static sx.blah.discord.Discord4J.LOGGER;

import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import javax.imageio.ImageIO;

import net.foxgenesis.ashlynebot.Bot;
import net.foxgenesis.discord.command.Command;
import net.foxgenesis.discord.command.IPlusCommand;
import net.foxgenesis.util.Waiter;
import net.foxgenesis.util.helper.DiscordHelper;
import sx.blah.discord.handle.obj.IEmoji;
import sx.blah.discord.handle.obj.IGuild;
import sx.blah.discord.handle.obj.IMessage;
import sx.blah.discord.handle.obj.IRole;
import sx.blah.discord.util.RequestBuffer;
import sx.blah.discord.util.RequestBuffer.RequestFuture;

@Command(name = "emoji", isSuperCommand = true, isHidden = true, cooldown = true, cooldownTime = 300_000)
public class EmojiCommand implements IPlusCommand {
	private static File folder = new File("emoji"); //$NON-NLS-1$
	//private static long ID = 331689006888648705l;
	private static int SIZEW = 6;
	private static int SIZEH = 6;
	private static final String[][] data = new String[SIZEH][SIZEW];
	private final static char PREFIX = 'a';
	private static final String emoji = 
			":e0::e6::e12::e18::e24::e30:\n" + //$NON-NLS-1$
					":e1::e7::e13::e19::e25::e31:\n" + //$NON-NLS-1$
					":e2::e8::e14::e20::e26::e32:\n" + //$NON-NLS-1$
					":e3::e9::e15::e21::e27::e33:\n" + //$NON-NLS-1$
					":e4::e10::e16::e22::e28::e34:\n" + //$NON-NLS-1$
					":e5::e11::e17::e23::e29::e35:"; //$NON-NLS-1$

	static {
		folder.mkdir();
	}


	@Override
	public void handleCommand(IMessage message, String inputs) throws Exception {
		try { message.delete(); } catch(Exception e){}
		if(inputs == null) {
			message.getAuthor().getOrCreatePMChannel().sendMessage(emoji.replaceAll("e", "" + EmojiCommand.PREFIX)); //$NON-NLS-1$ //$NON-NLS-2$
			return;
		}
		String[] d = inputs.split(" "); //$NON-NLS-1$
		if(d.length != 1) {
			outputMessageDelete(message,Messages.getString("EmojiCommand.11"),ERROR); //$NON-NLS-1$ 
			return;
		}
		BufferedImage i = null;
		try {
			Image t =  DiscordHelper.getDiscordImage(new URL(d[0])).getScaledInstance(SIZEW == 6? 1032 : 1029, 1032, Image.SCALE_SMOOTH);
			i = new BufferedImage(t.getWidth(null),t.getHeight(null),BufferedImage.TYPE_INT_ARGB);
			Graphics2D g = i.createGraphics();
			g.drawImage(t, 0, 0, null);
			g.dispose();
		} catch(Exception e) {
			outputMessageDelete(message,Messages.getString("EmojiCommand.13"),ERROR); //$NON-NLS-1$ 
			LOGGER.error("[EmojiCommand]: Exception while trying to get image", e); //$NON-NLS-1$
			return;
		}

		IGuild emojiGuild = Bot.getClient().getGuildByID(338766001607016448l);

		if(!emojiGuild.getEmojis().isEmpty()) {
			emojiGuild.getEmojis().stream().filter(em -> !em.isDeleted()).forEach(emoji -> RequestBuffer.request(emoji::deleteEmoji));
			Timer t = new Timer();

			t.scheduleAtFixedRate(new TimerTask() {
				@Override
				public void run() {
					synchronized(emojiGuild) {
						if(emojiGuild.getEmojis().isEmpty()) {
							this.cancel();
							emojiGuild.notify();
						}
					}
				}
			}, 1000l, 1000l);

			synchronized(emojiGuild) {
				try {
					LOGGER.debug("[EmojiCommand]: Waiting for guild to be clear of emojis..."); //$NON-NLS-1$
					emojiGuild.wait(300_000);
				} catch(InterruptedException e) {
					LOGGER.error("[EmojiCommand]: Interrupted while waiting", e); //$NON-NLS-1$
				}
				if(!emojiGuild.getEmojis().isEmpty()) {
					outputMessageDelete(message,Messages.getString("EmojiCommand.18"),WARN); //$NON-NLS-1$ 
					LOGGER.error("[EmojiCommand]: Unable to clear guild of emojis!", new RuntimeException()); //$NON-NLS-1$
					return;
				}
				LOGGER.debug("[EmojiCommand]: Guild emojis have been cleared"); //$NON-NLS-1$
			}
		}

		int width = i.getWidth(), height = i.getHeight();
		int cellW = width/SIZEW, cellH = height/SIZEH;
		int count = 0;
		File p = new File(folder,"" + EmojiCommand.PREFIX); //$NON-NLS-1$
		p.mkdir();
		List<RequestFuture<IEmoji>> list = new ArrayList<>();
		Waiter w = new Waiter(() -> list.stream().allMatch(RequestFuture::isDone));
		for(int x = 0; x < width; x+=cellW)
			for(int y = 0; y < height; y+=cellH) {
				File f = saveEmojiSection(i.getSubimage(x, y, cellW, cellH),p,count,"" + EmojiCommand.PREFIX); //$NON-NLS-1$
				final int currentCount = count;
				list.add(RequestBuffer.request(() -> {return emojiGuild.createEmoji(PREFIX + 
						"" + currentCount, sx.blah.discord.util.Image.forFile(f), new IRole[]{});})); //$NON-NLS-1$
				count++;
			}
		w.startWaiting();
		outputMessageDelete(message,Messages.getString("EmojiCommand.25") + getEmojiString(emojiGuild),INFO); //$NON-NLS-1$ 
	}

	private static String getEmojiString(IGuild guild) {
		StringBuilder build = new StringBuilder();
		for(int i=0; i<data.length; i++) {
			for(int j=0; j<data[i].length; j++) 
				build.append(guild.getEmojiByName(PREFIX + "" +  (i + (j * SIZEW)))); //$NON-NLS-1$
			build.append('\n');
		}
		return build.toString();
	}

	private static File saveEmojiSection(BufferedImage i, File folder, int count, String prefix) {
		String fName = String.format("%s%s", prefix,count); //$NON-NLS-1$
		int x = count%SIZEW;
		int y = count/SIZEW;
		data[x][y] = ':' + fName + ':';
		try {
			if(ImageIO.write(i, "PNG", new File(folder,fName + ".png"))) //$NON-NLS-1$ //$NON-NLS-2$
				return new File(folder, fName + ".png"); //$NON-NLS-1$
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}

}
