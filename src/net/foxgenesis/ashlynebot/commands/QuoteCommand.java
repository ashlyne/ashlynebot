package net.foxgenesis.ashlynebot.commands;

import static net.foxgenesis.util.helper.DiscordHelper.ERROR;
import static net.foxgenesis.util.helper.DiscordHelper.outputMessageDelete;

import net.foxgenesis.discord.command.Command;
import net.foxgenesis.discord.command.IPlusCommand;
import net.foxgenesis.util.helper.DiscordHelper;
import sx.blah.discord.api.internal.json.objects.EmbedObject;
import sx.blah.discord.handle.obj.IMessage;

@Command(name = "quote", help = "Quote a message", helpParams = "<message id>")
public class QuoteCommand implements IPlusCommand {

	@Override
	public void handleCommand(IMessage message, String inputs) throws Exception {
		if(inputs != null) 
			if(validNumber(inputs)) {
				IMessage m = message.getGuild().getMessageByID(Long.parseLong(inputs));
				if(m != null)
					message.getChannel().sendMessage(getMessageQuote(m));
				else outputMessageDelete(message,Messages.getString("QuoteCommand.0"),ERROR); //$NON-NLS-1$ 
			} else outputMessageDelete(message,Messages.getString("QuoteCommand.1"),ERROR); //$NON-NLS-1$ 
		else outputMessageDelete(message,Messages.getString("QuoteCommand.2"),ERROR); //$NON-NLS-1$ 
	}
	
	private static boolean validNumber(String in) {
		try {
			Long.parseLong(in);
			return true;
		} catch(Exception e) {
			return false;
		}
	}
	
	private static EmbedObject getMessageQuote(IMessage message) {
		return DiscordHelper.getDefaultRoleEmbed(message.getAuthor(), message.getGuild())
				.withTimestamp(message.getTimestamp()).withDesc(message.getContent()).build();
	}
}
