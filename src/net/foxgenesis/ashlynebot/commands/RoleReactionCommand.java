package net.foxgenesis.ashlynebot.commands;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import net.foxgenesis.ashlynebot.BotListeners;
import net.foxgenesis.ashlynebot.commands.RoleReactionCommand.ReactionMenu;
import net.foxgenesis.discord.command.Command;
import net.foxgenesis.discord.command.GuildObjectCommand;
import net.foxgenesis.discord.guild.ObjectKey;
import sx.blah.discord.api.events.EventSubscriber;
import sx.blah.discord.handle.impl.events.guild.channel.message.reaction.ReactionAddEvent;
import sx.blah.discord.handle.obj.IGuild;
import sx.blah.discord.handle.obj.IMessage;
import sx.blah.discord.handle.obj.IReaction;

@Command(name = "rolereact", isSuperCommand = true, isOwnerCommand = true)
public class RoleReactionCommand extends GuildObjectCommand<ArrayList<ReactionMenu>> {

	private static ObjectKey<ConcurrentHashMap<Long,ArrayList<ReactionMenu>>> key;

	public RoleReactionCommand() {
		super("roleReaction", () -> new ArrayList<>()); //$NON-NLS-1$
		RoleReactionCommand.key = this.obj;
	}

	@Override
	protected void handleCommand(IMessage message, String inputs, ArrayList<ReactionMenu> list) {
		
	}

	public class ReactionMenu {
		public final long id;
		private final Map<Long,Long> map;
		
		public ReactionMenu(long id) {
			this.id = id;
			this.map = new HashMap<>();
		}
		
		public long getForReaction(IReaction reaction) {
			return this.map.getOrDefault(reaction.getEmoji().getLongID(), -1l);
		}
	}


	static {
		BotListeners.add(new Object() {
			@EventSubscriber
			public void onReaction(ReactionAddEvent event) {
				System.out.println("Checking reaction..."); //$NON-NLS-1$
				IGuild guild = event.getGuild();
				long guildID = guild.getLongID();
				ConcurrentHashMap<Long,ArrayList<ReactionMenu>> key = RoleReactionCommand.key.object;
				if(key.containsKey(guildID)) 
					for(ReactionMenu m : key.get(guildID)) {
						if(m.id != event.getMessageID())
							continue;
						long i;
						if((i = m.getForReaction(event.getReaction())) != -1) {
							System.out.println("Assigning role"); //$NON-NLS-1$
							event.getAuthor().addRole(guild.getRoleByID(i));
						}
						break;
					}
			}
		});
	}
}
