package net.foxgenesis.ashlynebot.commands;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.RenderingHints;
import java.awt.geom.AffineTransform;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;

import javax.imageio.ImageIO;

import net.foxgenesis.discord.command.Command;
import net.foxgenesis.discord.command.IPlusCommand;
import net.foxgenesis.util.TextGraphicUtil;
import net.foxgenesis.util.helper.DiscordHelper;
import sx.blah.discord.handle.obj.IMessage;

@SuppressWarnings("unused")
@Command(name = "missile", help = "Create a missile meme", helpParams = "<upper_text[&&] @@ lower_text[&&]>", isHidden = true, isSuperCommand = true)
public class MissleCommand implements IPlusCommand {

	@Override
	public void handleCommand(IMessage message, String inputs) throws Exception {
		if(inputs != null && !inputs.isEmpty())
			test(message,inputs);
	}

	private static void test(IMessage message, String inputs) throws IOException {
		final int bX = 90, bY = 40;
		final int uX = 65, uY = 130;
		BufferedImage image = ImageIO.read(MissleCommand.class.getResourceAsStream("missle.jpg")); //$NON-NLS-1$
		Graphics2D g = image.createGraphics();

		final int size = image.getWidth() - bX;

		g.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
				RenderingHints.VALUE_ANTIALIAS_ON);

		g.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING,
				RenderingHints.VALUE_TEXT_ANTIALIAS_ON);

		g.setRenderingHint(RenderingHints.KEY_RENDERING,
				RenderingHints.VALUE_RENDER_QUALITY);
		g.setFont(new Font("impact", 0, 27)); //$NON-NLS-1$
		g.setColor(Color.white);
		g.setStroke(new BasicStroke(3));
		AffineTransform orig = g.getTransform();
		g.rotate(Math.toRadians(5));

		String[] l = inputs.split("@@",2); //$NON-NLS-1$

		drawString(g,l[0].trim(), bX, bY, image.getWidth(), image.getHeight(),new Rectangle(image.getWidth()-bX, bY),true);
		if(l.length > 1)
			drawString(g,l[1].trim(), uX, uY, image.getWidth(), image.getHeight(),new Rectangle(size, bY),false);

		g.setTransform(orig);
		g.dispose();
		ImageIO.write(image, "PNG", new File("missle.png")); //$NON-NLS-1$ //$NON-NLS-2$
		message.delete();
		message.getChannel().sendFile(DiscordHelper.getDefaultRoleEmbed(message.getAuthor(), message.getGuild()).withImage("attachment://missle.png").withFooterText("via missile command").build(),new File("missle.png")); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
	}

	private static void drawString(Graphics2D g, String text, int x, int y, int width, int height, Rectangle r, boolean direction) {
		final int l = 40;
		boolean D = text.endsWith("&&"); //$NON-NLS-1$
//		g.setColor(Color.red);
//		g.drawRect(x, y, (int)r.getWidth(), (int)r.getHeight());
		if(D)
			text = text.substring(0,text.length() - "&&".length()); //$NON-NLS-1$
		ArrayList<String> lines = new ArrayList<>();
		while(text.length() > l) {
			int index = text.substring(0,l).lastIndexOf(' ');
			String a = text.substring(0, index != -1 ? index : l > text.length() ? text.length() : l);
			lines.add(a);
			if(!lines.isEmpty()) {
				g.setFont(scaleFont(a,r,g,new Font("impact", 0, 27))); //$NON-NLS-1$
				if(direction)
					y -= g.getFontMetrics().getHeight();
				else
					y += g.getFontMetrics().getHeight();
			}
			text = text.substring(index != -1 ? index : l > text.length() ? text.length() : l);
		} 
		if(text.length() > 0)
			lines.add(text);
		if(!direction)
			Collections.reverse(lines);
		for (String line : lines) {
			if(D) {
				Color top_color = new Color(200, 200, 200);
				Color side_color = new Color(100, 100, 100);
				TextGraphicUtil.draw3D(line, x, y, top_color, side_color, Color.yellow, g);
			} else
				TextGraphicUtil.drawOutline(line, x, y, Color.white, Color.black, g);
			if(!direction)
				y -= g.getFontMetrics().getHeight();
			else
				y += g.getFontMetrics().getHeight();
		}
	}

	private static Font scaleFont(String text, Rectangle rect, Graphics g, Font pFont) {
		float fontSize = 27.0f;
		Font font = pFont;

		font = pFont.deriveFont(fontSize);
		float width = g.getFontMetrics(font).stringWidth(text);
		if(width > rect.getWidth())
			fontSize = (rect.width / width ) * fontSize;
		return pFont.deriveFont(fontSize);
	}
}
