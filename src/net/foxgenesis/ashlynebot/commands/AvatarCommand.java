package net.foxgenesis.ashlynebot.commands;

import static net.foxgenesis.util.helper.DiscordHelper.ERROR;
import static net.foxgenesis.util.helper.DiscordHelper.outputMessageDelete;

import net.foxgenesis.discord.command.Command;
import net.foxgenesis.discord.command.IPlusCommand;
import net.foxgenesis.util.helper.DiscordHelper;
import sx.blah.discord.handle.obj.IMessage;
import sx.blah.discord.handle.obj.IUser;

@Command(name = "avatar", help = "Display the avatar of a user")
public class AvatarCommand implements IPlusCommand {

	@Override
	public void handleCommand(IMessage message, String inputs) throws Exception {
		if(message.getMentions().size() > 0) {
			DiscordHelper.tryToDelete(message);
			IUser user = message.getMentions().get(0);
			message.getChannel().sendMessage(DiscordHelper.getDefaultRoleEmbed(user, 
					message.getGuild()).withTitle("Avatar").withImage(user.getAvatarURL()).build()); //$NON-NLS-1$
		} else outputMessageDelete(message,Messages.getString("AvatarCommand.0"),ERROR); //$NON-NLS-1$
	}

}
