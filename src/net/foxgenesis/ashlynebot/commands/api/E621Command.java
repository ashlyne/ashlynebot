package net.foxgenesis.ashlynebot.commands.api;

import java.awt.Color;

import net.foxgenesis.ashlynebot.commands.api.AbstractAPIHelper.APIPost;
import net.foxgenesis.ashlynebot.commands.api.AbstractAPIHelper.PostType;
import net.foxgenesis.discord.command.Command;
import net.foxgenesis.discord.command.IPlusCommand;
import net.foxgenesis.util.helper.DiscordHelper;
import sx.blah.discord.api.internal.json.objects.EmbedObject;
import sx.blah.discord.handle.obj.IMessage;
import sx.blah.discord.util.EmbedBuilder;

@Command(name = "e621", help = "Gets a random image from e621.net", helpParams="<tag> [moreTag...]", cooldown = true, isNSFW = true)
public class E621Command implements IPlusCommand {
	private static final AbstractAPIHelper helper = new E621Helper();

	@Override
	public void handleCommand(IMessage message, String inputs) throws Exception {
		if(inputs != null && inputs.equalsIgnoreCase("TOGGLE_TIME_FILTER")) { //$NON-NLS-1$
			if(!(message.getAuthor().equals(message.getClient().getApplicationOwner()) || message.getAuthor().equals(message.getGuild().getOwner()))) {
				message.reply(Messages.getString("E621Command.0")); //$NON-NLS-1$
				return;
			}
			message.reply(Messages.getString("E621Command.1") + helper.toggleFilter(message.getGuild())); //$NON-NLS-1$
			return;
		}
		APIPost post = helper.getPost(message,inputs);

		if(post == null) 
			message.reply(Messages.getString("E621Command.2")); //$NON-NLS-1$
		else {
			EmbedObject embed = createEmbed(message,post);
			message.getChannel().sendMessage(embed);
		}
	}

	private static EmbedObject createEmbed(IMessage message, APIPost post) {
		if(post == null)
			return null;
		System.err.println(post.link);
		EmbedBuilder b = DiscordHelper.getDefaultUserEmbed(message.getAuthor()).withColor(Color.pink).withTitle("Search Result").withUrl(post.link); //$NON-NLS-1$
		if(post.type == PostType.PICTURE) 
			b.withImage(post.link);
		else
			b.withThumbnail(post.thumbnail);

		b.withFooterText("*****   If it does not display, click the title.   *****"); //$NON-NLS-1$
		b.appendField("Tags",post.tags,true); //$NON-NLS-1$
		b.appendField("Query",post.query != null? post.query : "N/A",true); //$NON-NLS-1$ //$NON-NLS-2$
		EmbedObject obj = b.build();
		if(post.type == PostType.VIDEO)
			obj.video = new EmbedObject.VideoObject(post.link, 500,500);
		return obj;
	}
}
