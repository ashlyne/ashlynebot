package net.foxgenesis.ashlynebot.commands.api;

import static net.foxgenesis.util.helper.DiscordHelper.getFilterForGuild;

import java.io.IOException;

import javax.xml.parsers.DocumentBuilderFactory;

import net.foxgenesis.ashlynebot.config.BooleanField;
import net.foxgenesis.util.TimeFilterSelector;
import sx.blah.discord.handle.obj.IGuild;
import sx.blah.discord.handle.obj.IMessage;

public abstract class AbstractAPIHelper {
	protected static DocumentBuilderFactory f = DocumentBuilderFactory.newInstance();
	
	private final BooleanField FILTER;
	private final String name;
	public AbstractAPIHelper(String name) {
		this.name = name;
		this.FILTER = new BooleanField(name+".filter", true, true); //$NON-NLS-1$
	}
	
	protected boolean useFilter(IGuild g) {return this.FILTER.optFrom(g);}
	protected <T> TimeFilterSelector<T> getFilter(IGuild guild) {
		return getFilterForGuild(guild,this.name+".data"); //$NON-NLS-1$
	}
	protected void setFilterState(IGuild guild, boolean state) {
		this.FILTER.set(guild, state);
	}
	protected boolean toggleFilter(IGuild guild) {
		return this.FILTER.toggle(guild);
	}
	public abstract APIPost getPost(IMessage message, String query) throws IOException;
	
	public static class APIPost {
		public final PostType type;
		public final String link,thumbnail,tags,query;
		public APIPost(PostType type, String link, String thumbnail, String tags, String query) {
			this.type = type;
			this.link = link;
			this.thumbnail = thumbnail;
			this.tags = tags;
			this.query = query;
		}
	}
	
	public static enum PostType {
		VIDEO,PICTURE
	}
}
