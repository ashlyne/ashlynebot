package net.foxgenesis.ashlynebot.commands.api;

import static net.foxgenesis.ashlynebot.Bot.rand;

import java.io.IOException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;

import javax.xml.parsers.ParserConfigurationException;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import net.foxgenesis.util.TimeFilterSelector;
import net.foxgenesis.util.helper.WebHelper;
import sx.blah.discord.handle.obj.IMessage;

@SuppressWarnings("nls")
public class E621Helper extends AbstractAPIHelper {
	private static final int URL=0,THUMBNAIL=1,TAGS=2;
	private static final String SITE_URL = "https://e621.net/post/index.xml?limit=100";
	
	public E621Helper() {
		super("e621");
	}
	
	@Override
	public APIPost getPost(IMessage message, String tags) throws IOException {
		String tags2 = tags == null? "" : "&tags=" + URLEncoder.encode(tags, "UTF-8");
		HttpResponse response = WebHelper.sendPost(SITE_URL + tags2,new ArrayList<NameValuePair>(0));
		try {
			Document d =  f.newDocumentBuilder().parse(response.getEntity().getContent());
			Node n = getRandomNode(message,d);
			String[] data = new String[3];
			NodeList ll = n.getChildNodes();
			for(int i=0; i<ll.getLength(); i++) {
				Node nn = ll.item(i);
				String name = nn.getNodeName();
				switch(name) {
				default:continue;
				case "tags":
					data[TAGS] = nn.getTextContent();
					break;
				case "file_url":
					data[URL] = nn.getTextContent();
					break;
				case "sample_url":
					data[THUMBNAIL] = nn.getTextContent();
					break;
				}
				if(!(data[URL] == null || data[THUMBNAIL] == null || data[TAGS] == null))
					break;
			}
			return new APIPost(data[URL].endsWith(".webm")?PostType.VIDEO:PostType.PICTURE, 
					data[URL], data[THUMBNAIL], data[TAGS], tags);
		} catch (UnsupportedOperationException | SAXException | ParserConfigurationException e) {
			System.err.println("Failed getting porn");
			e.printStackTrace();
			return null;
		}
	}

	private Node getRandomNode(IMessage message, Document d) {
		HashMap<String,Node> ss = new HashMap<>();
		NodeList l = d.getElementsByTagName("post");
		for(int i=0; i<l.getLength(); i++) {
			Node n = l.item(i);
			NodeList ll = n.getChildNodes();
			for(int j=0; j<ll.getLength(); j++) 
				if(ll.item(j).getNodeName().equals("id")) {
					ss.put(ll.item(j).getTextContent(), n);
					break;
				}
		}
		
		if(useFilter(message.getGuild())) {
			TimeFilterSelector<String> s = getFilter(message.getGuild());
			String ew = s.getRandomItem(ss.keySet());
			s.addData(ew);
			return ss.get(ew);
		}
		return ss.size() > 0 ? ss.values().toArray(new Node[]{})[rand.nextInt(ss.size())] : null;
	}
}
