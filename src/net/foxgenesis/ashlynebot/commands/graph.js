var pi = Math.PI,PI = pi;
var e = Math.E,E = e;
function sin(x) {
	return Math.sin(x);
}

function cos(x) {
	return Math.cos(x);
}

function tan(x) {
	return Math.tan(x);
}

function csc(x) {
	return 1/sin(x);
}

function sec(x) {
	return 1/cos(x);
}

function cot(x) {
	return 1/tan(x);
}

function abs(x) {
	return Math.abs(x);
}

function pow(a,b) {
	return Math.pow(a,b);
}

function round(x) {
	return Math.round(x);
}

//function calc(i,l) {
	//return algebra.parse("y=" + i).solveFor("y").eval({x: new Fraction((l * 100) | 0,100)}).toString();
//}