package net.foxgenesis.ashlynebot.commands;

import static net.foxgenesis.util.helper.DiscordHelper.ERROR;
import static net.foxgenesis.util.helper.DiscordHelper.outputMessageDelete;

import java.util.EnumSet;
import java.util.List;

import net.foxgenesis.discord.command.Command;
import net.foxgenesis.discord.command.IPlusCommand;
import sx.blah.discord.handle.obj.IGuild;
import sx.blah.discord.handle.obj.IMessage;
import sx.blah.discord.handle.obj.IUser;
import sx.blah.discord.handle.obj.IVoiceChannel;
import sx.blah.discord.handle.obj.Permissions;

@Command(name = "massmove", help = "Mass move users in voice channels", helpParams = "<Destination Channel Name>", isOwnerCommand = true, isSuperCommand = true)
public class MassMoveCommand implements IPlusCommand {

	@Override
	public void handleCommand(IMessage message, String inputs) throws Exception {
		IUser user = message.getAuthor();
		IGuild guild = message.getGuild();
		EnumSet<Permissions> j =  user.getPermissionsForGuild(guild);
		if(j.contains(Permissions.VOICE_MOVE_MEMBERS)) {
			List<IVoiceChannel> channels = guild.getVoiceChannelsByName(inputs);
			if(channels.size() > 1 || channels.isEmpty()) {
				outputMessageDelete(message,Messages.getString("MassMoveCommand.0"), ERROR); //$NON-NLS-1$
				return;
			}
			IVoiceChannel c = channels.get(0), o = user.getVoiceStateForGuild(guild).getChannel();
			if(o != null)
				o.getConnectedUsers().stream().forEach(u -> u.moveToVoiceChannel(c));
			else
				outputMessageDelete(message,Messages.getString("MassMoveCommand.1"), ERROR); //$NON-NLS-1$
		} else outputMessageDelete(message,Messages.getString("MassMoveCommand.2"), ERROR); //$NON-NLS-1$
	}

}
