package net.foxgenesis.ashlynebot.commands;

import static net.foxgenesis.util.helper.DiscordHelper.ERROR;
import static net.foxgenesis.util.helper.DiscordHelper.getFilterForGuild;
import static net.foxgenesis.util.helper.DiscordHelper.outputMessageDelete;

import java.awt.Color;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;

import javax.imageio.ImageIO;
import javax.xml.parsers.DocumentBuilderFactory;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import net.foxgenesis.ashlynebot.config.BooleanField;
import net.foxgenesis.discord.command.Command;
import net.foxgenesis.discord.command.IPlusCommand;
import net.foxgenesis.util.TimeFilterSelector;
import net.foxgenesis.util.helper.DiscordHelper;
import net.foxgenesis.util.helper.WebHelper;
import sx.blah.discord.Discord4J;
import sx.blah.discord.api.internal.json.objects.EmbedObject;
import sx.blah.discord.handle.obj.IGuild;
import sx.blah.discord.handle.obj.IMessage;
import sx.blah.discord.util.EmbedBuilder;

@Command(name = "rule34", help = "Gets a random image from rule34.xxx", helpParams="<tag> [moreTag...]", cooldown = true, isNSFW = true)
public class Rule34Command implements IPlusCommand {
	private static DocumentBuilderFactory f = DocumentBuilderFactory.newInstance();
	private static final Random rand = new Random();
	private static final String[] SITES = {"https://rule34.xxx"}; //"https://gelbooru.com" //$NON-NLS-1$
	private static final String BASE_URL = "%s/index.php?page=dapi&s=post&q=index&limit=100"; //$NON-NLS-1$
	private static BooleanField SWITCH_FILTER = new BooleanField("rule34.filter",true,true); //$NON-NLS-1$

	@Override
	public void handleCommand(IMessage message, String inputs) throws Exception {
		if(inputs != null && inputs.equalsIgnoreCase("TOGGLE_TIME_FILTER")) { //$NON-NLS-1$
			if(!(message.getAuthor().equals(message.getClient().getApplicationOwner()) || message.getAuthor().equals(message.getGuild().getOwner()))) {
				outputMessageDelete(message,Messages.getString("Rule34Command.4"),ERROR); //$NON-NLS-1$ 
				return;
			}
			boolean l = SWITCH_FILTER.toggle(message.getGuild());
			outputMessageDelete(message,Messages.getString("Rule34Command.5") + !l,ERROR); //$NON-NLS-1$ 
			return;
		}
		getRandomPost(message,inputs,5);
	}

	private static void getRandomPost(IMessage message, String tags, int count) throws Exception {
		Document[] d = getDocument(tags);
		Node o = getRandomNode(message.getGuild(),d);
		if(o == null) {
			outputMessageDelete(message,Messages.getString("Rule34Command.7"),ERROR); //$NON-NLS-1$ 
			return;
		}
		String r = o.getAttributes().getNamedItem("file_url").getTextContent(); //$NON-NLS-1$
		if(r == null || r.isEmpty() || count<=0) 
			outputMessageDelete(message,Messages.getString("Rule34Command.10"),ERROR); //$NON-NLS-1$ 
		else
			message.getChannel().sendMessage(createEmbed(o,r,message));
	}

	private static Node getRandomNode(IGuild g,Document[] d) {
		ArrayList<String> sl = new ArrayList<>();
		ArrayList<Node> sn = new ArrayList<>();

		TimeFilterSelector<String> s = getFilterForGuild(g,"rule34.filter"); //$NON-NLS-1$
		for(Document da: d) {
			NodeList l = da.getElementsByTagName("post"); //$NON-NLS-1$
			for(int i=0; i<l.getLength(); i++) {
				Node n = l.item(i);
				sn.add(n);
				sl.add(n.getAttributes().getNamedItem("id").getTextContent()); //$NON-NLS-1$
			}
		}
		if(SWITCH_FILTER.optFrom(g)) {
			Collections.sort(sl);
			String ew = s.getRandomItem(sl);
			s.addData(ew);
			Node ns = sn.stream().filter(n -> n.getAttributes().getNamedItem("id").getTextContent().equals(ew)).findAny().orElse(null); //$NON-NLS-1$
			return ns;
		}
		return sn.size() > 0 ? sn.get(rand.nextInt(sn.size())) : null;
	}

	private static Document[] getDocument(String tags) throws UnsupportedOperationException, IOException {
		Document[] out = new Document[SITES.length];
		tags = tags == null? "" : "&tags=" + URLEncoder.encode(tags, "UTF-8"); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
		for(int i=0; i<SITES.length; i++) {
			try {
				HttpResponse response = WebHelper.sendPost(String.format(BASE_URL,SITES[i]) + tags,new ArrayList<NameValuePair>(0));
				out[i] =  f.newDocumentBuilder().parse(response.getEntity().getContent());
			} catch(Exception e) {
				Discord4J.LOGGER.error("Error while getting porn",e); //$NON-NLS-1$
			}
		}
		return out;
	}

	private static EmbedObject createEmbed(Node n, String url, IMessage message) {
		System.err.println(url);
		NamedNodeMap m = n.getAttributes();
		EmbedBuilder b = DiscordHelper.getDefaultUserEmbed(message.getAuthor()).withColor(Color.pink).withTitle(Messages.getString("Rule34Command.0")).withUrl(url); //$NON-NLS-1$
		if(!url.endsWith(".webm"))  //$NON-NLS-1$
			b.withImage(url);
		else
			b.withThumbnail("http:" + m.getNamedItem("preview_url").getTextContent()); //$NON-NLS-1$ //$NON-NLS-2$
		String query = message.getContent().substring("+rule34".length()).trim(); //$NON-NLS-1$
		b.withFooterText(Messages.getString("Rule34Command.1")); //$NON-NLS-1$
		
		String sll = m.getNamedItem("tags").getTextContent(); //$NON-NLS-1$
		b.appendField("Tags", sll.substring(0,sll.length() > 1023? 1023: sll.length()),true); //$NON-NLS-1$
		b.appendField("Query",query.isEmpty()? "N/A": query,true); //$NON-NLS-1$ //$NON-NLS-2$
		b.appendField("Score", m.getNamedItem("score").getTextContent(), true); //$NON-NLS-1$ //$NON-NLS-2$
		b.appendField("ID",m.getNamedItem("id").getTextContent(),true); //$NON-NLS-1$ //$NON-NLS-2$
		File f = new File("porn"); //$NON-NLS-1$
		f.mkdir();
		try {
			BufferedImage img = ImageIO.read(new URL(url));
			ImageIO.write(img, "PNG", new File(f,m.getNamedItem("id").getTextContent() + ".png")); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		EmbedObject obj = b.build();
		if(url.endsWith(".webm")) //$NON-NLS-1$
			obj.video = new EmbedObject.VideoObject(url, Integer.parseInt(m.getNamedItem("preview_width").getTextContent()), Integer.parseInt(m.getNamedItem("preview_height").getTextContent())); //$NON-NLS-1$ //$NON-NLS-2$
		return obj;
	}
}
