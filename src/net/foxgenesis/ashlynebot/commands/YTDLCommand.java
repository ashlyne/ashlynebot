package net.foxgenesis.ashlynebot.commands;

import static net.foxgenesis.util.helper.DiscordHelper.ERROR;
import static net.foxgenesis.util.helper.DiscordHelper.INFO;
import static net.foxgenesis.util.helper.DiscordHelper.outputMessageDelete;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;

import net.foxgenesis.discord.command.Command;
import net.foxgenesis.discord.command.IPlusCommand;
import net.foxgenesis.util.helper.ByteHelper;
import net.foxgenesis.util.helper.WebHelper;
import sx.blah.discord.handle.obj.IMessage;

@Command(name = "ytdl", help = "Converts a youtube video to a mp3", helpParams = "<Youtube URL>", cooldown = true)
public class YTDLCommand implements IPlusCommand {
	private static final String YTDL_URL = "http://foxgenesis.net/tools/youtube/ytdl.php?t=inline&url=%s"; //$NON-NLS-1$

	@Override
	public void handleCommand(IMessage message, String inputs) {
		String[] in = inputs.split(" "); //$NON-NLS-1$
		if(in.length == 1) {
			try {
				outputMessageDelete(message,Messages.getString("YTDLCommand.0"),INFO); //$NON-NLS-1$ 
				File f = File.createTempFile("YTDLConversion", ".mp3"); //$NON-NLS-1$ //$NON-NLS-2$
				if(downloadVideo(in[0].trim(), f)) {
					if(f.length() > 0x800000)
						outputMessageDelete(message,Messages.getString("YTDLCommand.1") + ByteHelper.toShortSizeString(f.length()),ERROR); //$NON-NLS-1$ 
					else {
						outputMessageDelete(message,Messages.getString("YTDLCommand.2"),INFO); //$NON-NLS-1$ 
						message.getChannel().sendFile(f);
					}
				} else outputMessageDelete(message,Messages.getString("YTDLCommand.3"),ERROR); //$NON-NLS-1$ 
			} catch(Exception e) {
				e.printStackTrace();
			}
		}
	}

	private static boolean downloadVideo(String youtubeUrl, File f) throws ClientProtocolException, IOException {
		if(!isValidYTLink(youtubeUrl))
			return false;
		HttpResponse r = WebHelper.sendGet(String.format(YTDL_URL, youtubeUrl));
		try(FileOutputStream out = new FileOutputStream(f)) {
			r.getEntity().writeTo(out);
			return true;
		} catch(Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	private static boolean isValidYTLink(String url) {
		return url.matches("(?:https?:\\/\\/)?(?:www\\.)?youtu\\.?be(?:\\.com)?\\/?.*(?:watch|embed)?(?:.*v=|v\\/|\\/)([\\w\\-_]+)\\&?"); //$NON-NLS-1$
	}
}
