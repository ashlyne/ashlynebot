package net.foxgenesis.ashlynebot.commands;

import static net.foxgenesis.ashlynebot.Bot.delete;
import static net.foxgenesis.util.helper.DiscordHelper.ERROR;
import static net.foxgenesis.util.helper.DiscordHelper.INFO;
import static net.foxgenesis.util.helper.DiscordHelper.outputMessageDelete;

import java.util.concurrent.ConcurrentHashMap;

import net.foxgenesis.ashlynebot.Bot;
import net.foxgenesis.discord.command.Command;
import net.foxgenesis.discord.command.IPlusCommand;
import net.foxgenesis.discord.guild.ObjectKey;
import net.foxgenesis.util.helper.AnimePlanetHelper;
import net.foxgenesis.util.helper.AnimePlanetHelper.AnimeProfile;
import net.foxgenesis.util.helper.DiscordHelper;
import sx.blah.discord.handle.obj.IMessage;
import sx.blah.discord.handle.obj.IUser;
import sx.blah.discord.util.EmbedBuilder;

@Command(name = "anime", help = "register or lookup an anime profile", helpParams = "<register ANIME_PLANET_USERNAME (i.e. register ashlyne) | remove | @USER>")
public class AnimePlanetCommand implements IPlusCommand {
	private static ObjectKey<ConcurrentHashMap<Long, String>> obj = Bot.getData().getObject("anime", new ConcurrentHashMap<Long,String>()); //$NON-NLS-1$
	
	@Override
	public void handleCommand(IMessage message, String inputs) throws Exception {
		if(!message.getMentions().isEmpty()) {
			ConcurrentHashMap<Long,String> f = obj.object;
			message.getMentions().forEach(user -> {
				if(f.containsKey(user.getLongID())) {
					AnimeProfile p = AnimePlanetHelper.getProfile(f.get(user.getLongID()));
					if(p == null)
						delete(message.reply(Messages.getString("AnimePlanetCommand.1"))); //$NON-NLS-1$
					else
						displayAnimeProfile(p,message);
				} else 
					delete(message.reply(Messages.getString("AnimePlanetCommand.2") + user.mention())); //$NON-NLS-1$
			});
			return;
		}
		
		if(inputs != null) {
			int index = inputs.indexOf(' ');
			String a = inputs,b=null;
			if(index != -1) {
				a = inputs.substring(0,index);
				b = inputs.substring(index).trim();
			}
			if(a.length() > 1) {
				IUser user = message.getAuthor();
				ConcurrentHashMap<Long,String> f = obj.object;
				switch(a.toLowerCase().trim()) {
				default: outputMessageDelete(message,Messages.getString("AnimePlanetCommand.4"), ERROR); return; //$NON-NLS-1$ 
				case "register": //$NON-NLS-1$
					if(f.containsKey(user.getLongID())) {
						outputMessageDelete(message,Messages.getString("AnimePlanetCommand.7"), ERROR); //$NON-NLS-1$ 
						return;
					}
					try {
						String profileName = b;
						if(AnimePlanetHelper.isAnimeProfileValid(profileName)) {
							f.put(user.getLongID(), profileName);
							obj.update();
							delete(message.reply(Messages.getString("AnimePlanetCommand.8") + profileName)); //$NON-NLS-1$
						} else 
							outputMessageDelete(message,Messages.getString("AnimePlanetCommand.10"), INFO); //$NON-NLS-1$ 
					} catch(Exception e) {
						outputMessageDelete(message,Messages.getString("AnimePlanetCommand.12"), ERROR); //$NON-NLS-1$ 
						return;
					}
					break;
				case "remove": //$NON-NLS-1$
					if(f.containsKey(user.getLongID())) {
						f.remove(user.getLongID());
						obj.update();
						outputMessageDelete(message,Messages.getString("AnimePlanetCommand.15"), INFO); //$NON-NLS-1$ 
						return;
					}
					outputMessageDelete(message,Messages.getString("AnimePlanetCommand.17"), ERROR); //$NON-NLS-1$ 
					return;
				}
			}
		} else
			outputMessageDelete(message,Messages.getString("AnimePlanetCommand.18"),ERROR); //$NON-NLS-1$
	}
	
	
	private static final void displayAnimeProfile(AnimeProfile p, IMessage m) {
		EmbedBuilder b = DiscordHelper.getDefaultRoleEmbed(m.getAuthor(), m.getGuild()).withTitle(p.user + "'s Anime Profile"); //$NON-NLS-1$
		b.withUrl("https://www.anime-planet.com/users/" + p.user); //$NON-NLS-1$
		
		StringBuilder bu = new StringBuilder();
		bu.append(DiscordHelper.getCustomEmoji("watched") + Messages.getString("AnimePlanetCommand.22") + p.watched + '\n'); //$NON-NLS-1$ //$NON-NLS-2$
		bu.append(DiscordHelper.getCustomEmoji("watching") + Messages.getString("AnimePlanetCommand.24") + p.watching + '\n'); //$NON-NLS-1$ //$NON-NLS-2$
		bu.append(DiscordHelper.getCustomEmoji("wantToWatch") + Messages.getString("AnimePlanetCommand.26") + p.wantToWatch + '\n'); //$NON-NLS-1$ //$NON-NLS-2$
		bu.append(DiscordHelper.getCustomEmoji("stalled") + Messages.getString("AnimePlanetCommand.28") + p.stalled + '\n'); //$NON-NLS-1$ //$NON-NLS-2$
		bu.append(DiscordHelper.getCustomEmoji("dropped") + Messages.getString("AnimePlanetCommand.30") + p.dropped + '\n'); //$NON-NLS-1$ //$NON-NLS-2$
		bu.append(DiscordHelper.getCustomEmoji("noWatch") + Messages.getString("AnimePlanetCommand.32") + p.noWatch + '\n'); //$NON-NLS-1$ //$NON-NLS-2$
		
		b.withDesc(bu.toString());
		b.appendField(Messages.getString("AnimePlanetCommand.33"), (int)p.hours + "", true); //$NON-NLS-1$ //$NON-NLS-2$
		b.appendField(Messages.getString("AnimePlanetCommand.35"), p.totalEp + "", true); //$NON-NLS-1$ //$NON-NLS-2$
		m.getChannel().sendMessage(b.build());
	}
}
