package net.foxgenesis.ashlynebot.commands;

import net.foxgenesis.discord.command.Command;
import net.foxgenesis.discord.command.IPlusCommand;
import net.foxgenesis.util.helper.DiscordHelper;
import sx.blah.discord.handle.obj.IMessage;

@Command(name = "tobinary", help = "Converts text to binary", helpParams = "<text...>")
public class BinaryCommand implements IPlusCommand {

	@Override
	public void handleCommand(IMessage message, String inputs) {
		byte[] bytes = inputs.getBytes();
		StringBuilder binary = new StringBuilder();
		for (byte b : bytes) {
			int val = b;
			for (int i = 0; i < 8; i++) {
				binary.append((val & 128) == 0 ? 0 : 1);
				val <<= 1;
			}
			binary.append(' ');
		}
		DiscordHelper.splitToMessages(binary.toString(), 2000 - (message.getAuthor().mention().length() + 4), message::reply);
	}
}
