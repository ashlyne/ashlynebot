package net.foxgenesis.ashlynebot.commands;

import com.vdurmont.emoji.EmojiManager;

import net.foxgenesis.discord.command.Command;
import net.foxgenesis.discord.command.IPlusCommand;
import net.foxgenesis.util.helper.DiscordHelper;
import sx.blah.discord.handle.obj.IMessage;
import sx.blah.discord.util.EmbedBuilder;
import sx.blah.discord.util.RequestBuffer.IVoidRequest;

@Command(name = "vote", help = "Create a vote", helpParams = "<message>")
public class VoteCommand implements IPlusCommand {
	@Override
	public void handleCommand(IMessage message, String inputs) throws Exception {
		message.delete();
		EmbedBuilder b = DiscordHelper.getDefaultRoleEmbed(message.getAuthor(), message.getGuild());
		b.withDesc("\"" + message.getFormattedContent().substring(6) + "\""); //$NON-NLS-1$ //$NON-NLS-2$
		b.withTitle(Messages.getString("VoteCommand.0")); //$NON-NLS-1$
		b.withFooterText(Messages.getString("VoteCommand.1")); //$NON-NLS-1$
		b.withTimestamp(message.getTimestamp());
		//b.withDesc(message.getFormattedContent().substring(5));
		IMessage msg = message.getChannel().sendMessage(b.build());
		DiscordHelper.bufferOrdered(new IVoidRequest[]{() ->  msg.addReaction(EmojiManager.getForAlias("white_check_mark")),() -> msg.addReaction(EmojiManager.getForAlias("no_entry"))}); //$NON-NLS-1$ //$NON-NLS-2$
	}
}
