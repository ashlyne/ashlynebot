package net.foxgenesis.ashlynebot.commands;

import static net.foxgenesis.util.helper.DiscordHelper.ERROR;
import static net.foxgenesis.util.helper.DiscordHelper.getFilterForGuild;
import static net.foxgenesis.util.helper.DiscordHelper.outputMessageDelete;

import java.util.function.Predicate;

import net.foxgenesis.discord.command.Command;
import net.foxgenesis.discord.command.IPlusCommand;
import net.foxgenesis.util.TimeFilterSelector;
import net.foxgenesis.util.helper.RedditHelper;
import net.foxgenesis.util.helper.RedditHelper.RedditMissingException;
import net.foxgenesis.util.helper.RedditHelper.RedditPost;
import sx.blah.discord.handle.obj.IMessage;

@Command(name = "reddit", help = "Summon some dank memes from reddit.com/r/SUBREDDIT", cooldown = true)
public class RedditCommand implements IPlusCommand {
	private static final Predicate<RedditPost> filter = RedditHelper.NON_STICKY.and(RedditHelper.PICTURES_ONLY);
	
	@Override
	public void handleCommand(IMessage message, String inputs) throws Exception {
		if(inputs != null) {
			RedditHelper r = new RedditHelper(inputs);
			try {
				TimeFilterSelector<RedditPost> s = getFilterForGuild(message.getGuild(), "meme.filter"); //$NON-NLS-1$
				RedditPost re  = s.getRandomItem(r.getPosts(100,message.getChannel().isNSFW()? filter : filter.and(RedditHelper.NON_NSFW)));
				if(re != null) 
					//message.getChannel().sendFile(re.getEmbedObject().withImage("attachment://test." + re.extension).build(), WebHelper.sendGet(re.url).getEntity().getContent(), "test." + re.extension);
					message.reply(re.video? re.url : "", re.video? null : re.getEmbedObject().build()); //$NON-NLS-1$
				else
					outputMessageDelete(message,Messages.getString("RedditCommand.0"),ERROR); //$NON-NLS-1$ 
			} catch(RedditMissingException e) {
				outputMessageDelete(message,Messages.getString("RedditCommand.1") + inputs,ERROR); //$NON-NLS-1$ 
			}
		} else {
			outputMessageDelete(message,Messages.getString("RedditCommand.2"),ERROR); //$NON-NLS-1$ 
		}
	}
}
