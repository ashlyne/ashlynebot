package net.foxgenesis.ashlynebot.commands;
import net.foxgenesis.discord.command.Command;
import net.foxgenesis.discord.command.IPlusCommand;
import net.foxgenesis.util.helper.DiscordHelper;
import sx.blah.discord.api.internal.json.objects.EmbedObject;
import sx.blah.discord.handle.obj.IGuild;
import sx.blah.discord.handle.obj.IMessage;
import sx.blah.discord.handle.obj.IUser;
import sx.blah.discord.util.EmbedBuilder;

@Command(name = "wide", help = "Turns text into w i d e text.", helpParams = "<text...>")
public class WideCommand implements IPlusCommand {

	@Override
	public void handleCommand(IMessage message, String inputs) {
		if(inputs != null && !inputs.isEmpty()) {
			DiscordHelper.tryToDelete(message);
			DiscordHelper.splitToMessages(generateSymbolMessage(inputs),2000,"","","\t",":",t ->  //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$
				message.getChannel().sendMessage(getObject(t,message.getAuthor(),message.getGuild()))
			);
		}
	}

	private static String generateSymbolMessage(String in) {
		StringBuilder b = new StringBuilder();
		in.chars().forEachOrdered(i -> {
			b.append((char)i);
			b.append(' ');
		});
		b.deleteCharAt(b.lastIndexOf(" ")); //$NON-NLS-1$
		return b.toString();
	}
	
	private static EmbedObject getObject(String text, IUser user, IGuild guild) {
		return new EmbedBuilder().withDesc(text).withAuthorName(user.getName()).withAuthorIcon(user.getAvatarURL()).withColor(user.getColorForGuild(guild)).build();
	}
}
