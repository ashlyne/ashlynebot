package net.foxgenesis.ashlynebot.commands;

import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.image.BufferedImage;
import java.io.InputStream;

import net.foxgenesis.discord.command.Command;

@Command(name = "nova", isHidden = true, isSuperCommand = true)
public class SexyFishCommand extends AbstractImagePainter {

	public SexyFishCommand() {
		super(false, "fish"); //$NON-NLS-1$
	}

	@Override
	InputStream getImage() {
		return SexyFishCommand.class.getResourceAsStream("fish.jpg"); //$NON-NLS-1$
	}

	@Override
	void paint(Graphics2D g, String top, String bottom, BufferedImage image) {
		Rectangle rec = new Rectangle(10, image.getHeight()-80, image.getWidth() - 10, image.getHeight());
		AbstractImagePainter.drawString(g, top, 5, image.getHeight() - 10, image.getWidth(), image.getHeight(), rec, true);
	}
}
