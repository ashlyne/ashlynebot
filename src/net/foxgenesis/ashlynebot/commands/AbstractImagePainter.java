package net.foxgenesis.ashlynebot.commands;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.RenderingHints;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collections;

import javax.imageio.ImageIO;

import net.foxgenesis.discord.command.IPlusCommand;
import net.foxgenesis.util.TextGraphicUtil;
import net.foxgenesis.util.helper.DiscordHelper;
import sx.blah.discord.handle.obj.IMessage;

@SuppressWarnings("nls")
public abstract class AbstractImagePainter implements IPlusCommand {
	private static final boolean DEBUG = false;
	private final boolean bottom;
	private final String name;
	private final File outputF;
	
	public AbstractImagePainter(boolean bottom, String name) {
		this.bottom = bottom;
		this.name = name;
		this.outputF = new File(this.name + ".png");
	}
	
	abstract InputStream getImage();
	abstract void paint(Graphics2D g, String top, String bottom, BufferedImage image);
	
	@Override
	public void handleCommand(IMessage message, String inputs) throws Exception {
		handle(message, inputs);
	}
	
	private void handle(IMessage message, String inputs) throws IOException {
		String top,bottom = null;
		if(this.bottom) {
			String[] l = inputs.split("@@",2);
			top = l[0];
			if(l.length > 1)
				bottom = l[1];
		} else top = inputs;
		
		BufferedImage image = ImageIO.read(getImage());
		Graphics2D g = image.createGraphics();
		
		g.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
				RenderingHints.VALUE_ANTIALIAS_ON);

		g.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING,
				RenderingHints.VALUE_TEXT_ANTIALIAS_ON);

		g.setRenderingHint(RenderingHints.KEY_RENDERING,
				RenderingHints.VALUE_RENDER_QUALITY);
		g.setFont(new Font("impact", 0, 27));
		g.setColor(Color.white);
		g.setStroke(new BasicStroke(3));
		paint(g,top,bottom,image);
		g.dispose();
		
		ImageIO.write(image, "PNG", this.outputF);
		DiscordHelper.tryToDelete(message);
		message.getChannel().sendFile(DiscordHelper.getDefaultRoleEmbed(message.getAuthor(), message.getGuild()).withImage("attachment://" + this.name + ".png").withFooterText("via " + this.name + " command").build(),this.outputF);
	}

	@SuppressWarnings("unused")
	protected static void drawString(Graphics2D g, String text, int x, int y, int width, int height, Rectangle r, boolean direction) {
		final int l = 15;
		boolean D = text.endsWith("&&");
		if(DEBUG) {
			g.setColor(Color.red);
			g.drawRect((int)r.getX(), (int)r.getY(), (int)r.getWidth(), (int)r.getHeight());
		}
		if(D)
			text = text.substring(0,text.length() - "&&".length());
		String oT = text;
		ArrayList<String> lines = new ArrayList<>();
		while(text.length() > l) {
			int index = text.substring(0,l).lastIndexOf(' ');
			String a = text.substring(0, index != -1 ? index : l > text.length() ? text.length() : l);
			lines.add(a);
			text = text.substring(index != -1 ? index : l > text.length() ? text.length() : l);
			System.err.println("Test test test");
		}
		if(text.length() > 0)
			lines.add(text);
		g.setFont(scaleFont(lines.size(),new Font("impact", 0, 27)));
		if(g.getFont().getSize() != 27) {
			int ll;
			switch(g.getFont().getSize()) {
			case 25: ll = 20; break;
			case 20: ll = 25; break;
			default: ll = 40; break;
			}
			text = oT;
			lines.clear();
			while(text.length() > ll) {
				int index = text.substring(0,ll).lastIndexOf(' ');
				String a = text.substring(0, index != -1 ? index : ll > text.length() ? text.length() : ll);
				lines.add(a);
				text = text.substring(index != -1 ? index : ll > text.length() ? text.length() : ll);
			}
			if(text.length() > 0)
				lines.add(text);
		}
		for(int i=1; i<lines.size(); i++)
			if(direction)
				y -= g.getFontMetrics().getHeight();
			else
				y += g.getFontMetrics().getHeight();
		if(!direction)
			Collections.reverse(lines);
		for (String line : lines) {
			if(D) {
				Color top_color = new Color(200, 200, 200);
				Color side_color = new Color(100, 100, 100);
				TextGraphicUtil.draw3D(line, x, y, top_color, side_color, Color.yellow, g);
			} else
				TextGraphicUtil.drawOutline(line, x, y, Color.white, Color.black, g);
			if(!direction)
				y -= g.getFontMetrics().getHeight();
			else
				y += g.getFontMetrics().getHeight();
		}
	}

	private static Font scaleFont(int lines, Font pFont) {
		float fontSize;
		switch(lines) {
		case 1: case 2: fontSize = 27.0f;break;
		case 3: fontSize = 25.0f; break;
		case 4: fontSize = 25.0f; break;
		case 5: fontSize = 20.0f; break;
		default: fontSize = 14.0f; break;
		}
		return pFont.deriveFont(fontSize);
	}
}
