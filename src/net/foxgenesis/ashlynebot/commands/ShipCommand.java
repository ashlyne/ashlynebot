package net.foxgenesis.ashlynebot.commands;

import java.awt.Graphics;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

import javax.imageio.ImageIO;

import com.vdurmont.emoji.EmojiManager;

import net.foxgenesis.ashlynebot.input.ConsoleCommand;
import net.foxgenesis.ashlynebot.input.InputManager;
import net.foxgenesis.discord.command.Command;
import net.foxgenesis.discord.command.IPlusCommand;
import net.foxgenesis.util.helper.DiscordHelper;
import sx.blah.discord.handle.obj.IGuild;
import sx.blah.discord.handle.obj.IMessage;
import sx.blah.discord.handle.obj.IUser;
import sx.blah.discord.handle.obj.StatusType;
import sx.blah.discord.util.RequestBuffer.IVoidRequest;

@Command(name = "ship", help = "Declare the best couple", helpParams = "[@USER A][@USER B]")
public class ShipCommand implements IPlusCommand {
	private final static Random rand = new Random();
	private static Image heart;
	private static final File outputF = new File("ship.png"); //$NON-NLS-1$
	private static boolean RIGGED = false;

	static {
		try {
			heart = ImageIO.read(ShipCommand.class.getResourceAsStream("heart.png")); //$NON-NLS-1$
		} catch (IOException e) {
			e.printStackTrace();
		}
		InputManager.registerConsoleCommand(ConsoleCommand.newCommand("rigship", (inputs, split) -> { //$NON-NLS-1$
			RIGGED = !RIGGED;
			System.out.println("[ShipCommand]: Changed rigging to: " + RIGGED); //$NON-NLS-1$
			return false;
		}));
	}

	@Override
	public void handleCommand(IMessage message, String inputs) throws Exception {
		IUser a,b,c = null;
		List<IUser> mentions = message.getMentions();
		if(message.getFormattedContent().contains("@everyone") || message.getFormattedContent().contains("@here")) //$NON-NLS-1$ //$NON-NLS-2$
			mentions.clear();

		if(mentions.size() > 2) {
			a = mentions.get(0);
			b = mentions.get(1);
			c = mentions.get(2);
		} else if(mentions.size() > 1) {
			a = mentions.get(0);
			b = mentions.get(1);
		} else if(mentions.size() > 0) {
			a = mentions.get(0);
			if(RIGGED) {
				if(a.getLongID() == 135129684374454273l)
					b = message.getGuild().getUserByID(112748859163156480l);
				else if(a.getLongID() == 112748859163156480l)
					b = message.getGuild().getUserByID(135129684374454273l);
				else
					b = getRandomUser(getOnlineMembers(message.getGuild()),a);
			} else b = getRandomUser(getOnlineMembers(message.getGuild()),a);
		} else {
			a = getRandomUser(getOnlineMembers(message.getGuild()),null);
			if(RIGGED) {
				if(a.getLongID() == 135129684374454273l)
					b = message.getGuild().getUserByID(112748859163156480l);
				else if(a.getLongID() == 112748859163156480l)
					b = message.getGuild().getUserByID(135129684374454273l);
				else
					b = getRandomUser(getOnlineMembers(message.getGuild()),a);
			} else b = getRandomUser(getOnlineMembers(message.getGuild()),a);
		}

		BufferedImage image = new BufferedImage(c == null? 1000 : 1500,300,BufferedImage.TYPE_INT_ARGB);
		Graphics g = image.createGraphics();

		if(c == null) {
			g.drawImage(DiscordHelper.getAvatar(a), 0, 0, image.getWidth()/3,image.getHeight(),null);
			g.drawImage(DiscordHelper.getAvatar(b), image.getWidth()/3*2, 0, image.getWidth()/3,image.getHeight(),null);
			g.drawImage(heart, image.getWidth()/3, 0, image.getWidth()/3,image.getHeight(), null);
		} else {
			g.drawImage(DiscordHelper.getAvatar(a), 0, 0, image.getWidth()/5,image.getHeight(),null);
			g.drawImage(heart, image.getWidth()/5, 0, image.getWidth()/5,image.getHeight(), null);
			g.drawImage(DiscordHelper.getAvatar(b), image.getWidth()/5*2, 0, image.getWidth()/5,image.getHeight(),null);
			g.drawImage(heart, image.getWidth()/5*3, 0, image.getWidth()/5,image.getHeight(), null);
			g.drawImage(DiscordHelper.getAvatar(c), image.getWidth()/5*4, 0, image.getWidth()/5,image.getHeight(),null);
		}
		g.dispose();

		final double shipValue = rand.nextDouble();

		char[] o = {'@','@','@','@','@'};
		o[(int) (shipValue / 0.2)] = '%';
		String output = new String(o).replaceAll("@", ":heavy_minus_sign:").replaceAll("%", ":radio_button:"); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$
		output += " " + Math.round(shipValue * 100) + "% chance"; //$NON-NLS-1$ //$NON-NLS-2$

		synchronized(outputF) {
			ImageIO.write(image, "PNG", outputF); //$NON-NLS-1$
			IMessage msg = message.getChannel().sendFile(DiscordHelper.getDefaultRoleEmbed(message.getAuthor(), 
					message.getGuild()).withTitle("I ship thee!").withImage("attachment://ship.png").withDesc(a.mention(true) + " :heart: " + b.mention(true) + (c != null? " :heart: " + c.mention(true) : "") + '\n' + output) //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$ //$NON-NLS-5$
					.withFooterText("via ship command").build(),outputF); //$NON-NLS-1$
			DiscordHelper.bufferOrdered(new IVoidRequest[]{() ->  msg.addReaction(EmojiManager.getForAlias("white_check_mark")),() -> msg.addReaction(EmojiManager.getForAlias("no_entry"))}); //$NON-NLS-1$ //$NON-NLS-2$
		}
	}

	private static IUser getRandomUser(List<IUser> users, IUser except) {
		users.sort((a,b) -> a.getStringID().compareTo(b.getStringID()));
		IUser a = users.get(rand.nextInt(users.size()));
		if(except != null && a.equals(except) && users.size() > 2)
			return getRandomUser(users,except);
		return a;
	}

	private static List<IUser> getOnlineMembers(IGuild guild) {
		return guild.getUsers().stream().filter(u -> u.getPresence().getStatus() == StatusType.ONLINE)
				.filter(u -> !u.isBot()).collect(Collectors.toList());
	}
}
