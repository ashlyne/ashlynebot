package net.foxgenesis.ashlynebot.commands;

import java.net.URLEncoder;

import net.foxgenesis.discord.command.Command;
import net.foxgenesis.discord.command.IPlusCommand;
import net.foxgenesis.util.helper.WebHelper;
import sx.blah.discord.handle.obj.IMessage;

@Command(name = "say", help = "Send a message to the GMod server", helpParams = "<text...>")
public class GModCommand implements IPlusCommand {
	@Override
	public void handleCommand(IMessage message, String inputs) throws Exception {
		if(inputs != null && !inputs.isEmpty()) {
			String s = WebHelper.readInputStream(WebHelper.sendPost(String.format("http://sirmeep.uk/gmoddiscord/fbkGModSay.php?mode=w&text=%s&username=%s",URLEncoder.encode(inputs,"UTF-8"),URLEncoder.encode(message.getAuthor().getName(),"UTF-8")),null)); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
			if(s != null && !s.isEmpty())
				message.reply("Error: " + s); //$NON-NLS-1$
			else
				message.reply("Message sent"); //$NON-NLS-1$
		}
		else
			message.reply("Message can not be empty"); //$NON-NLS-1$
	}
}
