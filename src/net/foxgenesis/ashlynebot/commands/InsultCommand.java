package net.foxgenesis.ashlynebot.commands;
import static net.foxgenesis.util.helper.DiscordHelper.ERROR;
import static net.foxgenesis.util.helper.DiscordHelper.INFO;
import static net.foxgenesis.util.helper.DiscordHelper.getFilterForGuild;
import static net.foxgenesis.util.helper.DiscordHelper.outputMessageDelete;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;
import java.util.function.Consumer;
import java.util.regex.Pattern;

import org.json.JSONObject;

import net.foxgenesis.discord.command.Command;
import net.foxgenesis.discord.command.CommandStorage;
import net.foxgenesis.discord.guild.SQLDataType;
import net.foxgenesis.discord.guild.StorageHandler;
import net.foxgenesis.util.TimeFilterSelector;
import net.foxgenesis.util.helper.DiscordHelper;
import net.foxgenesis.util.helper.JSONHelper;
import sx.blah.discord.handle.obj.IGuild;
import sx.blah.discord.handle.obj.IMessage;
import sx.blah.discord.handle.obj.IUser;

@Command(name = "it", help = "Insults a user.", helpParams = "<@mention>")
public class InsultCommand extends CommandStorage {

	public InsultCommand() {
		super("insults", SQLDataType.JSON.name, SQLDataType.JSON); //$NON-NLS-1$
	}

	private static final Pattern antiMention = Pattern.compile("ashlyne|@",Pattern.CASE_INSENSITIVE); //$NON-NLS-1$

	@Override
	protected void handleCommand(IMessage message, String inputs, StorageHandler h) throws Exception {
		if(inputs == null)
			return;
		String[] d = inputs.split(" "); //$NON-NLS-1$
		if(d.length >= 1) {
			String command = d[0];
			inputs = inputs.substring(command.length()).trim();
			if(command.equals("add")) { //$NON-NLS-1$
				if(inputs.contains("%s")) { //$NON-NLS-1$
					if(moreThanOne(inputs)) {
						outputMessageDelete(message,Messages.getString("InsultCommand.0"),ERROR); //$NON-NLS-1$ 
						return;
					}
					if(inputs.length() >= 1500) {
						outputMessageDelete(message,String.format(Messages.getString("InsultCommand.1"),inputs.length()),ERROR); //$NON-NLS-1$ 
						return;
					}
					if(message.getMentions().size() > 0 || antiMention.matcher(inputs).find()) {
						outputMessageDelete(message,Messages.getString("InsultCommand.2"),ERROR); //$NON-NLS-1$ 
						return;
					}
					if(inputs.length() < 30) {
						outputMessageDelete(message,Messages.getString("InsultCommand.3"),ERROR); //$NON-NLS-1$ 
						return;
					}
					GuildInsults i = new GuildInsults(h,message.getGuild());

					if(i.getAmountOfInsults() >= 50) {
						outputMessageDelete(message,Messages.getString("InsultCommand.4"),ERROR); //$NON-NLS-1$ 
						return;
					}

					String id;
					do 
						id = UUID.randomUUID().toString();
					while(i.hasID(id));


					i.addInsult(id,inputs);
					outputMessageDelete(message,String.format(Messages.getString("InsultCommand.5"),id.toString()),INFO); //$NON-NLS-1$ 

				} else outputMessageDelete(message,Messages.getString("InsultCommand.6"),ERROR); //$NON-NLS-1$ 
				return;
			} else if(command.equals("remove")) { //$NON-NLS-1$
				UUID id = UUID.fromString(inputs);
				GuildInsults i = new GuildInsults(h, message.getGuild());
				if(id != null && i.hasID(id.toString())) {
					i.removeInsult(id.toString());
					outputMessageDelete(message,String.format(Messages.getString("InsultCommand.7"),id.toString()),INFO); //$NON-NLS-1$ 
				}
				else 
					outputMessageDelete(message,Messages.getString("InsultCommand.8"),ERROR); //$NON-NLS-1$ 
				i = null;
				return;
			} else if(command.equals("list")) { //$NON-NLS-1$
				reduce(new GuildInsults(h, message.getGuild()),message.getChannel()::sendMessage);
				return;
			}
		}
		List<IUser> l = message.getMentions();
		if(l.size() != 1) {
			outputMessageDelete(message,Messages.getString("InsultCommand.9"),ERROR); //$NON-NLS-1$ 
			return;
		}
		GuildInsults i = new GuildInsults(h, message.getGuild());
		if(i.isEmpty()) {
			outputMessageDelete(message,Messages.getString("InsultCommand.10"),ERROR); //$NON-NLS-1$ 
			return;
		}
		message.getChannel().sendMessage(DiscordHelper.getDefaultRoleEmbed(message.getAuthor(), 
				message.getGuild()).withDesc(String.format(i.getRandomInsult(), l.get(0).mention())).build());
	}

	private static boolean moreThanOne(String in) {
		try {
			String.format(in,"@Test"); //$NON-NLS-1$
		} catch(Exception e) {
			return true;
		}
		int posA = in.indexOf("%s"); //$NON-NLS-1$
		int posB = in.indexOf("%s"); //$NON-NLS-1$
		return !(posA == posB && posB == -1) && posA != posB;
	}

	private static void reduce(GuildInsults i, Consumer<String> c) {
		String output = "Insults: " + System.lineSeparator() + "```glsl" + System.lineSeparator(); //$NON-NLS-1$ //$NON-NLS-2$
		for(String a : i.getKeySet()) {
			String out = String.format("%s\n\t\t//%s\n",i.getInsult(a),a) + System.lineSeparator(); //$NON-NLS-1$
			if(output.length() + out.length() > 2000) {
				c.accept(output + "```"); //$NON-NLS-1$
				output = "```glsl" + System.lineSeparator(); //$NON-NLS-1$
			} 
			output+= out;
		}
		c.accept(output + "```"); //$NON-NLS-1$
	}

	private static class GuildInsults {
		private Map<String,Object> insults = new HashMap<>();
		private TimeFilterSelector<String> filter;
		private final StorageHandler h;

		public GuildInsults(StorageHandler h, IGuild i) throws Exception {
			this.h = h;
			this.filter = getFilterForGuild(i,"insults.filter"); //$NON-NLS-1$
			if(h.data == null)
				h.data = new JSONObject();
			this.insults = JSONHelper.jsonToMap((JSONObject)h.data);
		}

		public String getRandomInsult() {
			if(this.filter.keySet().equals(this.insults.keySet())) 
				this.filter.clear();
			String a =  (String) this.insults.get(this.filter.getRandomItem(this.insults.keySet()));
			return a;
		}

		public boolean isEmpty() {
			return this.insults.isEmpty();
		}

		public void addInsult(String key, String in) {
			this.insults.put(key, in);
			save();
		}

		public void removeInsult(String id) {
			this.insults.remove(id);
			save();
		}

		public int getAmountOfInsults() {
			return this.insults.size();
		}

		public boolean hasID(String id) {
			return this.insults.containsKey(id);
		}

		public String getInsult(String id) {
			return (String) this.insults.get(id);
		}

		public Set<String> getKeySet() {
			return this.insults.keySet();
		}

		private void save() {
			this.h.data = new JSONObject(this.insults);
			this.h.update();
		}
	}
}
