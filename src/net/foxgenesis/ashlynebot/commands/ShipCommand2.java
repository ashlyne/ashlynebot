package net.foxgenesis.ashlynebot.commands;

import java.awt.Graphics;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

import javax.imageio.ImageIO;

import com.vdurmont.emoji.EmojiManager;

import net.foxgenesis.discord.command.Command;
import net.foxgenesis.discord.command.IPlusCommand;
import net.foxgenesis.util.helper.DiscordHelper;
import sx.blah.discord.handle.obj.IGuild;
import sx.blah.discord.handle.obj.IMessage;
import sx.blah.discord.handle.obj.IUser;
import sx.blah.discord.handle.obj.StatusType;
import sx.blah.discord.util.RequestBuffer;
import sx.blah.discord.util.RequestBuffer.IVoidRequest;

@Command(name = "ship", help = "Declare the best couple", helpParams = "[@USER A][@USER B]")
public class ShipCommand2 implements IPlusCommand {
	private final static Random rand = new Random();
	private static Image heart;
	private static final File outputF = new File("ship.png"); //$NON-NLS-1$

	static {
		try {
			heart = ImageIO.read(ShipCommand2.class.getResourceAsStream("heart.png")); //$NON-NLS-1$
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void handleCommand(IMessage message, String inputs) throws Exception {
		List<IUser> mentions = message.getMentions();
		if(message.getFormattedContent().contains("@everyone") || message.getFormattedContent().contains("@here")) //$NON-NLS-1$ //$NON-NLS-2$
			mentions.clear();

		if(mentions.isEmpty()) {
			IUser a = getRandomUser(getOnlineMembers(message.getGuild()),null);
			mentions.add(a);
			mentions.add(getRandomUser(getOnlineMembers(message.getGuild()),a));
		}

		final int users = mentions.size(),
				sections = users + (users - 1);

		BufferedImage image = new BufferedImage(1000 * users / 2,300,BufferedImage.TYPE_INT_ARGB);
		Graphics g = image.createGraphics();

		final int width = image.getWidth(), height = image.getHeight();
		for(int i=0,j=0; i<sections; i++) 
			if((i & 1) == 0) {
				g.drawImage(DiscordHelper.getAvatar(mentions.get(j)), width/sections*i, 0, width/sections, height,null);
				j++;
			} else 
				g.drawImage(heart, width/sections*i, 0, width/sections, height,null);
		
		g.dispose();

		final double shipValue = rand.nextDouble();

		char[] o = {'@','@','@','@','@'};
		o[(int) (shipValue / 0.2)] = '%';
		StringBuffer buffer = new StringBuffer(new String(o).replaceAll("@", ":heavy_minus_sign:").replaceAll("%", ":radio_button:")); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$
		buffer.append(" " + Math.round(shipValue * 100) + "% chance"); //$NON-NLS-1$ //$NON-NLS-2$

		final String desc = mentions.stream().map(u -> u.mention(true)).reduce((a,b) -> a + " :heart: " + b).get(); //$NON-NLS-1$
		RequestBuffer.request(() -> {
			synchronized(outputF) {
				try {
					ImageIO.write(image, "PNG", outputF); //$NON-NLS-1$
					IMessage msg = message.getChannel().sendFile(DiscordHelper.getDefaultRoleEmbed(message.getAuthor(), 
							message.getGuild()).withTitle("I ship thee!").withImage("attachment://ship.png").withDesc(desc + '\n' + buffer.toString()) //$NON-NLS-1$ //$NON-NLS-2$ 
							.withFooterText("via ship command").build(),outputF); //$NON-NLS-1$
					DiscordHelper.bufferOrdered(new IVoidRequest[]{() ->  msg.addReaction(EmojiManager.getForAlias("white_check_mark")),() -> msg.addReaction(EmojiManager.getForAlias("no_entry"))}); //$NON-NLS-1$ //$NON-NLS-2$
				} catch(Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	private static IUser getRandomUser(List<IUser> users, IUser except) {
		users.sort((a,b) -> a.getStringID().compareTo(b.getStringID()));
		IUser a = users.get(rand.nextInt(users.size()));
		if(except != null && a.equals(except) && users.size() > 2)
			return getRandomUser(users,except);
		return a;
	}

	private static List<IUser> getOnlineMembers(IGuild guild) {
		return guild.getUsers().stream().filter(u -> u.getPresence().getStatus() == StatusType.ONLINE)
				.filter(u -> !u.isBot()).collect(Collectors.toList());
	}
}
