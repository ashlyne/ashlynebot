package net.foxgenesis.ashlynebot.commands;

import static net.foxgenesis.util.helper.DiscordHelper.ERROR;
import static net.foxgenesis.util.helper.DiscordHelper.outputMessageDelete;

import java.util.Random;

import net.foxgenesis.discord.command.Command;
import net.foxgenesis.discord.command.IPlusCommand;
import net.foxgenesis.util.helper.DiscordHelper;
import sx.blah.discord.handle.obj.IMessage;
import sx.blah.discord.util.EmbedBuilder;

@Command(name = "yesno", help = "Will respond with yes or no in response to question asked", helpParams = "<question>")
public class YesNoCommand implements IPlusCommand{
	private static final Random rand = new Random();
	@Override
	public void handleCommand(IMessage message, String inputs) throws Exception {
		message.delete();
		if(inputs != null) {
			EmbedBuilder b = DiscordHelper.getDefaultRoleEmbed(message.getAuthor(), message.getGuild());
			b.withTitle(inputs + '?' + DiscordHelper.getCustomEmoji("thenkong")); //$NON-NLS-1$
			b.withDesc(rand.nextBoolean()? "Yes" : "No");  //$NON-NLS-1$//$NON-NLS-2$
			message.getChannel().sendMessage(b.build());
		} else
			outputMessageDelete(message,Messages.getString("YesNoCommand.0"),ERROR); //$NON-NLS-1$
	}

}
