package net.foxgenesis.ashlynebot.commands;

import static net.foxgenesis.ashlynebot.Bot.genChan;
import static net.foxgenesis.util.helper.DiscordHelper.ERROR;
import static net.foxgenesis.util.helper.DiscordHelper.INFO;
import static net.foxgenesis.util.helper.DiscordHelper.outputMessageDelete;
import static sx.blah.discord.Discord4J.LOGGER;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

import net.foxgenesis.ashlynebot.Bot;
import net.foxgenesis.ashlynebot.TimedEvents;
import net.foxgenesis.ashlynebot.config.BooleanField;
import net.foxgenesis.ashlynebot.config.LongField;
import net.foxgenesis.discord.command.Command;
import net.foxgenesis.discord.command.IPlusCommand;
import net.foxgenesis.discord.guild.GuildDataReadyEvent;
import net.foxgenesis.discord.guild.ObjectKey;
import net.foxgenesis.discord.timed.TimedEvent;
import net.foxgenesis.util.helper.DiscordHelper;
import sx.blah.discord.handle.obj.IChannel;
import sx.blah.discord.handle.obj.IGuild;
import sx.blah.discord.handle.obj.IMessage;
import sx.blah.discord.handle.obj.IUser;
import sx.blah.discord.util.MissingPermissionsException;
import sx.blah.discord.util.RequestBuffer;

@Command(name = "bday", help = "register your birthday", helpParams = "<register Month/Day (i.e. register 05/24) | remove | list>")
public class BirthdayCommand implements IPlusCommand {
	private static ObjectKey<ConcurrentHashMap<Long, Date>> obj = Bot.getData().getObject("bday", new ConcurrentHashMap<Long,Date>()); //$NON-NLS-1$
	private static final SimpleDateFormat format = new SimpleDateFormat("MM/dd"); //$NON-NLS-1$
	private static final BooleanField enabled = new BooleanField("bday.enabled",true,true); //$NON-NLS-1$
	private static final LongField announceChannel = new LongField("bday.channel", g -> genChan(g).getLongID(),true); //$NON-NLS-1$

	private static final TimedEvent event = new TimedEvent("bday", (long)1.08e+7,(long)1.08e+7, true, true) { //$NON-NLS-1$
		@Override
		public void run(IGuild guild) {
			if(!enabled.optFrom(guild))
				return;

			Map<IUser,Date> d = getTodaysBDays(guild);
			LOGGER.trace("[BirthdayCommand]: " + d.size() + " birthdays today"); //$NON-NLS-1$ //$NON-NLS-2$
			d.keySet().forEach(user -> {
				IChannel defaultChannel = guild.getChannelByID(announceChannel.from(guild));
				try {
					RequestBuffer.request(() ->
					defaultChannel.sendMessage(DiscordHelper.getDefaultRoleEmbed(user, 
							guild).withTitle(Messages.getString("BirthdayCommand.0")).withDesc( //$NON-NLS-1$
									String.format(Messages.getString("BirthdayCommand.1"),user.getName())).build())); //$NON-NLS-1$
				} catch(MissingPermissionsException e) {
					LOGGER.warn("Unable to display birthdays due to missing perms in guild %s | %s | SYSTEM: %s",  //$NON-NLS-1$
							guild.getName(), defaultChannel.getName(), defaultChannel);
				}
			});
		}
		@Override
		public boolean shouldStop() {return false;}
	};




	@Override
	public void handleCommand(IMessage message, String inputs) throws Exception {
		if(inputs != null) {
			int index = inputs.indexOf(' ');
			String a = inputs,b=null;
			if(index != -1) {
				a = inputs.substring(0,index);
				b = inputs.substring(index).trim();
			}
			if(a.length() > 1) {
				IUser user = message.getAuthor();
				ConcurrentHashMap<Long,Date> f = obj.object;
				switch(a.toLowerCase().trim()) {
				default: outputMessageDelete(message,Messages.getString("BirthdayCommand.2"),ERROR); return; //$NON-NLS-1$ 
				case "register": //$NON-NLS-1$
					if(f.containsKey(user.getLongID())) {
						outputMessageDelete(message,Messages.getString("BirthdayCommand.3"),ERROR); //$NON-NLS-1$ 
						return;
					}
					try {
						Date date = format.parse(b);
						f.put(user.getLongID(), date);
						obj.update();
						outputMessageDelete(message,Messages.getString("BirthdayCommand.5") + format.format(date),INFO); //$NON-NLS-1$ 
					} catch(Exception e) {
						outputMessageDelete(message,Messages.getString("BirthdayCommand.7"),ERROR); //$NON-NLS-1$ 
						return;
					}
					break;
				case "remove": //$NON-NLS-1$
					if(f.containsKey(user.getLongID())) {
						f.remove(user.getLongID());
						obj.update();
						outputMessageDelete(message,Messages.getString("BirthdayCommand.9"),INFO); //$NON-NLS-1$ 
						return;
					}
					outputMessageDelete(message,Messages.getString("BirthdayCommand.11"),ERROR); //$NON-NLS-1$ 
					return;
				case "list": //$NON-NLS-1$
					StringBuilder build = new StringBuilder();
					build.append(Messages.getString("BirthdayCommand.12")); //$NON-NLS-1$
					getBDaysForGuild(message.getGuild()).entrySet().stream().sorted((aa,bb) -> {
						Date d1 = aa.getValue(),d2 = bb.getValue();
						return d1.compareTo(d2);
					}).forEachOrdered(e -> build.append(String.format("`%s - %s`\n", e.getKey().getName(),format.format(e.getValue())))); //$NON-NLS-1$
					DiscordHelper.splitToMessages(build.toString(), message.getChannel()::sendMessage);
					break;
				}
			}
		}
		//message.reply("Invalid Usage.");
	}

	private static Map<IUser,Date> getTodaysBDays(IGuild guild) {
		Calendar t = Calendar.getInstance();
		Calendar today = new Calendar.Builder().setCalendarType(t.getCalendarType()).set(Calendar.MONTH, 
				t.get(Calendar.MONTH)).set(Calendar.DAY_OF_MONTH, t.get(Calendar.DAY_OF_MONTH)).build();
		if(!Bot.getData().isReady())
			try {
				Bot.getClient().getDispatcher().waitFor(GuildDataReadyEvent.class);
			} catch (InterruptedException e1) {
				e1.printStackTrace();
			}
		return getBDaysForGuild(guild).entrySet().parallelStream().filter(entry -> 
		toCal(entry.getValue()).compareTo(today) == 0).collect(Collectors.toMap(e -> e.getKey(), e -> e.getValue()));
	}

	private static Map<IUser,Date> getBDaysForGuild(IGuild guild) {
		return obj.object.entrySet().stream().filter(e -> guild.getUserByID(e.getKey()) != null).collect(Collectors.toMap(e -> guild.getUserByID(e.getKey()),e -> e.getValue()));
	}

	private static Calendar toCal(Date date){ 
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		return cal;
	}


	static {
		TimedEvents.registerGlobalEvent(event);
		TimedEvents.register(event);
	}
}
