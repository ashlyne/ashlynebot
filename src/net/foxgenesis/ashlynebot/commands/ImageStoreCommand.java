package net.foxgenesis.ashlynebot.commands;

import static net.foxgenesis.ashlynebot.Bot.delete;
import static net.foxgenesis.util.helper.DiscordHelper.ERROR;
import static net.foxgenesis.util.helper.DiscordHelper.INFO;
import static net.foxgenesis.util.helper.DiscordHelper.outputMessageDelete;

import java.util.Arrays;

import org.json.JSONObject;

import net.foxgenesis.ashlynebot.Bot;
import net.foxgenesis.discord.command.Command;
import net.foxgenesis.discord.command.CommandStorage;
import net.foxgenesis.discord.guild.SQLDataType;
import net.foxgenesis.discord.guild.StorageHandler;
import net.foxgenesis.util.helper.DiscordHelper;
import sx.blah.discord.api.internal.json.objects.EmbedObject;
import sx.blah.discord.handle.obj.IGuild;
import sx.blah.discord.handle.obj.IMessage;
import sx.blah.discord.handle.obj.IUser;

@Command(name = "img", help = "Store or recall and image", helpParams = "<store name@@url[&&&url...] | name | remove <name> | list>")
public class ImageStoreCommand extends CommandStorage {

	public ImageStoreCommand() {
		super("images", SQLDataType.JSON.name, SQLDataType.JSON); //$NON-NLS-1$
	}

	@Override
	public void handleCommand(IMessage message, String inputs, StorageHandler h) throws Exception {
		if(inputs == null || inputs.isEmpty()) {
			helpMessage(message);
			return;
		}
		DiscordHelper.tryToDelete(message);
		String[] v = inputs.split(" "); //$NON-NLS-1$
		JSONObject f  = h.data == null? (JSONObject) (h.data = new JSONObject()) : (JSONObject) h.data;
		if(v.length == 2){
			if(canUseCommand(message.getAuthor(),message.getGuild())) {
				if(v[0].equalsIgnoreCase("store")) { //$NON-NLS-1$
					if(v[1].matches(".*@@.*")) { //$NON-NLS-1$
						String[] d = {v[1].substring(0, v[1].indexOf("@@")),v[1].substring(v[1].indexOf("@@") + 2)}; //$NON-NLS-1$ //$NON-NLS-2$
						if(d.length == 2) 
							if(!f.has(getKeyForName(d[0]))) {
								f.put(getKeyForName(d[0]), d[1]);
								h.update();
								outputMessageDelete(message,Messages.getString("ImageStoreCommand.7"),INFO); //$NON-NLS-1$ 
							} else
								outputMessageDelete(message,Messages.getString("ImageStoreCommand.9"),ERROR); //$NON-NLS-1$ 
						else
							helpMessage(message);
					} else
						helpMessage(message);
				} else if(v[0].equalsIgnoreCase("remove"))  //$NON-NLS-1$
					if(f.has(getKeyForName(v[1]))) {
						f.remove(getKeyForName(v[1]));
						h.update();
						outputMessageDelete(message,Messages.getString("ImageStoreCommand.12"),INFO); //$NON-NLS-1$ 
					} else
						outputMessageDelete(message,Messages.getString("ImageStoreCommand.14"),ERROR); //$NON-NLS-1$ 
			} else
				outputMessageDelete(message,Messages.getString("ImageStoreCommand.16"),ERROR); //$NON-NLS-1$ 
			return;
		} else if(v.length == 1) {
			if(v[0].equalsIgnoreCase("list")) //$NON-NLS-1$
				DiscordHelper.splitToMessages(f.keySet().stream().filter(c -> c.startsWith("imgStore.")).sorted().reduce((a,b) -> a + "\t" + b).orElseGet(() -> ""), //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
						2000, "```","```",s -> delete(message.getChannel().sendMessage(s.replaceAll("imgStore.","")))); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$
			else if(f.has(getKeyForName(v[0]))) {
				message.delete();
				Arrays.asList(f.getString(getKeyForName(v[0])).split("&&&")).forEach(s -> { //$NON-NLS-1$
					EmbedObject j = getObject(s,v[0],message.getAuthor(),message.getGuild());
					message.getChannel().sendMessage(j);
				});
			}
			else
				outputMessageDelete(message,Messages.getString("ImageStoreCommand.27"),ERROR); //$NON-NLS-1$
			return;
		}
		helpMessage(message);
	}

	private static void helpMessage(IMessage message) {
		outputMessageDelete(message,Messages.getString("ImageStoreCommand.29"),ERROR); //$NON-NLS-1$
	}

	private static String getKeyForName(String in) {
		return "imgStore." + in.toLowerCase(); //$NON-NLS-1$
	}

	private static boolean canUseCommand(IUser user, IGuild guild) {
		return Bot.isBotAdmin(user) || Bot.hasAdminPerms(user, guild);
	}

	private static EmbedObject getObject(String url, String subtext, IUser user, IGuild guild) {
		return DiscordHelper.getDefaultRoleEmbed(user, guild).withImage(url).withFooterText(subtext).build();
	}

}
