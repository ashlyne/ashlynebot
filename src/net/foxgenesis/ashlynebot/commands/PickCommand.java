package net.foxgenesis.ashlynebot.commands;

import static net.foxgenesis.ashlynebot.Bot.rand;
import static net.foxgenesis.util.helper.DiscordHelper.ERROR;
import static net.foxgenesis.util.helper.DiscordHelper.INFO;
import static net.foxgenesis.util.helper.DiscordHelper.outputMessageDelete;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Matcher;

import net.foxgenesis.ashlynebot.PlusCommands;
import net.foxgenesis.discord.command.Command;
import net.foxgenesis.discord.command.IPlusCommand;
import net.foxgenesis.util.helper.DiscordHelper;
import sx.blah.discord.handle.obj.IMessage;

@Command(name = "pick", help = "Pick from a select list and given question.", helpParams = "\"Question\" \"Par1\" \"Par2\" etc")
public class PickCommand implements IPlusCommand {

	@Override
	public void handleCommand(IMessage message, String inputs) throws Exception {
		if(inputs != null) {
			DiscordHelper.tryToDelete(message);
			List<String> list = new ArrayList<>();
			Matcher m = PlusCommands.pattern.matcher(inputs.trim());
			while(m.find())
				list.add(m.group(1).replace("\"","")); //$NON-NLS-1$ //$NON-NLS-2$
			if(list.size() >= 2) {
				String question = list.remove(0);
				String[] d = list.toArray(new String[]{});
				outputMessageDelete(message,String.format(Messages.getString("PickCommand.5"), question.trim(),   //$NON-NLS-1$
						d[rand.nextInt(d.length)].trim(),Arrays.stream(d).reduce((a,b) -> a + "  , " + b).get().trim()),INFO); //$NON-NLS-1$ 
				return;
			}
		}
		outputMessageDelete(message,Messages.getString("PickCommand.7"),ERROR); //$NON-NLS-1$
	}

}
