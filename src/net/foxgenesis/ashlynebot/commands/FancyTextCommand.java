package net.foxgenesis.ashlynebot.commands;

import static net.foxgenesis.util.helper.DiscordHelper.ERROR;
import static net.foxgenesis.util.helper.DiscordHelper.INFO;
import static net.foxgenesis.util.helper.DiscordHelper.outputMessage;
import static net.foxgenesis.util.helper.DiscordHelper.outputMessageDelete;

import java.net.URLEncoder;

import org.json.JSONObject;

import com.mashape.unirest.http.Unirest;

import net.foxgenesis.discord.command.Command;
import net.foxgenesis.discord.command.IPlusCommand;
import sx.blah.discord.handle.obj.IMessage;

@Command(name = "fancytext", help = "Turn text into Fancy Text", helpParams = "<text>")
public class FancyTextCommand implements IPlusCommand {

	@Override
	public void handleCommand(IMessage message, String inputs) throws Exception {
		if(inputs != null) {
			JSONObject defs = Unirest.get(String.format("https://ajith-Fancy-text-v1.p.mashape.com/text?text=%s",URLEncoder.encode(inputs.replaceAll("\n", " "), "UTF-8"))) //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$
					.header("X-Mashape-Host", "ajith-Fancy-text-v1.p.mashape.com").asJson().getBody().getObject(); //$NON-NLS-1$ //$NON-NLS-2$
			outputMessage(message,defs.getString("fancytext"),INFO); //$NON-NLS-1$
		} else outputMessageDelete(message, Messages.getString("FancyTextCommand.no-text"), ERROR);  //$NON-NLS-1$
	}

}
