package net.foxgenesis.ashlynebot.commands;

import java.util.HashMap;
import java.util.Random;

import net.foxgenesis.discord.command.Command;
import net.foxgenesis.discord.command.IPlusCommand;
import net.foxgenesis.util.helper.DiscordHelper;
import net.foxgenesis.util.helper.RandomHelper;
import sx.blah.discord.handle.obj.IMessage;

@Command(name = "rate", help = "Rates an item 0-10", helpParams = "<text..>")
public class RateCommand implements IPlusCommand {
	private static final Random rand = new Random();
	private static final HashMap<Integer,Double> p = new HashMap<>();
	static {
		p.put(0, 0.3);
		p.put(1, 0.4);
		p.put(2, 0.44);
		p.put(3, 0.5);
		p.put(4, 0.55);
		p.put(5, 1.0);
		p.put(6, 0.55);
		p.put(7, 0.5);
		p.put(8, 0.44);
		p.put(9, 0.4);
		p.put(10, 0.3);
	}
	@Override
	public void handleCommand(IMessage message, String inputs) throws Exception {
		if(inputs != null) {
			int random = RandomHelper.getWeightedRandom(p.entrySet().stream(), rand);
			String output = String.format(Messages.getString("RateCommand.0"),inputs); //$NON-NLS-1$
			if(random == 0)
				output+= Messages.getString("RateCommand.1"); //$NON-NLS-1$
			else if(random == 10)
				output+= Messages.getString("RateCommand.2"); //$NON-NLS-1$
			else
				output+= " " + random + "/10.";  //$NON-NLS-1$//$NON-NLS-2$
			message.reply(output);
		} else
			message.reply(Messages.getString("RateCommand.3")); //$NON-NLS-1$
		DiscordHelper.tryToDelete(message);
	}

}
