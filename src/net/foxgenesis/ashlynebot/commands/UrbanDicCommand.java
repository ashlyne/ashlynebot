package net.foxgenesis.ashlynebot.commands;

import static net.foxgenesis.util.helper.DiscordHelper.ERROR;
import static net.foxgenesis.util.helper.DiscordHelper.outputMessageDelete;

import java.net.URLEncoder;

import org.json.JSONArray;
import org.json.JSONObject;

import com.mashape.unirest.http.Unirest;

import net.foxgenesis.discord.command.Command;
import net.foxgenesis.discord.command.IPlusCommand;
import sx.blah.discord.handle.obj.IMessage;
import sx.blah.discord.util.EmbedBuilder;

@Command(name = "urban", help = "Define a word from UrbanDictonary", helpParams = "<word>")
public class UrbanDicCommand implements IPlusCommand {

	@Override
	public void handleCommand(IMessage message, String inputs) throws Exception {
		if(inputs != null) {
			JSONArray defs = Unirest.get(String.format("https://mashape-community-urban-dictionary.p.mashape.com/define?term=%s",URLEncoder.encode(inputs, "UTF-8"))) //$NON-NLS-1$ //$NON-NLS-2$
					.header("X-Mashape-Host", "mashape-community-urban-dictionary.p.mashape.com").asJson().getBody().getObject().getJSONArray("list"); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
			
			if(defs.length() > 0) {
				JSONObject definition = defs.getJSONObject(0);
				EmbedBuilder b = new EmbedBuilder().withColor(0x498FE1).withThumbnail("https://upload.wikimedia.org/wikipedia/commons/thumb/8/82/UD_logo-01.svg/2000px-UD_logo-01.svg.png")//$NON-NLS-1$ //$NON-NLS-2$
						.withTitle(String.format(Messages.getString("UrbanDicCommand.embed_title"), inputs)) //$NON-NLS-1$
						.withDesc(definition.getString("definition").replaceAll("\"", "*").replaceAll("[\\[\\]]", "**"));  //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$ //$NON-NLS-5$
				message.getChannel().sendMessage(b.build());
			} else outputMessageDelete(message, Messages.getString("UrbanDicCommand.no_def"), ERROR); //$NON-NLS-1$
		} else outputMessageDelete(message, Messages.getString("UrbanDicCommand.no_word"), ERROR); //$NON-NLS-1$
	}

}
