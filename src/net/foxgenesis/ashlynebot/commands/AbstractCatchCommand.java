package net.foxgenesis.ashlynebot.commands;

import java.io.Serializable;
import java.util.Random;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Function;

import net.foxgenesis.ashlynebot.commands.AbstractCatchCommand.CatchInv;
import net.foxgenesis.discord.command.CommandObjectStorage;
import sx.blah.discord.handle.obj.IMessage;
import sx.blah.discord.handle.obj.IUser;
import sx.blah.discord.util.RequestBuffer;

@SuppressWarnings("nls")
public abstract class AbstractCatchCommand<T extends CatchInv> extends CommandObjectStorage<ConcurrentHashMap<Long,T>> {
	protected static final Random rand = new Random();
	private final Function<IUser,T> newInv;
	
	public AbstractCatchCommand(String name, Function<IUser,T> newInv) {
		super("catch." + name, new ConcurrentHashMap<Long,T>());
		this.newInv = newInv;
	}
	
	@Override
	public final void handleCommand(IMessage message, String inputs) {
		IUser author = message.getAuthor();
		ConcurrentHashMap<Long,T> data = this.obj.object;
		
		if(!data.containsKey(author.getLongID()))
			data.put(author.getLongID(), this.newInv.apply(author));
		
		T inv = data.get(author.getLongID());
		
		if(inputs != null) {
			String[] d = inputs.split(" ");
			if(d[0].equalsIgnoreCase("inv"))
				RequestBuffer.request(() -> message.reply(inv.toString()));
			else
				handleArgument(message, author,inv,d[0],inputs.substring(d[0].length()));
		} else {
			String status = onCatch(author, inv);
			if(inv.shouldUpdate) {
				inv.shouldUpdate = false;
				this.obj.update();
			}
			RequestBuffer.request(() -> message.reply(status));
		}
	}
	
	protected T getUserInv(IUser user) {
		ConcurrentHashMap<Long,T> data = this.obj.object;
		return data.containsKey(user.getLongID()) ? data.get(user.getLongID()) : this.newInv.apply(user);
	}
	
	protected abstract String onCatch(IUser user, T inv);
	protected abstract void handleArgument(final IMessage message, final IUser user, T inv, String command, String rest);
	
	protected static abstract class CatchInv implements Serializable {
		private static final long serialVersionUID = 1L;
		private long count;
		protected volatile boolean shouldUpdate;
		public long getCount() {return this.count;}
		public void setCount(long count) {this.count = count < 0? 0 : count; this.shouldUpdate = true;}
		
		@Override
		public abstract String toString();
	}
}
