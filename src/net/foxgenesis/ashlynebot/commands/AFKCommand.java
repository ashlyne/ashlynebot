package net.foxgenesis.ashlynebot.commands;

import java.util.concurrent.ConcurrentHashMap;

import net.foxgenesis.ashlynebot.Bot;
import net.foxgenesis.ashlynebot.BotListeners;
import net.foxgenesis.discord.command.Command;
import net.foxgenesis.discord.command.IPlusCommand;
import net.foxgenesis.discord.guild.ObjectKey;
import sx.blah.discord.api.events.EventSubscriber;
import sx.blah.discord.handle.impl.events.guild.channel.message.MessageReceivedEvent;
import sx.blah.discord.handle.obj.IGuild;
import sx.blah.discord.handle.obj.IMessage;
import sx.blah.discord.handle.obj.IUser;

@Command(name = "afk", help = "Go afk", helpParams = "[message]")
public class AFKCommand implements IPlusCommand {
	private static ObjectKey<ConcurrentHashMap<Long, String>> obj = Bot.getData().getObject("afk", new ConcurrentHashMap<Long,String>()); //$NON-NLS-1$
	private static final String OUT = Messages.getString("AFKCommand.0"); //$NON-NLS-1$
	private static final String SET = Messages.getString("AFKCommand.1"); //$NON-NLS-1$
	private static final String BACK = Messages.getString("AFKCommand.2"); //$NON-NLS-1$

	public AFKCommand() {
		BotListeners.add(this);
	}

	@SuppressWarnings("static-method")
	@EventSubscriber
	public void mention(MessageReceivedEvent e) {
		ConcurrentHashMap<Long,String> a = obj.object;
		IMessage m = e.getMessage();
		long id = m.getAuthor().getLongID();
		if(!m.getFormattedContent().startsWith("+afk") && a.containsKey(id)) { //$NON-NLS-1$
			a.remove(id);
			m.reply(BACK);
			setAFK(m.getAuthor(),m.getGuild(),false);
			obj.update();
		}
		if(!(m.mentionsEveryone() || m.mentionsHere()))
			m.getMentions().forEach(u -> {
				if(a.containsKey(u.getLongID())) {
					String r = a.get(u.getLongID());
					m.reply(String.format(OUT, u.getDisplayName(e.getGuild()),r.isEmpty()? "" : "``" + r + "``")); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
				}
			});
	}

	@Override
	public void handleCommand(IMessage message, String inputs) throws Exception {
		ConcurrentHashMap<Long,String> a = obj.object;
		IUser user = message.getAuthor();
		long id = user.getLongID();
		if(inputs == null) {
			if(a.containsKey(id)) {
				a.remove(id);
				message.reply(BACK);
				setAFK(user,message.getGuild(),false);
				obj.update();
				return;
			}
		}
		a.put(id, inputs == null? "" : inputs); //$NON-NLS-1$
		message.reply(SET);
		setAFK(user,message.getGuild(),true);
		obj.update();
	}
	
	private static void setAFK(IUser user, IGuild guild, boolean state) {
		guild.setUserNickname(user, state? "[AFK]" + user.getDisplayName(guild) : user.getDisplayName(guild).replaceAll("\\[AFK\\]", "")); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
	}
}
