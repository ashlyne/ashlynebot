package net.foxgenesis.ashlynebot.commands;

import static net.foxgenesis.ashlynebot.Bot.delete;
import static net.foxgenesis.util.helper.DiscordHelper.ERROR;
import static net.foxgenesis.util.helper.DiscordHelper.outputMessageDelete;
import static net.foxgenesis.util.helper.DiscordHelper.outputMessageObj;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.script.Bindings;
import javax.script.ScriptContext;
import javax.script.ScriptEngine;

import net.foxgenesis.discord.command.Command;
import net.foxgenesis.discord.command.IPlusCommand;
import net.foxgenesis.util.JavaScriptUtil;
import net.foxgenesis.util.helper.DiscordHelper;
import sx.blah.discord.handle.obj.IMessage;

@Command(name = "graphp", help = "Graph in polar coords", helpParams = "equ [@@equ...] [&&x=size,rgb=bool,f=thickness,r=rotations,swap=HEX|HEX]", cooldown = true)
public class GraphCommandPolar implements IPlusCommand {
	private static final File f = new File("graph.png"); //$NON-NLS-1$
	private static final ScriptEngine engine = JavaScriptUtil.getSeperateJSEngine();

	@Override
	public void handleCommand(IMessage message, String inputs) throws Exception {
		if(inputs != null && !inputs.isEmpty()) 
			test(message,inputs.replaceAll("exit\\(\\)", "")); //$NON-NLS-1$ //$NON-NLS-2$
		else outputMessageDelete(message,Messages.getString("GraphCommandPolar.4"),ERROR); //$NON-NLS-1$
	}

	private static void test(IMessage message, String inputs) throws IOException {
		String[] se = inputs.split("&&",2); //$NON-NLS-1$
		String inpu = se[0];
		int tsize = 500;
		int tf = 1;
		int tswapA = -1, tswapB = tswapA, tre = 1;
		boolean trgb = true;
		if(se.length > 1) {
			String[] params = se[1].split(","); //$NON-NLS-1$
			for(String pa : params) {
				String[] da = pa.trim().split("="); //$NON-NLS-1$
				if(da.length == 2) {
					String key = da[0].trim();
					String value = da[1].trim();
					switch(key.toLowerCase()) {
					default:break;
					case "x":  //$NON-NLS-1$
						tsize = Integer.parseInt(value);
						break;
					case "rgb": //$NON-NLS-1$
						trgb = Boolean.parseBoolean(value);
						break;
					case "f": //$NON-NLS-1$
						tf = Integer.parseInt(value);
						break;
					case "r": //$NON-NLS-1$
						tre = Integer.parseInt(value);
						break;
					case "swap": //$NON-NLS-1$
						trgb = false;
						String[] de = value.split("\\|"); //$NON-NLS-1$
						if(de.length == 2) {
							tswapA = Integer.parseInt(de[0],16);
							tswapB = Integer.parseInt(de[1],16);
						}
					}
				}
			}
		}
		if(tre > 30)
			tre = 30;
		int size = tsize, swapA = tswapA,swapB = tswapB, fF = tf, re = tre;
		boolean rgb = trgb;
		BufferedImage image = new BufferedImage(1000,1000, BufferedImage.TYPE_INT_RGB);
		Graphics2D g = image.createGraphics();
		g.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
				RenderingHints.VALUE_ANTIALIAS_ON);

		g.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING,
				RenderingHints.VALUE_TEXT_ANTIALIAS_ON);

		g.setRenderingHint(RenderingHints.KEY_RENDERING,
				RenderingHints.VALUE_RENDER_QUALITY);

		Bindings vars = engine.getBindings(ScriptContext.ENGINE_SCOPE);
		String[] eq = inpu.split("@@"); //$NON-NLS-1$
		try {
			int r = image.getWidth()/2;
			for(String inp: eq) {
				double px = 0, py = 0;
				boolean go = false;
				for(double i=0; i<Math.toRadians(360) * re; i+=0.01) {
					double x = Math.cos(i), y = Math.sin(i);
					vars.put("x", i); //$NON-NLS-1$
					vars.put("θ", i); //$NON-NLS-1$
					//.replaceAll("[^x0-9*-><,+/%&|\\(\\)(Math.)(sin|(cos|tan|PI|E|abs|sqrt|pow)]", "")
					//inp = (String) inv.invokeFunction("calc", inp,i);
					//double in = Double.parseDouble(asdf);
					double in = ((Number)engine.eval(inp)).doubleValue();
					g.setStroke(new BasicStroke(fF));
					if(rgb)
						g.setColor(new Color(Color.HSBtoRGB((float)in/(size / 2f), 1f, 1f)));
					else if(swapA != -1 && swapB != -1)
						g.setColor(mixColors(new Color(swapA),new Color(swapB), i / (float)(Math.PI/6)));
					double s = size / 500D;
					x*=in/s;
					y*=in/s;
					if(go) {
						g.drawLine(r + (int)px, r - (int)py, r + (int)x, r - (int)y);
					} else go = true;
					px = x;
					py = y;

				}
			}
		} catch(Exception e) {
			delete(message.reply("",outputMessageObj(Messages.getString("GraphCommandPolar.18") + e.getMessage(),ERROR).build()),30_000); //$NON-NLS-1$ //$NON-NLS-2$
			message.delete();
			return;
		}
		g.setFont(new Font("arial", 0, 15)); //$NON-NLS-1$
		g.setColor(Color.white);
		g.drawString(String.format("%d x %d",size,size), 10, g.getFontMetrics().getHeight()); //$NON-NLS-1$

		g.dispose();
		DiscordHelper.tryToDelete(message);
		synchronized(f) {
			ImageIO.write(image, "PNG", f); //$NON-NLS-1$
			message.getChannel().sendFile(DiscordHelper.getDefaultRoleEmbed(message.getAuthor(), message.getGuild()).withImage("attachment://graph.png") //$NON-NLS-1$
					.withTitle("Polar Graph").appendField("Scale", "" + (size / 500D), true).appendField("Amt", "" + eq.length, true).withFooterText(String.format(Messages.getString("GraphCommandPolar.28"), inputs)).build(),f); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$ //$NON-NLS-5$ //$NON-NLS-6$
		}
	}

	private static Color mixColors(Color color1, Color color2, double percent){
		double inverse_percent = 1.0f - percent;
		int redPart = (int) (Math.abs(color1.getRed()*percent + color2.getRed()*inverse_percent)) % 255;
		int greenPart = (int) (Math.abs(color1.getGreen()*percent + color2.getGreen()*inverse_percent)) % 255;
		int bluePart = (int) (Math.abs(color1.getBlue()*percent + color2.getBlue()*inverse_percent)) % 255;
		return new Color(redPart, greenPart, bluePart);
	}
	//+graphp 500 & x % 400 << 132&&x=360,f=6,r=360
	//500 & x % 400 << 33
	//(2 + ((Math.abs(Math.cos(x * 3)) + (0.25-(Math.abs(Math.cos(x * 3 + Math.PI / 2)))) * 2) / (2 + Math.abs(Math.cos(x * 6 + Math.PI / 2)) * 8))) * 75 @@ (2 + ((Math.abs(Math.cos(x * 3)) + (0.25-(Math.abs(Math.cos(x * 3 + Math.PI / 2)))) * 2) / (2 + Math.abs(Math.cos(x * 6 + Math.PI / 2)) * 8))) * 50 @@ (2 + ((Math.abs(Math.cos(x * 3)) + (0.25-(Math.abs(Math.cos(x * 3 + Math.PI / 2)))) * 2) / (2 + Math.abs(Math.cos(x * 6 + Math.PI / 2)) * 8))) * 25
}
