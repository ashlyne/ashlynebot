package net.foxgenesis.ashlynebot.commands;

import static net.foxgenesis.util.helper.DiscordHelper.ERROR;
import static net.foxgenesis.util.helper.DiscordHelper.INFO;
import static net.foxgenesis.util.helper.DiscordHelper.outputMessageDelete;

import net.foxgenesis.discord.command.Command;
import net.foxgenesis.discord.command.IPlusCommand;
import net.foxgenesis.util.helper.DiscordHelper;
import sx.blah.discord.handle.obj.IMessage;

@Command(name = "bban", isHidden = true, isSuperCommand = true)
public class BotBanCommand implements IPlusCommand {

	@Override
	public void handleCommand(IMessage message, String inputs) throws Exception {
		String[] d = inputs.split(" ",2); //$NON-NLS-1$
		if(d.length < 0) {
			outputMessageDelete(message,Messages.getString("BotBanCommand.0"),ERROR); //$NON-NLS-1$
			return;
		}
		inputs = inputs.substring(4);
		for(int i=1; i<d.length; i++) {
			if(!d[i].startsWith("@")) { //$NON-NLS-1$
				inputs = inputs.substring(inputs.indexOf(d[i]) + d[i].length()).trim();
				break;
			}
		}
		if(d[0].trim().equalsIgnoreCase("ban")) { //$NON-NLS-1$
			while(inputs.charAt(0) == '@') {
				inputs = inputs.substring(inputs.indexOf(' ')+1);
			}
			String reason = inputs;
			message.getMentions().forEach(u -> {
				if(DiscordHelper.BannedBotUsers.ban(u, reason, message.getAuthor()))
					outputMessageDelete(message,String.format(Messages.getString("BotBanCommand.1"),u.mention()),INFO); //$NON-NLS-1$ 
				else
					outputMessageDelete(message,String.format(Messages.getString("BotBanCommand.2"),u.mention()),ERROR); //$NON-NLS-1$ 
			});
		} else if(d[0].trim().equalsIgnoreCase("unban")) //$NON-NLS-1$
			message.getMentions().forEach(u -> {
				if(DiscordHelper.BannedBotUsers.unban(u))
					outputMessageDelete(message,String.format(Messages.getString("BotBanCommand.3"),u.mention()),INFO); //$NON-NLS-1$ 
				else
					outputMessageDelete(message,String.format(Messages.getString("BotBanCommand.4"),u.mention()),ERROR); //$NON-NLS-1$ 
			});
		else outputMessageDelete(message,Messages.getString("BotBanCommand.6"),ERROR); //$NON-NLS-1$
		DiscordHelper.tryToDelete(message);
	}
}
