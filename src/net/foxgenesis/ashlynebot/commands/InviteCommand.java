package net.foxgenesis.ashlynebot.commands;

import net.foxgenesis.discord.command.Command;
import net.foxgenesis.discord.command.IPlusCommand;
import sx.blah.discord.handle.obj.IMessage;
import sx.blah.discord.handle.obj.IPrivateChannel;

@Command(name = "invite")
public class InviteCommand implements IPlusCommand {

	@Override
	public void handleCommand(IMessage message, String inputs) throws Exception {
		//channel.createInvite(0, 1, false, false).toString()
		IPrivateChannel c = message.getAuthor().getOrCreatePMChannel();
		c.sendMessage("Test Invite"); //$NON-NLS-1$
		c.sendMessage(message.getGuild().getDefaultChannel().createInvite(0, 1, false, false).toString());
	}
}
