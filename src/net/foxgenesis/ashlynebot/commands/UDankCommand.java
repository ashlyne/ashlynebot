package net.foxgenesis.ashlynebot.commands;

import static net.foxgenesis.util.helper.DiscordHelper.ERROR;
import static net.foxgenesis.util.helper.DiscordHelper.outputMessageDelete;

import java.util.HashMap;
import java.util.Timer;
import java.util.TimerTask;

import javax.sound.sampled.AudioSystem;

import net.foxgenesis.discord.command.Command;
import net.foxgenesis.discord.command.IPlusCommand;
import net.foxgenesis.util.helper.DiscordHelper;
import sx.blah.discord.handle.obj.IGuild;
import sx.blah.discord.handle.obj.IMessage;
import sx.blah.discord.handle.obj.IVoiceChannel;
import sx.blah.discord.util.audio.AudioPlayer;

@Command(name = "udank", help = "plays udank in your current voice channel", isOwnerCommand = true, isSuperCommand = true)
public class UDankCommand implements IPlusCommand {
	private HashMap<IGuild,Timer> guilds = new HashMap<>();
	@Override
	public void handleCommand(IMessage message, String inputs) throws Exception {
		message.delete();
		IGuild guild = message.getGuild();
		IVoiceChannel voice = DiscordHelper.getVoiceChannelForUser(message.getAuthor(), guild);
		if(voice == null) {
			outputMessageDelete(message,Messages.getString("UDankCommand.0"),ERROR); //$NON-NLS-1$
			return;
		}
		AudioPlayer player = AudioPlayer.getAudioPlayerForGuild(guild);
		if(player.getPlaylistSize() > 0) {
			outputMessageDelete(message,Messages.getString("UDankCommand.1"),ERROR); //$NON-NLS-1$
			return;
		}
		Timer timer;
		if(this.guilds.containsKey(guild)) {
			timer = this.guilds.get(guild);
			timer.cancel();
			timer = new Timer();
		}
		else {
			timer = new Timer();
			this.guilds.put(guild, timer);
		}
		player.setVolume(0.2f);
		voice.join();
		player.queue(AudioSystem.getAudioInputStream(UDankCommand.class.getResourceAsStream("udank.mp3"))); //$NON-NLS-1$
		timer.schedule(new TimerTask() {
			@Override
			public void run() {
				System.out.println("Leaving..."); //$NON-NLS-1$
				voice.leave();
			}
		}, 27_500l);
	}
}
