package net.foxgenesis.ashlynebot.commands;

import java.util.function.Predicate;

import net.foxgenesis.discord.command.Command;
import net.foxgenesis.discord.command.IPlusCommand;
import net.foxgenesis.util.TimeFilterSelector;
import net.foxgenesis.util.helper.DiscordHelper;
import net.foxgenesis.util.helper.RedditHelper;
import net.foxgenesis.util.helper.RedditHelper.RedditPost;
import sx.blah.discord.handle.obj.IMessage;

@Command(name = "omemes", help = "Summon some dank memes from reddit.com/r/OffensiveMemes", cooldown = true)
public class OffensiveMemeCommand implements IPlusCommand {
	private static final RedditHelper r = new RedditHelper("OffensiveMemes"); //$NON-NLS-1$
	private static final Predicate<RedditPost> filter = RedditHelper.NON_NSFW.and(RedditHelper.NON_STICKY.and(RedditHelper.PICTURES_ONLY));

	@Override
	public void handleCommand(IMessage message, String inputs) throws Exception {
		DiscordHelper.tryToDelete(message);
		try {
			TimeFilterSelector<RedditPost> s = DiscordHelper.getFilterForGuild(message.getGuild(), "omeme.filter"); //$NON-NLS-1$
			RedditPost re = s.getRandomItem(r.getPosts(100,filter));
			message.reply(re.url);
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
}
