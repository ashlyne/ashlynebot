package net.foxgenesis.ashlynebot.commands;

import static net.foxgenesis.ashlynebot.Bot.delete;
import static net.foxgenesis.util.helper.DiscordHelper.ERROR;
import static net.foxgenesis.util.helper.DiscordHelper.INFO;
import static net.foxgenesis.util.helper.DiscordHelper.outputMessage;
import static net.foxgenesis.util.helper.DiscordHelper.outputMessageDelete;
import static net.foxgenesis.util.helper.DiscordHelper.outputMessageObj;
import static sx.blah.discord.Discord4J.LOGGER;

import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.function.Consumer;
import java.util.stream.Collectors;

import net.foxgenesis.ashlynebot.Bot;
import net.foxgenesis.ashlynebot.TimedEvents;
import net.foxgenesis.discord.command.Command;
import net.foxgenesis.discord.command.IPlusCommand;
import net.foxgenesis.discord.guild.ObjectKey;
import net.foxgenesis.discord.timed.TimedEvent;
import net.foxgenesis.util.InsertionSort;
import net.foxgenesis.util.MethodTimer;
import sx.blah.discord.handle.obj.IGuild;
import sx.blah.discord.handle.obj.IMessage;
import sx.blah.discord.handle.obj.IUser;

@Command(name = "reminder", help = "Set a reminder", helpParams = "MESSAGE in TIME")
public class ReminderCommand implements IPlusCommand {
	private static ObjectKey<InsertionSort<Reminder>> obj = Bot.getData().getObject("reminder", new InsertionSort<Reminder>()); //$NON-NLS-1$

	private static final TimedEvent event = new TimedEvent("reminder", TimedEvent.SECOND * 2, TimedEvent.SECOND * 2, true, false) { //$NON-NLS-1$
		@Override
		public void run(IGuild guild) {
			LOGGER.debug(String.format("Finished reminders in %s", MethodTimer.run(() -> { //$NON-NLS-1$
				handleCurrentReminders(r -> {
					IUser user = Bot.getClient().getUserByID(r.id);
					user.getOrCreatePMChannel().sendMessage(outputMessageObj(r.reason, INFO).withFooterText("Reminder").withTimestamp(Calendar.getInstance().getTime().toInstant()).build()); //$NON-NLS-1$
				});
			})));
		}
		@Override
		public boolean shouldStop() {return false;}
	};

	static {
		TimedEvents.registerGlobalEvent(event);
		TimedEvents.register(event);
	}

	@Override
	public void handleCommand(IMessage message, String inputs) throws Exception {
		final long cTime = System.currentTimeMillis();
		
		
		delete(message);
		if(inputs.equalsIgnoreCase("clear")) { //$NON-NLS-1$
			obj.object.removeIf(r -> r.id == message.getAuthor().getLongID());
			outputMessageDelete(message,Messages.getString("ReminderCommand.0"),INFO);  //$NON-NLS-1$
			return;
		} else if(inputs.equalsIgnoreCase("clear*") && Bot.isBotAdmin(message.getAuthor())) { //$NON-NLS-1$
			obj.object.clear();
			outputMessageDelete(message,Messages.getString("ReminderCommand.1"),INFO); //$NON-NLS-1$
			return;
		} else if(inputs.equalsIgnoreCase("list") && Bot.isBotAdmin(message.getAuthor())) { //$NON-NLS-1$
			outputMessage(message,obj.object.toString(),INFO);
			return;
		}
		int ind = inputs.lastIndexOf(" in "); //$NON-NLS-1$

		if(ind == -1) {
			outputMessageDelete(message,Messages.getString("ReminderCommand.2"),ERROR); //$NON-NLS-1$
			return;
		}
		String[] dd = inputs.substring(ind+3).trim().split("(?<=\\d)(?=\\D)|(?=\\d)(?<=\\D)"); //$NON-NLS-1$
		if((dd.length & 1) == 1) {
			outputMessageDelete(message,Messages.getString("ReminderCommand.3"),ERROR); //$NON-NLS-1$
			return;
		}

		String reason = inputs.substring(0,ind).trim();
		StringBuilder b = new StringBuilder();
		long time = 0;
		for(int i=0; i<dd.length; i+=2) {
			int a = Integer.parseInt(dd[i]);
			String t = dd[i+1].trim();
			if(t.matches("(h(?:our(s)?)?)")) { //$NON-NLS-1$
				time += a * TimedEvent.HOUR;
				b.append(String.format("%s %s ", a, a > 1? "Hours" : "Hour"));  //$NON-NLS-1$//$NON-NLS-2$ //$NON-NLS-3$
			} else if(t.matches("d(?:ay(?:s)?)?")) { //$NON-NLS-1$
				time += a * TimedEvent.DAY;
				b.append(String.format("%s %s ", a, a > 1? "Days" : "Day"));  //$NON-NLS-1$//$NON-NLS-2$ //$NON-NLS-3$
			} else if(t.matches("m(?:in(?:ute(?:s)?)?)?")) { //$NON-NLS-1$
				time += a * TimedEvent.MINUTE;
				b.append(String.format("%s %s ", a, a > 1? "Minutes" : "Minute"));  //$NON-NLS-1$//$NON-NLS-2$ //$NON-NLS-3$
			} else if(t.matches("s(?:ec(?:s)?(?:ond(?:s)?)?)?")) { //$NON-NLS-1$
				time += a * TimedEvent.SECOND;
				b.append(String.format("%s %s ", a, a > 1? "Seconds" : "Second"));  //$NON-NLS-1$//$NON-NLS-2$ //$NON-NLS-3$
			} else {
				outputMessageDelete(message,String.format(Messages.getString("ReminderCommand.4"),t),ERROR); //$NON-NLS-1$
				return;
			}
		}

		if(time > (TimedEvent.DAY * 20)) {
			outputMessageDelete(message,Messages.getString("ReminderCommand.5"),ERROR); //$NON-NLS-1$
			return;
		}
		time+= cTime;
		
		if(obj.object.add(new Reminder(message.getAuthor().getLongID(), reason, time))) {
			outputMessageDelete(message,String.format(Messages.getString("ReminderCommand.6"), b.toString()),INFO); //$NON-NLS-1$
			obj.update();
		} else
			outputMessageDelete(message,Messages.getString("ReminderCommand.7"),ERROR); //$NON-NLS-1$
	}

	private static void handleCurrentReminders(Consumer<Reminder> onRemind) {
		//TODO: Can be optimized by using insertion sort and then iterate until next time is in the future or is empty
		long now = System.currentTimeMillis();
		List<Reminder> list = obj.object.parallelStream().filter(r -> now > r.time).collect(Collectors.toList());
		list.forEach(onRemind.andThen(obj.object::remove));
		//obj.object.removeAll(list);
		if(!list.isEmpty())
			obj.update();
	}


	private static class Reminder implements Serializable, Comparable<Reminder> {
		private static final long serialVersionUID = 259393171373964898L;
		public final long time;
		public final long id;
		public final String reason;

		public Reminder(long id, String reason, long time) {
			this.id = id;
			this.reason = reason;
			this.time = time;
		}

		@Override
		public int compareTo(Reminder o) {
			return Long.compare(this.time, o.time);
		}

		@Override
		public String toString() {
			return String.format("%s(%s)", this.reason, new Date(this.time)); //$NON-NLS-1$
		}
	}
}
