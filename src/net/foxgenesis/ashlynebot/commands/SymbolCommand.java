package net.foxgenesis.ashlynebot.commands;

import net.foxgenesis.discord.command.Command;
import net.foxgenesis.discord.command.IPlusCommand;
import net.foxgenesis.util.helper.DiscordHelper;
import sx.blah.discord.api.internal.json.objects.EmbedObject;
import sx.blah.discord.handle.obj.IGuild;
import sx.blah.discord.handle.obj.IMessage;
import sx.blah.discord.handle.obj.IUser;
import sx.blah.discord.util.EmbedBuilder;

@Command(name = "sym", help = "Turns text into symbols.", helpParams = "<text...>")
public class SymbolCommand implements IPlusCommand {

	@Override
	public void handleCommand(IMessage message, String inputs) {
		if(inputs != null && !inputs.isEmpty()) {
			DiscordHelper.tryToDelete(message);
			DiscordHelper.splitToMessages(generateSymbolMessage(inputs),2000,"","","\t",":",t ->  //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$
				message.getChannel().sendMessage(getObject(t,message.getAuthor(),message.getGuild()))
			);
		}
	}

	private static String generateSymbolMessage(String in) {
		char[] a = in.toLowerCase().replaceAll("[^A-Za-z0-9!?.#\\* ]", "").toCharArray(); //$NON-NLS-1$ //$NON-NLS-2$
		String out = ""; //$NON-NLS-1$
		for(char c : a) {
			if(c >= 48 && c <= 57) {
				out+=':';
				switch(c - 48) {
				case 0: out+="zero"; break; //$NON-NLS-1$
				case 1: out+="one"; break; //$NON-NLS-1$
				case 2: out+="two"; break; //$NON-NLS-1$
				case 3: out+="three"; break; //$NON-NLS-1$
				case 4: out+="four"; break; //$NON-NLS-1$
				case 5: out+="five"; break; //$NON-NLS-1$
				case 6: out+="six"; break; //$NON-NLS-1$
				case 7: out+="seven"; break; //$NON-NLS-1$
				case 8: out+="eight"; break; //$NON-NLS-1$
				case 9: out+="nine"; break; //$NON-NLS-1$
				default: out+="NaN"; break; //$NON-NLS-1$
				}
				out+=':';
			} else if(c == 32)
				out+='\t';
			else
				switch(c) {
				default: out+=":regional_indicator_" + c + ":"; break; //$NON-NLS-1$ //$NON-NLS-2$
				case 'a': case 'b': case 'x': case 'o': out+= ":" + c + ":"; break; //$NON-NLS-1$ //$NON-NLS-2$
				case '.': out+=":black_small_square:"; break; //$NON-NLS-1$
				case '!': out+=":exclamation:"; break; //$NON-NLS-1$
				case '?': out+=":question:"; break; //$NON-NLS-1$
				case '#': out+=":hash:"; break; //$NON-NLS-1$
				case '*': out+=":asterisk:"; break; //$NON-NLS-1$
				case '$': out+=":heavy_dollar_sign:"; break; //$NON-NLS-1$
				}
		}
		return out;
	}
	
	private static EmbedObject getObject(String text, IUser user, IGuild guild) {
		return new EmbedBuilder().withDesc(text).withAuthorName(user.getName()).withAuthorIcon(user.getAvatarURL()).withColor(user.getColorForGuild(guild)).build();
	}
}
