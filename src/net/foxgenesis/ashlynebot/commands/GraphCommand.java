package net.foxgenesis.ashlynebot.commands;

import static net.foxgenesis.util.helper.DiscordHelper.ERROR;
import static net.foxgenesis.util.helper.DiscordHelper.outputMessageDelete;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.imageio.ImageIO;
import javax.script.ScriptEngine;
import javax.script.ScriptException;
import javax.script.SimpleBindings;

import net.foxgenesis.discord.command.Command;
import net.foxgenesis.discord.command.IPlusCommand;
import net.foxgenesis.util.JavaScriptUtil;
import net.foxgenesis.util.helper.DiscordHelper;
import sx.blah.discord.handle.obj.IMessage;

@Command(name = "graph", help = "Draw a graph", helpParams = "<function>", cooldown = true)
public class GraphCommand implements IPlusCommand {

	@Override
	public void handleCommand(IMessage message, String inputs) throws Exception {
		if(inputs != null && !inputs.isEmpty()) 
			test(message,inputs.replaceAll("exit\\(\\)", "")); //$NON-NLS-1$ //$NON-NLS-2$
		else outputMessageDelete(message,Messages.getString("GraphCommand.3"),ERROR); //$NON-NLS-1$
	}

	private static void test(IMessage message, String inputs) throws IOException {
		BufferedImage image = new BufferedImage(500, 500, BufferedImage.TYPE_INT_RGB);
		Graphics2D g = image.createGraphics();
		g.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
				RenderingHints.VALUE_ANTIALIAS_ON);

		g.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING,
				RenderingHints.VALUE_TEXT_ANTIALIAS_ON);

		g.setRenderingHint(RenderingHints.KEY_RENDERING,
				RenderingHints.VALUE_RENDER_QUALITY);

		ScriptEngine engine = JavaScriptUtil.getJSEngine();
		Map<String, Object> vars = new HashMap<>();
		try {
			double px = 0, py = 0;
			boolean go = false;
			for(double i=-(image.getWidth()/2); i<image.getWidth()/2; i+=0.1) {
				double x = i;
				vars.put("x", x); //$NON-NLS-1$
				double y = ((Number)engine.eval(inputs.replaceAll("[^x0-9*-><,+/%&|\\(\\)(Math.)(sin)(cos)(tan)(PI)(E)(abs)(pow)]", ""), new SimpleBindings(vars))).doubleValue(); //$NON-NLS-1$ //$NON-NLS-2$
				if(go) {
					g.drawLine(image.getWidth()/2 + (int)px, image.getHeight()/2 - (int)py, image.getWidth()/2 + (int)x, image.getHeight()/2 - (int)y);
				} else go = true;
				px = x;
				py = y;
			}
		} catch(ScriptException e) {
			outputMessageDelete(message,Messages.getString("GraphCommand.8") + e.getMessage(),ERROR); //$NON-NLS-1$
			message.delete();
			return;
		}
		g.setFont(new Font("arial", 0, 15)); //$NON-NLS-1$
		g.setColor(Color.white);
		g.drawString("500 x 500", 10, g.getFontMetrics().getHeight()); //$NON-NLS-1$

		g.dispose();
		ImageIO.write(image, "PNG", new File("graph.png")); //$NON-NLS-1$ //$NON-NLS-2$
		DiscordHelper.tryToDelete(message);
		message.getChannel().sendFile(DiscordHelper.getDefaultRoleEmbed(message.getAuthor(), message.getGuild()).withImage("attachment://graph.png").withFooterText(String.format(Messages.getString("GraphCommand.0"), inputs)).build(),new File("graph.png")); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
	}
}
