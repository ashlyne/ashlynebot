package net.foxgenesis.ashlynebot.commands;

import static net.foxgenesis.ashlynebot.Bot.delete;
import static net.foxgenesis.util.helper.DiscordHelper.ERROR;
import static net.foxgenesis.util.helper.DiscordHelper.INFO;
import static net.foxgenesis.util.helper.DiscordHelper.WARN;
import static net.foxgenesis.util.helper.DiscordHelper.outputMessage;
import static net.foxgenesis.util.helper.DiscordHelper.outputMessageDelete;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.rmi.server.UID;
import java.util.Map.Entry;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.stream.Collectors;

import javax.imageio.ImageIO;

import com.jhlabs.image.AbstractBufferedImageOp;

import net.foxgenesis.discord.command.Command;
import net.foxgenesis.discord.command.IPlusCommand;
import net.foxgenesis.util.ImageFilters;
import net.foxgenesis.util.helper.DiscordHelper;
import sx.blah.discord.handle.obj.IMessage;
import sx.blah.discord.util.RequestBuffer;

@Command(name = "pfilter", help = "Create a picture with an equation", helpParams = "<equation>", cooldown = true)
public class ImageFilterCommand implements IPlusCommand {

	private static final File f = new File("filter.png"); //$NON-NLS-1$
	private static final LinkedBlockingQueue<FilterRequest> q = new LinkedBlockingQueue<>(5);
	private static Thread t;
	static {
		t = new Thread(() -> {
			while(true) {
				try {
					FilterRequest req = q.take();
					String inputs = req.inputs;
					IMessage message = req.message;
					UID id = new UID(); 
					RequestBuffer.request(() -> {
						delete(outputMessage(message,Messages.getString("ImageFilterCommand.1"),INFO)); //$NON-NLS-1$ 
						DiscordHelper.TypingStatus.setTyping(message.getChannel(), id, true);
					});
					try {
						BufferedImage j = ImageFilters.filter(inputs.replaceAll("0xFFFFFF", 16777215 + "") //$NON-NLS-1$ //$NON-NLS-2$
								.replaceAll("0xFFFF", 65535 + "").replaceAll("0xFF", "" + 255),req.image); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$
						saveAndSend(j,message);
					} catch(Exception e) {
						RequestBuffer.request(() -> delete(outputMessage(message,Messages.getString("ImageFilterCommand.8") + e.getMessage(),ERROR),30_000)); //$NON-NLS-1$ 
					}
					DiscordHelper.TypingStatus.setTyping(message.getChannel(), id, false);

				} catch(Exception e) {
					e.printStackTrace();
				}
			}
		},"Image Filter Thread"); //$NON-NLS-1$
		t.start();
	}

	@Override
	public void handleCommand(IMessage message, String inputs) throws Exception {
		if(inputs != null && !inputs.trim().isEmpty()) {
			if(inputs.equalsIgnoreCase("list")) { //$NON-NLS-1$
				StringBuilder bb = new StringBuilder();
				for(Entry<String, AbstractBufferedImageOp> a : ImageFilters.getFilters().stream().sorted((a,b) -> a.getKey().compareToIgnoreCase(b.getKey())).collect(Collectors.toList())) {
					if(bb.length() > 1900) {
						final String out = bb.toString();
						RequestBuffer.request(() -> outputMessage(message, '\n' + out, INFO)).get();
						bb = new StringBuilder();
					}
					bb.append(String.format("__**%s**__: *%s*\n", a.getKey(), a.getValue())); //$NON-NLS-1$
				}
				final String out = bb.toString();
				RequestBuffer.request(() -> outputMessage(message, '\n' + out, INFO));
				return;
			}
			if(!ImageFilters.hasFilter(inputs)) {
				delete(outputMessage(message,"No filter with name",ERROR)); //$NON-NLS-1$
				return;
			}
			BufferedImage image = null;
			IMessage mm = message.getChannel().getMessageHistory(40).stream().filter(m -> (!m.getEmbeds().isEmpty()?
					m.getEmbeds().get(0).getImage() != null : false) || !m.getAttachments().isEmpty()).sorted(
							(a,b) -> b.getTimestamp().compareTo(a.getTimestamp())).findFirst().get();
			if(mm == null) {
				delete(outputMessage(message,Messages.getString("ImageFilterCommand.10"), WARN)); //$NON-NLS-1$ 
				return;
			}
			image = DiscordHelper.getDiscordImage(new URL(mm.getAttachments().get(0).getUrl()));
			delete(outputMessage(message,Messages.getString("ImageFilterCommand.0"),INFO)); //$NON-NLS-1$
			if(!q.add(new FilterRequest(inputs.replaceAll("exit\\(\\)", ""),message,image))) //$NON-NLS-1$ //$NON-NLS-2$
				delete(outputMessage(message,Messages.getString("ImageFilterCommand.12"),ERROR)); //$NON-NLS-1$ 
			return;
		}
		outputMessageDelete(message,Messages.getString("ImageFilterCommand.13"), ERROR); //$NON-NLS-1$
	}


	private static void saveAndSend(BufferedImage image, IMessage message) throws IOException {
		synchronized(f) {
			if(ImageIO.write(image, "PNG", f)) //$NON-NLS-1$
				message.getChannel().sendFile(f);
			else
				delete(outputMessage(message,Messages.getString("ImageFilterCommand.14"), ERROR)); //$NON-NLS-1$
		}
	}

	private static class FilterRequest {
		public final BufferedImage image;
		public final String inputs;
		public final IMessage message;
		public FilterRequest(String inputs,IMessage message, BufferedImage image) {
			this.image = image;
			this.inputs = inputs;
			this.message = message;
		}
	}

//	public static double eval(final String str) {
//		return new Object() {
//			int pos = -1, ch;
//
//			void nextChar() {
//				this.ch = (++this.pos < str.length()) ? str.charAt(this.pos) : -1;
//			}
//
//			boolean eat(int charToEat) {
//				while (this.ch == ' ') nextChar();
//				if (this.ch == charToEat) {
//					nextChar();
//					return true;
//				}
//				return false;
//			}
//
//			double parse() {
//				nextChar();
//				double x = parseExpression();
//				if (this.pos < str.length()) throw new RuntimeException(String.format(Messages.getString("ImageFilterCommand.27"),(char)this.ch,this.pos)); //$NON-NLS-1$
//				return x;
//			}
//
//			// Grammar:
//			// expression = term | expression `+` term | expression `-` term
//			// term = factor | term `*` factor | term `/` factor
//			// factor = `+` factor | `-` factor | `(` expression `)`
//			//        | number | functionName factor | factor `^` factor
//
//			double parseExpression() {
//				double x = parseTerm();
//				for (;;) {
//					if      (eat('+')) x += parseTerm(); // addition
//					else if (eat('-')) x -= parseTerm(); // subtraction
//					else return x;
//				}
//			}
//
//			double parseTerm() {
//				double x = parseFactor();
//				for (;;) {
//					if      (eat('*')) x *= parseFactor(); // multiplication
//					else if (eat('/')) x /= parseFactor(); // division
//					else return x;
//				}
//			}
//
//			double parseFactor() {
//				if (eat('+')) return parseFactor(); // unary plus
//				if (eat('-')) return -parseFactor(); // unary minus
//
//				double x;
//				int startPos = this.pos;
//				if (eat('(')) { // parentheses
//					x = parseExpression();
//					eat(')');
//				} else if ((this.ch >= '0' && this.ch <= '9') || this.ch == '.') { // numbers
//					while ((this.ch >= '0' && this.ch <= '9') || this.ch == '.') nextChar();
//					x = Double.parseDouble(str.substring(startPos, this.pos));
//				} else if (this.ch >= 'a' && this.ch <= 'z') { // functions
//					while (this.ch >= 'a' && this.ch <= 'z') nextChar();
//					String func = str.substring(startPos, this.pos);
//					x = parseFactor();
//					if (func.equals("sqrt")) x = Math.sqrt(x); //$NON-NLS-1$
//					else if (func.equals("sin")) x = Math.sin(Math.toRadians(x)); //$NON-NLS-1$
//					else if (func.equals("cos")) x = Math.cos(Math.toRadians(x)); //$NON-NLS-1$
//					else if (func.equals("tan")) x = Math.tan(Math.toRadians(x)); //$NON-NLS-1$
//					else if (func.equals("clamp")) x = x > 0xFF? 0xFF : x < 0? 0 : x; //$NON-NLS-1$
//					else throw new RuntimeException(Messages.getString("ImageFilterCommand.28") + func); //$NON-NLS-1$
//				} else {
//					throw new RuntimeException(String.format(Messages.getString("ImageFilterCommand.27"),(char)this.ch,this.pos)); //$NON-NLS-1$
//				}
//
//				if (eat('^')) x = Math.pow(x, parseFactor()); // exponentiation
//				if (eat('|')) x = (int)x | (int)parseFactor();
//				if (eat('&')) x = (int)x & (int)parseFactor();
//				if (eat('>')) x = (int)x >> (int)parseFactor();
//					if (eat('<')) x = (int)x << (int)parseFactor();
//
//					return x;
//			}
//		}.parse();
//	}
}
