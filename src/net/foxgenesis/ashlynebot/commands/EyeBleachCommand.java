package net.foxgenesis.ashlynebot.commands;

import net.foxgenesis.discord.command.Command;
import net.foxgenesis.discord.command.IPlusCommand;
import net.foxgenesis.util.TimeFilterSelector;
import net.foxgenesis.util.helper.DiscordHelper;
import net.foxgenesis.util.helper.RedditHelper;
import net.foxgenesis.util.helper.RedditHelper.RedditPost;
import sx.blah.discord.handle.obj.IMessage;

@Command(name = "eyebleach", help = "Summon some eye bleach", cooldown = true)
public class EyeBleachCommand implements IPlusCommand {
	private static final RedditHelper r = new RedditHelper("Eyebleach"); //$NON-NLS-1$
	
	@Override
	public void handleCommand(IMessage message, String inputs) throws Exception {
		DiscordHelper.tryToDelete(message);
		TimeFilterSelector<RedditPost> s = DiscordHelper.getFilterForGuild(message.getGuild(), "eyebleach.filter"); //$NON-NLS-1$
		RedditPost re = s.getRandomItem(r.getPosts(100, rr -> !rr.stickied));
		message.reply(re.url);
	}
}
