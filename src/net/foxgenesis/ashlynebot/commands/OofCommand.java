package net.foxgenesis.ashlynebot.commands;

import static net.foxgenesis.util.helper.DiscordHelper.ERROR;
import static net.foxgenesis.util.helper.DiscordHelper.outputMessageDelete;

import java.util.HashMap;
import java.util.Timer;
import java.util.TimerTask;

import net.foxgenesis.discord.command.Command;
import net.foxgenesis.discord.command.IPlusCommand;
import net.foxgenesis.util.helper.DiscordHelper;
import sx.blah.discord.handle.obj.IGuild;
import sx.blah.discord.handle.obj.IMessage;
import sx.blah.discord.handle.obj.IVoiceChannel;
import sx.blah.discord.util.audio.AudioPlayer;

@Command(name = "oof", help = "plays oof in your current voice channel", isHidden = true, isSuperCommand = true)
public class OofCommand implements IPlusCommand {
	private HashMap<IGuild,Timer> guilds = new HashMap<>();
	@Override
	public void handleCommand(IMessage message, String inputs) throws Exception {
		message.delete();
		IGuild guild = message.getGuild();
		IVoiceChannel voice = DiscordHelper.getVoiceChannelForUser(message.getAuthor(), guild);
		if(voice == null) {
			outputMessageDelete(message,Messages.getString("OofCommand.1"),ERROR); //$NON-NLS-1$ 
			return;
		}
		AudioPlayer player = AudioPlayer.getAudioPlayerForGuild(guild);
		if(player.getPlaylistSize() > 0) {
			outputMessageDelete(message,Messages.getString("OofCommand.3"),ERROR); //$NON-NLS-1$ 
			return;
		}
		Timer timer;
		if(this.guilds.containsKey(guild)) {
			timer = this.guilds.get(guild);
			timer.cancel();
			timer = new Timer();
		}
		else {
			timer = new Timer();
			this.guilds.put(guild, timer);
		}
		player.setVolume(0.6f);
		voice.join();
		//long time = player.queue(AudioSystem.getAudioInputStream(OofCommand.class.getResourceAsStream("oof.mp3"))).getTotalTrackTime();
		timer.schedule(new TimerTask() {
			@Override
			public void run() {
				System.out.println("Leaving..."); //$NON-NLS-1$
				voice.leave();
			}
		}, 2000l);
	}
}
