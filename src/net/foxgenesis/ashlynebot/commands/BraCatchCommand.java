package net.foxgenesis.ashlynebot.commands;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.ConcurrentHashMap;

import net.foxgenesis.ashlynebot.commands.AbstractCatchCommand.CatchInv;
import net.foxgenesis.ashlynebot.commands.BraCatchCommand.BraInv;
import net.foxgenesis.discord.command.Command;
import net.foxgenesis.util.helper.RandomHelper;
import sx.blah.discord.handle.obj.IChannel;
import sx.blah.discord.handle.obj.IGuild;
import sx.blah.discord.handle.obj.IMessage;
import sx.blah.discord.handle.obj.IUser;

@Command(name = "bra", help = "catch bras", helpParams = "[inv]", cooldown = true)
public class BraCatchCommand extends AbstractCatchCommand<BraInv> {
	public BraCatchCommand() {
		super("bra", user -> {return new BraInv(user.getLongID());}); //$NON-NLS-1$
	}

	@Override
	protected String onCatch(IUser user, BraInv inv) {
		Object[] roll = BraType.roll();
		BraType type = (BraType) roll[0];
		int amount = (int) roll[1];
		if(type == BraType.NORMAL)
			inv.setCount(inv.getCount() + amount);
		else
			inv.putExtra(type,amount);
		return type.getLine(amount);
	}

	private static final HashMap<Long, BraFight> fights = new HashMap<>();
	@Override
	protected void handleArgument(final IMessage message, final IUser user, BraInv inv, String command, String rest) {
			if(rest.length() > 1 && rest.charAt(0) == ' ')
				rest = rest.substring(1);
			final IGuild guild = message.getGuild();

			switch(command) {
			default:break;
			case "f": //$NON-NLS-1$
				BraFight fight = null;
				if(fights.containsKey(user.getLongID())) 
					fight = fights.get(user.getLongID());
				else {
					Player p1 = new Player(user.getDisplayName(guild),inv);
					fight = new BraFight(p1,new Player(p1));
					fights.put(user.getLongID(), fight);
				}
				if(fight.fight(message.getChannel())) 
					fights.remove(user.getLongID());
				break;
			}
	}

	protected static class BraInv extends CatchInv {
		private static final long serialVersionUID = -624226320668576598L;
		private final ConcurrentHashMap<BraType,Integer> extra = new ConcurrentHashMap<>();
		private final long id;

		private BraInv(long id) {this.id = id;}

		public void putExtra(BraType type, int amount) {
			synchronized(this.extra) {
				int before = this.extra.containsKey(type) ? 
						this.extra.get(type) : 0;
						this.extra.put(type, before+amount);
			}
			this.shouldUpdate = true;
		}

		@Override
		public String toString() {
			String output = "```markdown\nInventory\n=============\n"; //$NON-NLS-1$
			long total = getCount();
			for(Entry<BraType,Integer> type : this.extra.entrySet())
				total+=type.getValue();

			output += String.format("* Bra: [%s]" + (DEBUG? " (%.2f%%)" : ""), getCount(), ((double)getCount() / total) * 100) + '\n'; //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
			for(Entry<BraType,Integer> type : this.extra.entrySet()) 
				output += String.format("* %s: [%s]" + (DEBUG? " (%.2f%%)" : ""), type.getKey().braName, type.getValue(), ((double)type.getValue() / total) * 100) + '\n'; //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
			output += String.format("Total Bras: %s\n=============", total); //$NON-NLS-1$
			output+="```"; //$NON-NLS-1$
			return output;
		}
	}

	private static class BraFight {
		private final Player p1,p2;

		private BraFight(Player p1, Player p2) {
			this.p1 = p1;
			this.p2 = p2;
		}

		private boolean fight(IChannel channel) {
			short winner = 0;
			String fightChat = ""; //$NON-NLS-1$

			long dmg = getDamage(this.p1);
			fightChat+=String.format("%s dealt %s HP!\n", this.p1.name, dmg); //$NON-NLS-1$
			if(this.p2.dealDamage(dmg)) 
				winner = 1;
			else {
				dmg = getDamage(this.p2);
				fightChat+=String.format("\n%s dealt %s HP!\n", this.p2.name, dmg); //$NON-NLS-1$
				if(this.p1.dealDamage(dmg))
					winner = 2;
			}

			fightChat+='\n'+playerHealth(this.p1)+'\n'+playerHealth(this.p2);

			if(winner == 1) {
				int reward = rand.nextInt(10_000) + 300;
				fightChat+="\n\n" + this.p1.name + " wins `" + reward + "` bras!"; //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
				this.p1.inv.setCount(this.p1.inv.getCount() + reward);
				this.p1.inv.shouldUpdate = true;
			}

			channel.sendMessage(fightHeader(this.p1,this.p2,winner) + fightChat);
			return winner != 0;
		}

		private static String fightHeader(Player p1, Player p2, short winner) {
			String output = "**:bikini:"; //$NON-NLS-1$
			if(winner == 0) 
				output+=String.format("%s VS %s", p1.name.replaceAll("\\*", ""), p2.name); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
			else 
				output+=String.format("%S WINS THE BATTLE!", (winner == 1? p1.name : p2.name).replaceAll("\\*", "")); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
			return output+="\n----------------------------------**\n"; //$NON-NLS-1$
		}

		private static String playerHealth(Player p) {
			return  String.format("%s has `%s`/`%s` HP left.", p.name, p.health, p.maxHealth); //$NON-NLS-1$
		}

		private static long getDamage(Player player) {
			return (long)(rand.nextDouble() * player.power * (player.isPlayer? 1.5 : 1));
		}
	}

	private static class Player {
		private final String name;
		private final long maxHealth;
		private long health;
		private long power;
		private final BraInv inv;
		private final boolean isPlayer;

		private Player(String name, BraInv inv) {
			this.inv = inv;
			this.isPlayer = inv.id != -1;
			this.name = (this.isPlayer? "**" : "") + name + (this.isPlayer? "**" : ""); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$
			this.health = inv.getCount();
			this.power = inv.getCount();
			for(Entry<BraType,Integer> entry: inv.extra.entrySet()) {
				BraType type = entry.getKey();
				int amt = entry.getValue();
				this.health+=type.health*amt;
				this.power+=type.power*amt;
			}
			this.maxHealth = this.health;
		}

		private Player(Player p) {
			this.inv = null;
			this.isPlayer = false;
			this.name = "RANDOM NAME"; //$NON-NLS-1$

			this.maxHealth = p.maxHealth - (p.maxHealth > 2500? 2500 : 0) + rand.nextInt(5000);
			this.health = this.maxHealth;

			this.power = this.maxHealth/2;
		}

		private boolean dealDamage(long damage) {
			if((this.health-=damage) < 0) {
				this.health = 0;
				return true;
			}
			return false;
		}
	}

	private static enum BraType {
		NORMAL(0.9d, 10, 1, 1, "Bra", ":bikini: | You found **%1$s** *%2$s(s)*!"), //$NON-NLS-1$ //$NON-NLS-2$
		SPORTS(0.2d, 5, 10, 5, "Sports Bra", ":cartwheel: | You found **%1$s** *%2$s(s)*!"), //$NON-NLS-1$ //$NON-NLS-2$
		SEXY(0.001d, 1, 500, 250, "Sexy Bra", ":gem: **%1$s %2$s Found** :gem:"); //$NON-NLS-1$ //$NON-NLS-2$

		private final double chance;
		private final int range, health, power;
		private String braName, line;
		BraType(double chance, int range, int health, int power, String braName, String line) {
			this.chance = chance;
			this.range = range;
			this.braName = braName;
			this.line = line;
			this.health = health;
			this.power = power;
		}

		private String getLine(int amt) {
			return String.format(this.line, amt, this.braName);
		}

		private static Object[] roll() {
			BraType type = RandomHelper.getWeightedRandom(weights.entrySet().stream(), rand);
			int amt = rand.nextInt(type.range) + 1;
			return new Object[]{type,amt};
		}

		private static Map<BraType,Double> weights;
		static {
			weights = new HashMap<>();
			Arrays.stream(BraType.values()).forEach(type -> weights.put(type, type.chance));
		}
	}

	private static final boolean DEBUG = false;
	public static void main(String[] args) {
		BraInv inv = new BraInv(-1);
		for(int i=0; i<100_000; i++) {
			Object[] roll = BraType.roll();
			BraType type = (BraType) roll[0];
			int amount = (int) roll[1];

			if(type == BraType.NORMAL)
				inv.setCount(inv.getCount() + amount);
			else
				inv.putExtra(type,amount);
		}
		System.out.println(inv);
	}
}
