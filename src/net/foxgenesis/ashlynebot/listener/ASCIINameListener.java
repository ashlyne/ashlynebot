package net.foxgenesis.ashlynebot.listener;

import static net.foxgenesis.ashlynebot.Bot.genChan;
import static net.foxgenesis.util.helper.DiscordHelper.ERROR;
import static net.foxgenesis.util.helper.DiscordHelper.INFO;
import static net.foxgenesis.util.helper.DiscordHelper.outputMessageDelete;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import net.foxgenesis.ashlynebot.Bot;
import net.foxgenesis.ashlynebot.PlusCommands;
import net.foxgenesis.ashlynebot.config.BooleanField;
import net.foxgenesis.ashlynebot.events.BotReadyEvent;
import net.foxgenesis.discord.command.Command;
import net.foxgenesis.discord.command.IPlusCommand;
import net.foxgenesis.discord.guild.GuildDataManager;
import net.foxgenesis.discord.guild.SQLDataType;
import net.foxgenesis.discord.guild.StorageKey;
import net.foxgenesis.discord.listener.ReadyListener;
import net.foxgenesis.util.helper.FileHelper;
import net.foxgenesis.util.helper.StringHelper;
import sx.blah.discord.api.events.EventSubscriber;
import sx.blah.discord.handle.impl.events.guild.member.NicknameChangedEvent;
import sx.blah.discord.handle.obj.IGuild;
import sx.blah.discord.handle.obj.IMessage;
import sx.blah.discord.handle.obj.IRole;
import sx.blah.discord.handle.obj.IUser;
import sx.blah.discord.util.RequestBuffer;

public class ASCIINameListener implements ReadyListener {
	private static final Logger LOGGER = LoggerFactory.getLogger("ASCII Name Listener"); //$NON-NLS-1$
	private static HashMap<String,ArrayList<String>> names = new HashMap<>();
	private static final StorageKey KEY = new StorageKey("names", SQLDataType.BLOB.name, SQLDataType.BLOB); //$NON-NLS-1$
	private static final BooleanField ENABLED = new BooleanField("ascii_filter", false, true); //$NON-NLS-1$

	public ASCIINameListener() {
		GuildDataManager m = (GuildDataManager) Bot.getData();
		m.pushStorage(KEY);
		PlusCommands.addCommand(new AsciiDisable());
	}

	@SuppressWarnings("static-method")
	@EventSubscriber
	public void userNicknameChanged(NicknameChangedEvent e) {
		IGuild guild = e.getGuild();
		if(!ENABLED.optFrom(guild))
			return;

		IUser user = e.getUser();
		String name = e.getNewNickname().orElse(user.getName());
		LOGGER.debug("Checking name: " + name); //$NON-NLS-1$
		ArrayList<String> l = getNamesForGuild(guild);
		IRole muted = getMutedRoleFromGuild(guild);
		if(muted == null)
			return;
		if(!StringHelper.containsNonASCII(name)) {
			if(l.contains(user.getStringID()))
				mute(user,guild,l,muted,false,true,false);
		} else mute(user,guild,l,muted,true,true,false);
	}

	private static void scanGuild(IGuild g, boolean mute, boolean quiet) {
		LOGGER.debug("Scanning guild " + g.getName()); //$NON-NLS-1$
		ArrayList<String> l = getNamesForGuild(g);
		IRole muted = getMutedRoleFromGuild(g);
		if(muted == null)
			return;
		if(mute) {
			l.clear();
			g.getUsers().stream().filter(u -> StringHelper.containsNonASCII(u.getDisplayName(g))).forEach(u -> mute(u,g,l,muted,true,false,quiet));
		} else 
			g.getUsersByRole(muted).stream().filter(u -> l.contains(u.getStringID())).forEach(u -> mute(u,g,l,muted,false,false,quiet));
	}


	@Override
	public void onReadyEvent(BotReadyEvent e) {
		load();
		Runtime.getRuntime().addShutdownHook(new Thread(() -> save(),"Names Save")); //$NON-NLS-1$
		Bot.getClient().getGuilds().stream().filter(g -> {
			try {
				if(ENABLED.isPresent(g))
					return ENABLED.optFrom(g);
			} catch(Exception ee) {
				ee.printStackTrace();
			}
			return false;
		}).forEach(g -> scanGuild(g,true,true));
		
		save();
	}

	private static ArrayList<String> getNamesForGuild(IGuild g) {
		names.putIfAbsent(g.getStringID(), new ArrayList<String>());
		return names.get(g.getStringID());
	}

	private static IRole getMutedRoleFromGuild(IGuild g) {
		for(IRole r: g.getRoles())
			if(r.getName().equalsIgnoreCase("muted")) //$NON-NLS-1$
				return r;
		LOGGER.warn("[%s]: Failed to get muted role!\n",g.getName()); //$NON-NLS-1$
		return null;
	}

	private static void mute(IUser u, IGuild g, ArrayList<String> l, IRole muted, boolean state, boolean save, boolean quiet) {
		if(muted == null)
			return;
		if(state) {
			RequestBuffer.request(() -> {
				l.add(u.getStringID());
				LOGGER.debug(String.format("muting %s from %s",u.getDisplayName(g),g.getName())); //$NON-NLS-1$
				u.addRole(muted);
				if(!quiet)
					genChan(g).sendMessage(u.mention() + Messages.getString("ASCIINameListener.0")); //$NON-NLS-1$
			});
		} else {
			RequestBuffer.request(() -> {
				l.remove(u.getStringID());
				LOGGER.debug(String.format("unmuting %s from %s",u.getDisplayName(g),g.getName())); //$NON-NLS-1$
				u.removeRole(muted);
				if(!quiet)
					genChan(g).sendMessage(u.mention() + Messages.getString("ASCIINameListener.1")); //$NON-NLS-1$
			});
		}
		if(save)
			save();
	}

	private static void save() {
		try {
			FileHelper.saveToFile(new File(Bot.dataDir, "names.bin"), o -> { //$NON-NLS-1$
				synchronized(names) {
					try {
						o.writeObject(names);
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
			});
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@SuppressWarnings("unchecked")
	private static void load() {
		try {
			FileHelper.loadFromFile(new File(Bot.dataDir, "names.bin"), in -> { //$NON-NLS-1$
				try {
					names = (HashMap<String, ArrayList<String>>) in.readObject();
				} catch (ClassNotFoundException | IOException e) {
					names = new HashMap<>();
					e.printStackTrace();
				}
			});
		} catch (IOException e) {
			names = new HashMap<>();
		}
	}

	@Command(name = "asciifilter", isOwnerCommand = true, isSuperCommand = true, help = "Disable/Enable ASCII name check", helpParams = "<true | false>")
	private static class AsciiDisable implements IPlusCommand {
		@Override
		public void handleCommand(IMessage message, String inputs) throws Exception {
			if(inputs != null) {
				boolean b = Boolean.parseBoolean(inputs);
				ENABLED.set(message.getGuild(), b);
				if(b) {
					outputMessageDelete(message,Messages.getString("ASCIINameListener.2"),INFO); //$NON-NLS-1$ 
					scanGuild(message.getGuild(),true,false);
				} else {
					outputMessageDelete(message,Messages.getString("ASCIINameListener.3"),INFO); //$NON-NLS-1$ 
					scanGuild(message.getGuild(),false,false);
				}
				return;
			}
			outputMessageDelete(message,Messages.getString("ASCIINameListener.4"),ERROR); //$NON-NLS-1$
		}
	}
}
