package net.foxgenesis.ashlynebot.listener;

import java.util.HashMap;

import net.foxgenesis.ashlynebot.Bot;
import net.foxgenesis.ashlynebot.PlusCommands;
import net.foxgenesis.ashlynebot.events.BotReadyEvent;
import net.foxgenesis.discord.command.Command;
import net.foxgenesis.discord.command.IPlusCommand;
import net.foxgenesis.discord.guild.IGuildData;
import net.foxgenesis.discord.listener.ReadyListener;
import sx.blah.discord.api.events.EventSubscriber;
import sx.blah.discord.handle.impl.events.guild.channel.message.MessageReceivedEvent;
import sx.blah.discord.handle.obj.IGuild;
import sx.blah.discord.handle.obj.IMessage;
import sx.blah.discord.handle.obj.IUser;

public class SlowmoMode implements ReadyListener {
	@SuppressWarnings("nls")
	private static final String SLOWMOKEY = "slow_mo",SLOWMOKEYDATA = SLOWMOKEY+".d";
	private static final long DELAY_TIME = 3_000;

	static {
		PlusCommands.addCommand(new SlowmoCommand());
	}
	
	@Override
	public void onReadyEvent(BotReadyEvent e) {
		
	}
	
	@SuppressWarnings({ "static-method", "nls" })
	@EventSubscriber
	public void onMessage(MessageReceivedEvent e) {
		if(e.getChannel().isPrivate())
			return;
		IGuild guild = e.getGuild();
		HashMap<Long,Long> data = isSlowMo(guild);
		if(data == null)
			return;
		
		IUser user = e.getAuthor();
		long lastSpoke = System.currentTimeMillis() - (data.containsKey(user.getLongID())? data.get(user.getLongID()) : 0l);
		
		if(lastSpoke >= DELAY_TIME) {
			data.put(user.getLongID(), System.currentTimeMillis());
			return;
		}
		e.getMessage().delete();
		try {
			user.getOrCreatePMChannel().sendMessage("**WOAH!** Slow down please. (eta: " + lastSpoke + " ms)");
		} catch(Exception e1){System.err.println("Failed to send slowmo message to " + user.getLongID());} //$NON-NLS-1$
	}
	
	@SuppressWarnings("unchecked")
	private static HashMap<Long,Long> isSlowMo(IGuild guild) {
		IGuildData data = Bot.getData().getDataForGuild(guild);
		if(data.getTempData().containsKey(SLOWMOKEY)? (boolean) data.getTempData().get(SLOWMOKEY) : false) {
			if(!data.getTempData().containsKey(SLOWMOKEYDATA))
				data.getTempData().put(SLOWMOKEYDATA, new HashMap<Long,Long>());
			return (HashMap<Long, Long>) data.getTempData().get(SLOWMOKEYDATA);
		}
		return null;
	}
	
	@SuppressWarnings("nls")
	private static void setSlowMo(IGuild guild, boolean state) {
		IGuildData data = Bot.getData().getDataForGuild(guild);
		data.getTempData().put(SLOWMOKEY, state);
		guild.getDefaultChannel().sendMessage("Slow Mo " + (state? "Enabled" : "Disabled"));
	}
	
	@Command(name = "slowmo", help = "Enable/Disable slow-mo chat", helpParams = "[true | false]", isSuperCommand = true, isOwnerCommand = true)
	private static class SlowmoCommand implements IPlusCommand {
		@Override
		public void handleCommand(IMessage message, String inputs) throws Exception {
			if(inputs == null) {
				HashMap<Long,Long> data = isSlowMo(message.getGuild());
				setSlowMo(message.getGuild(),data==null);
				return;
			}
			setSlowMo(message.getGuild(),Boolean.parseBoolean(inputs));
		}
	}
}
