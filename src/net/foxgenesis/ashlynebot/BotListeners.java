package net.foxgenesis.ashlynebot;

import java.util.ArrayList;
import java.util.List;

import net.foxgenesis.ashlynebot.events.BotReadyEvent;
import net.foxgenesis.ashlynebot.listener.ASCIINameListener;
import net.foxgenesis.ashlynebot.listener.SlowmoMode;
import net.foxgenesis.discord.listener.ReadyListener;

public final class BotListeners {
	private static List<ReadyListener> onReady = new ArrayList<>();
	
	static void init() {
		add(new ASCIINameListener(),
				new UtilityListeners(),
				new SlowmoMode());
	}
	
	static void ready(BotReadyEvent e) {
		onReady.forEach(o -> o.onReadyEvent(e));
	}
	
	public static void add(Object... j) {
		for(Object a : j)
			add(a);
	}
	
	public static void add(Object j) {
		Bot.getClient().getDispatcher().registerListener(j);
		if(j instanceof ReadyListener)
			onReady.add((ReadyListener)j);
	}
}
