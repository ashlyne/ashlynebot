package net.foxgenesis.ashlynebot;

import net.foxgenesis.discord.guild.JSONObjectAdv;
import sx.blah.discord.api.events.EventSubscriber;
import sx.blah.discord.handle.impl.events.guild.member.UserBanEvent;
import sx.blah.discord.handle.impl.events.guild.member.UserJoinEvent;
import sx.blah.discord.handle.impl.events.guild.member.UserLeaveEvent;
import sx.blah.discord.handle.obj.IChannel;
import sx.blah.discord.handle.obj.IGuild;
import sx.blah.discord.handle.obj.Permissions;

public class UtilityListeners {
	@SuppressWarnings("static-method")
	@EventSubscriber
	public void userLeft(UserLeaveEvent e) {
		if(isEnabled(e.getGuild(),GuildIOEvent.USER_LEAVE))
			default_Channel(e.getGuild()).sendMessage(String.format(InfoMessages.getString("UtilityListeners.0"), e.getUser().mention(true))); //$NON-NLS-1$
	}

	@SuppressWarnings("static-method")
	@EventSubscriber
	public void userLeft(UserJoinEvent e) {
		if(isEnabled(e.getGuild(),GuildIOEvent.USER_JOIN))
			default_Channel(e.getGuild()).sendMessage(String.format(InfoMessages.getString("UtilityListeners.1"), e.getUser().mention(true))); //$NON-NLS-1$
	}

	@SuppressWarnings("static-method")
	@EventSubscriber
	public void userLeft(UserBanEvent e) {
		if(isEnabled(e.getGuild(),GuildIOEvent.USER_BAN))
			default_Channel(e.getGuild()).sendMessage(String.format(InfoMessages.getString("UtilityListeners.2"), e.getUser().mention(true))); //$NON-NLS-1$
	}

	private static boolean isEnabled(IGuild guild, GuildIOEvent e) {
		if(default_Channel(guild).getModifiedPermissions(Bot.getClient().getOurUser()).contains(Permissions.SEND_MESSAGES)) {
			JSONObjectAdv config = Bot.getData().getDataForGuild(guild).getConfig();
			if(config != null) {
				boolean su = config.optBoolean("greetings",true); //$NON-NLS-1$
				return su? config.optBoolean("greetings." + e,true) : false; //$NON-NLS-1$
			}
			return true;
		}
		return false;
	}

	private static IChannel default_Channel(IGuild guild) {
		return Bot.getData().getDataForGuild(guild).getDefaultChannel();
	}

	private static enum GuildIOEvent {
		USER_LEAVE,
		USER_JOIN,
		USER_BAN
	}
}
