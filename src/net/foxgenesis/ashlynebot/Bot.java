package net.foxgenesis.ashlynebot;
import java.io.File;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Random;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.github.sheigutn.pushbullet.Pushbullet;
import com.github.sheigutn.pushbullet.items.push.sendable.defaults.SendableNotePush;
import com.mashape.unirest.http.Unirest;

import net.foxgenesis.ashlynebot.input.InputManager;
import net.foxgenesis.discord.guild.GuildDataManager;
import net.foxgenesis.discord.guild.IGuildDataManager;
import net.foxgenesis.sql.MySQLConnection;
import net.foxgenesis.util.SingleInstanceUtil;
import net.foxgenesis.util.helper.DiscordHelper;
import net.foxgenesis.util.web.IWebPanelData;
import net.foxgenesis.util.web.IWebPanelProvider;
import net.foxgenesis.util.web.WebPanel;
import sx.blah.discord.Discord4J;
import sx.blah.discord.Discord4J.Discord4JLogger;
import sx.blah.discord.api.IDiscordClient;
import sx.blah.discord.handle.obj.IChannel;
import sx.blah.discord.handle.obj.IGuild;
import sx.blah.discord.handle.obj.IMessage;
import sx.blah.discord.handle.obj.IUser;
import sx.blah.discord.handle.obj.Permissions;

//TODO: clean up
public class Bot {
	private static IDiscordClient client;
	private static WebPanel panel;
	private static GuildDataManager data;
	private static MySQLConnection sql;
	private static Pushbullet pushbullet;


	public static final File dataDir = new File("data"); //$NON-NLS-1$
	public static boolean DEBUG = false;
	public static final Random rand = new Random();
	
	private static final Logger LOGGER = LoggerFactory.getLogger("AshlyneBot"); //$NON-NLS-1$
	
	public static void main(String[] args) throws ClassNotFoundException, SQLException {
		//System.setProperty("log4j.configurationFile", "log4j2.xml");
		
		LOGGER.info("Checking for running instances.."); //$NON-NLS-1$
		SingleInstanceUtil.waitAndGetLock(7);

		File libs = new File("lib"); //$NON-NLS-1$
		libs.mkdir();
		dataDir.mkdir();
		for(int i=0; i<args.length; i++) {
			String a = args[i];
			if(a.charAt(0) == '-')
				switch(a.substring(1)) {
				case "panel": //$NON-NLS-1$
					panel = new WebPanel();
					panel.setAllowed(true);
					break;
				case "trace": //$NON-NLS-1$
					((Discord4JLogger) Discord4J.LOGGER).setLevel(Discord4J.Discord4JLogger.Level.TRACE);
					break;
				case "debug": //$NON-NLS-1$
					((Discord4JLogger) Discord4J.LOGGER).setLevel(Discord4J.Discord4JLogger.Level.DEBUG);
					DEBUG = true;
					break;
				}
		}
		LOGGER.info("Creating api connections"); //$NON-NLS-1$
		pushbullet = new Pushbullet(Messages.getString("Bot.pushbulletAPI")); //$NON-NLS-1$
		Unirest.setDefaultHeader("X-Mashape-Key", Messages.getString("Bot.mashapekey")); //$NON-NLS-1$ //$NON-NLS-2$

		LOGGER.info("Creating database connection"); //$NON-NLS-1$
		sql = new MySQLConnection(Messages.getString("Bot.0"),3306,Messages.getString("Bot.1"),Messages.getString("Bot.2"),Messages.getString("Bot.3")); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$
		data = new GuildDataManager(sql);

		LOGGER.info("Creating discord connection"); //$NON-NLS-1$
		client = Example.createClient(Messages.getString("Bot.4"), true); //$NON-NLS-1$
		client.getDispatcher().registerListener(new AnnotationListener()); 

		InputManager manager = new InputManager();
		manager.start();
	}

	static void shutdown() {shutdown(0);}
	static void shutdown(int reason) {closeResources();System.exit(reason);}
	static void closeResources() {
		try {
			LOGGER.info("**** SHUTDOWN REQUESTED ****"); //$NON-NLS-1$
			TimedEvents.unload();
			DiscordHelper.unload();
			sql.getConnection().close();
			if(Bot.getClient().isLoggedIn())
				Bot.getClient().logout();
		} catch(Exception e) {
			e.printStackTrace();
			pushbullet().push(new SendableNotePush("Error while closing resources",DiscordHelper.dump(e))); //$NON-NLS-1$
		}
	}


	public static IDiscordClient getClient() {return client;}
	static IWebPanelData getWebPanelData() {return panel;}
	public static IWebPanelProvider getWebProvider() {return panel;}
	public static IGuildDataManager getData() {return data;}
	public static Connection SQL() {return sql.getConnection();}

	public static boolean hasAdminPerms(IUser user, IGuild guild) {
		if(isGuildOwner(user,guild))
			return true;
		return guild.getRolesForUser(user).stream().map(u -> 
		u.getPermissions()).anyMatch(e -> e.contains(Permissions.ADMINISTRATOR));
	}

	public static boolean isGuildOwner(IUser user, IGuild guild) {return user.equals(guild.getOwner());}
	public static boolean isBotAdmin(IUser user) {return user.equals(client.getApplicationOwner());}
	public static IChannel genChan(IGuild g) {return data.getDataForGuild(g).getDefaultChannel();}
	public static void delete(IMessage message) {DiscordHelper.TimedMsgRemover.delete(message);}
	public static void delete(IMessage message, long time) {DiscordHelper.TimedMsgRemover.delete(message,time);}
	public static Pushbullet pushbullet() {return pushbullet;}
}