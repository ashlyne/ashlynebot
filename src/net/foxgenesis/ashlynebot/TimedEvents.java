package net.foxgenesis.ashlynebot;

import static net.foxgenesis.ashlynebot.Bot.pushbullet;

import java.util.HashMap;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.github.sheigutn.pushbullet.items.push.sendable.defaults.SendableNotePush;

import net.foxgenesis.ashlynebot.input.ConsoleCommand;
import net.foxgenesis.ashlynebot.input.InputManager;
import net.foxgenesis.discord.timed.TimedEvent;
import net.foxgenesis.util.helper.DiscordHelper;
//TODO: Save non ran events for next startup
public final class TimedEvents {
	private static final Logger LOGGER = LoggerFactory.getLogger(TimedEvents.class);
	private static final Timer t = new Timer("Timed Events"); //$NON-NLS-1$
	private static final Map<String,TimedEvent> timers = new HashMap<>();
	static void init() {
		InputManager.registerConsoleCommand(ConsoleCommand.newCommand("timers", (inputs, split) -> { //$NON-NLS-1$
			if(inputs == null) {
				StringBuilder b = new StringBuilder();
				b.append("Timers:\n=====================================\n"); //$NON-NLS-1$
				timers.entrySet().stream().sorted((a,bc) -> a.getKey().compareTo(bc.getKey())).filter(
						n -> !n.getKey().equals("help")).forEachOrdered(e -> b.append(e.getKey() + '\n')); //$NON-NLS-1$
				b.append("====================================="); //$NON-NLS-1$
				System.out.println(b);
				return true;
			} else if(timers.containsKey(inputs)) {
				runGlobalEvent(inputs);
				return true;
			}
			return false;
		}));
	}

	public static void register(TimedEvent e) {
		if(e.repeat)
			t.scheduleAtFixedRate(new TimerTask() {
				@Override
				public void run() {
					try {
						if(e.shouldStop())
							this.cancel();
						else if(e.useGuilds)
							Bot.getClient().getGuilds().stream().forEach(e::run);
						else
							e.run(null);
					} catch(Exception e) {
						pushbullet().push(new SendableNotePush("Error in timed event",DiscordHelper.dump(e))); //$NON-NLS-1$
					}
				}

			}, e.delay, e.period);
		else
			t.schedule(new TimerTask() {
				@Override
				public void run() {
					try {
						if(e.shouldStop())
							this.cancel();
						else if(e.useGuilds)
							Bot.getClient().getGuilds().stream().forEach(e::run);
						else
							e.run(null);
					} catch(Exception e) {
						pushbullet().push(new SendableNotePush("Error in timed event",DiscordHelper.dump(e))); //$NON-NLS-1$
					}
				}

			}, e.delay, e.period);
	}

	public static void registerGlobalEvent(TimedEvent e) {
		if(timers.containsKey(e.getName()))
			throw new UnsupportedOperationException("Name is already taken"); //$NON-NLS-1$
		timers.put(e.getName(), e);
		LOGGER.debug("[TimedEvents]: Registered new timed event [%s]", e.getName()); //$NON-NLS-1$
	}

	public static void runGlobalEvent(String name) {
		TimedEvent t = timers.get(name);
		if(t == null)
			throw new UnsupportedOperationException("No TimedEvent found with given name"); //$NON-NLS-1$
		LOGGER.info("[TimedEvents]: Running timed event \"%s\"...",t.getName()); //$NON-NLS-1$
		TimedEvents.t.schedule(new TimerTask() {
			@Override
			public void run() {
				if(t.shouldStop())
					this.cancel();
				else if(t.useGuilds)
					Bot.getClient().getGuilds().stream().forEach(t::run);
				else t.run(null);
			}
		}, 0);
	}

	static void unload() {
		t.cancel();
	}
}
