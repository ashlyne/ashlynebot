package net.foxgenesis.ashlynebot.filters;

import java.util.regex.Pattern;

import net.foxgenesis.ashlynebot.Bot;
import net.foxgenesis.ashlynebot.TimedEvents;
import net.foxgenesis.discord.filters.Filter;
import net.foxgenesis.discord.filters.ITextFilter;
import net.foxgenesis.discord.timed.TimedEvent;
import net.foxgenesis.util.helper.DiscordHelper;
import sx.blah.discord.handle.obj.IGuild;
import sx.blah.discord.handle.obj.IMessage;

@Filter(name = "Anti-Racism")
public class AntiRacismFilter implements ITextFilter {
	private static final Pattern pattern = Pattern.compile("ni[gb(:b:)][gb(:b:)][ae]", Pattern.CASE_INSENSITIVE); //$NON-NLS-1$

	@Override
	public void handle(IMessage message) {
		if(DiscordHelper.BannedBotUsers.ban(message.getAuthor(), "Racism (5min ban)", Bot.getClient().getOurUser())) //$NON-NLS-1$
			TimedEvents.register(new TimedEvent("timed_ban", TimedEvent.MINUTE * 5) { //$NON-NLS-1$
				@Override
				public void run(IGuild guild) {
					DiscordHelper.BannedBotUsers.unban(message.getAuthor());
				}
				@Override
				public boolean shouldStop() {
					return false;
				}
			});
	}

	@Override
	public boolean isAllowed(IMessage message) {
		return pattern.matcher(message.getFormattedContent()).find();
	}

}
