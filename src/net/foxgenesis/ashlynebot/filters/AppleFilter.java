package net.foxgenesis.ashlynebot.filters;

import static net.foxgenesis.ashlynebot.Bot.delete;
import static net.foxgenesis.util.helper.DiscordHelper.INFO;
import static net.foxgenesis.util.helper.DiscordHelper.outputMessageObject;

import java.util.regex.Pattern;

import net.foxgenesis.discord.filters.Filter;
import net.foxgenesis.discord.filters.ITextFilter;
import sx.blah.discord.handle.obj.IMessage;

@Filter(name = "AppleFilter")
public class AppleFilter implements ITextFilter {
	private static final Pattern p = Pattern.compile(".* (ipad|ios|iphone|itouch|macintosh|iwatch)[s ].*",Pattern.CASE_INSENSITIVE); //$NON-NLS-1$

	@Override
	public void handle(IMessage message) {
		delete(message.getChannel().sendMessage(outputMessageObject(message.getAuthor(),Messages.getString("AppleFilter.0"), //$NON-NLS-1$
				INFO).withImage(Messages.getString("AppleFilter.1")).build())); //$NON-NLS-1$
	}

	@Override
	public boolean isAllowed(IMessage message) {
		return p.matcher(message.getFormattedContent()).find();
	}
}
