package net.foxgenesis.ashlynebot.config;

import java.util.function.Function;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.Marker;
import org.slf4j.MarkerFactory;

import net.foxgenesis.ashlynebot.Bot;
import net.foxgenesis.discord.guild.IGuildData;
import net.foxgenesis.discord.guild.JSONObjectAdv;
import sx.blah.discord.handle.obj.IGuild;

public abstract class ConfigField<E> {
	private static final Logger LOGGER = LoggerFactory.getLogger(ConfigField.class);
	private static final Marker MARK = MarkerFactory.getMarker("ConfigField"); //$NON-NLS-1$
	
	public final String name;
	protected final boolean editable;
	protected final Function<IGuild,E> def;
	
	public ConfigField(String name, Function<IGuild,E> def, boolean editable) {
		this.name = name;
		this.editable = editable;
		this.def = def;
	}
	
	abstract E optFrom(JSONObjectAdv config,IGuild guild);
	abstract E from(JSONObjectAdv config);
	abstract void set(JSONObjectAdv config, E newState);
	protected boolean isPresent(JSONObjectAdv config) {return config.has(this.name);}
	private void remove(JSONObjectAdv config) {
		config.remove(this.name);
	}
	
	public E from(IGuild g) {return from(data(g));}
	public E optFrom(IGuild g) {return optFrom(data(g),g);}
	public final boolean isPresent(IGuild g) {return isPresent(data(g));}
	public void set(IGuild g, E newState) {set(data(g), newState);}
	public void remove(IGuild g) {remove(data(g));}
	
	protected static final JSONObjectAdv data(IGuild g) {
		LOGGER.debug(MARK,"Getting config for %s [%s]", g.getName(), g.getLongID()); //$NON-NLS-1$
		IGuildData a = Bot.getData().getDataForGuild(g);
		if(a == null) {
			LOGGER.error(MARK,"No IGuildData for %s [%s]", g.getName(), g.getLongID()); //$NON-NLS-1$
			return null;
		}
		JSONObjectAdv config = a.getConfig();
		if(config == null) {
			LOGGER.error(MARK,"No JSONObjectAdv for %s [%s]", g.getName(), g.getLongID()); //$NON-NLS-1$
			return null;
		}
		return config;
	}
	
	public final String getName() {return this.name;}
	public final boolean isEditable(){return this.editable;}
}
