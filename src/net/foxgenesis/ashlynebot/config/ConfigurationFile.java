/**
    Copyright (C) 2015  FoxGenesis

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */
package net.foxgenesis.ashlynebot.config;

import static sx.blah.discord.Discord4J.LOGGER;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.HashMap;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import org.slf4j.Marker;
import org.slf4j.MarkerFactory;

import net.foxgenesis.util.helper.FileHelper;

/**
 * Basic configuration file setup[ {@code key: value}]. Lines that start with
 * <i>#</i> will not be parsed.
 * 
 * @author Seth

 */
@SuppressWarnings("nls")
public class ConfigurationFile {
	private final File f;
	private final ConcurrentHashMap<String, Object> data = new ConcurrentHashMap<>();

	/**
	 * Read a configuration from an already existing file
	 * 
	 * @param file
	 *            - {@link File} to use
	 * @throws IOException 
	 */
	public ConfigurationFile(File f) {
		this.f = f;
		final Marker m = MarkerFactory.getMarker("Configuration Read");
		if(f.exists()) {
			try {
				FileHelper.loadFromFile(f, c -> {
					LOGGER.trace(m, "Attempting to read configuration file");
					try {
						Object j = c.readObject();
						if(j instanceof ConcurrentHashMap) {
							LOGGER.trace(m, "Object found. Attempting cast");
							@SuppressWarnings("unchecked")
							ConcurrentHashMap<String,Object> d = (ConcurrentHashMap<String, Object>)j;
							LOGGER.trace(m, "Adding config data");
							this.data.putAll(d);
							LOGGER.trace(m,"Configuration successfully read");
						} else LOGGER.error(m, "Configuration object is invalid [%s]",j.getClass());
							
					} catch(Exception e) {
						LOGGER.error(m, "Failed to read configuration. ", e);
					}
				});
			} catch (IOException e) {
				LOGGER.error(m,"Failed to read configuration file. Attempting old save read..");
				try {
					Files.readAllLines(this.f.toPath()).stream().filter(line -> !line.trim().startsWith("#") && !line.isEmpty())
					.forEach(this::add);
				} catch(Exception e2) {
					LOGGER.error(m, "Failed to read configuration file after attempting old save read", e2);
				}
			}
		} else
			try {
				f.createNewFile();
			} catch (IOException e) {
				e.printStackTrace();
			}
	}

	/**
	 * Save all changes
	 */
	public void save() {
		Marker m = MarkerFactory.getMarker("Configuration Save");
		try {
			LOGGER.trace(m, "Saving configuration");
			FileHelper.saveToFile(this.f, c -> {
				synchronized(this.data) {
					try {
						LOGGER.trace(m,"Writting object");
						c.writeObject(this.data);
						LOGGER.trace(m,"Object written");
					} catch (IOException e) {
						LOGGER.error(m,"Error while trying to write object", e);
					}
				}
			});
		} catch (IOException e) {
			LOGGER.error(m,"Error while trying to open object stream", e);
		}
		//		try(BufferedWriter w = new BufferedWriter(new FileWriter(f))) {
		//			data.forEach((a,b) -> {
		//				try {
		//					w.write(a + ":" + b + System.lineSeparator());
		//				} catch(IOException e) {
		//					e.printStackTrace();
		//				}
		//			});
		//		}
	}

	@Deprecated
	private void add(String line) {
		line = line.trim();
		String[] d = line.split(":", 2);
		if (d.length == 2) 
			this.data.put(d[0].trim(), d[1].trim());
		else
			System.err.println("Invalid line in configuration: " + line);
	}

	/**
	 * Get an {@link Integer} from a given key
	 * 
	 * @param key
	 *            - key to use
	 * @return Wanted data as an {@link Integer}
	 * @throws NullPointerException
	 *             if data was not found
	 */
	public int getInt(String key) {
		return (Integer) get(key);
	}

	/**
	 * Get a {@link Double} from a given key
	 * 
	 * @param key
	 *            - key to use
	 * @return Wanted data as a {@link Double}
	 * @throws NullPointerException
	 *             if data was not found
	 */
	public double getDouble(String key) {
		return (Double) get(key);
	}

	/**
	 * Get a {@link Long} from a given key
	 * 
	 * @param key
	 *            - key to use
	 * @return Wanted data as a {@link Long}
	 * @throws NullPointerException
	 *             if data was not found
	 */
	public long getLong(String key) {
		return (Long) get(key);
	}

	/**
	 * Get a {@link Float} from a given key
	 * 
	 * @param key
	 *            - key to use
	 * @return Wanted data as a {@link Float}
	 * @throws NullPointerException
	 *             if data was not found
	 */
	public float getFloat(String key) {
		return (Float) get(key);
	}
	
	/**
	 * Get a {@link Character} from a given key
	 * 
	 * @param key
	 *            - key to use
	 * @return Wanted data as a {@link Character}
	 * @throws NullPointerException
	 *             if data was not found
	 */
	public char getChar(String key) {
		return (Character) get(key);
	}
	
	/**
	 * Get a {@link Byte} from a given key
	 * 
	 * @param key
	 *            - key to use
	 * @return Wanted data as a {@link Byte}
	 * @throws NullPointerException
	 *             if data was not found
	 */
	public byte getByte(String key) {
		return (Byte) get(key);
	}
	
	/**
	 * Get a {@link Short} from a given key
	 * 
	 * @param key
	 *            - key to use
	 * @return Wanted data as a {@link Short}
	 * @throws NullPointerException
	 *             if data was not found
	 */
	public short getShort(String key) {
		return (Short) get(key);
	}

	/**
	 * Get a {@link Boolean} from a given key
	 * 
	 * @param key
	 *            - key to use
	 * @return Wanted data as a {@link Boolean}
	 * @throws NullPointerException
	 *             if data was not found
	 */
	public boolean getBoolean(String key) {
		return (Boolean) get(key);
	}

	/**
	 * Get an {@link Object} from a given key
	 * 
	 * @param key
	 *            - key to use
	 * @return Wanted data as a {@link Object}
	 * @throws NullPointerException
	 *             if data was not found
	 */
	public Object getObject(String key) {
		return get(key);
	}
	
	/**
	 * Get a {@link String} from a given key
	 * 
	 * @param key
	 *            - key to use
	 * @return Wanted data as a {@link String}
	 * @throws NullPointerException
	 *             if data was not found
	 */
	public String getString(String key) {
		return (String) get(key);
	}
	
	private Object get(String key) {
		if (!containsKey(key))
			throw new NullPointerException("No value with key: " + key);
		return this.data.get(key);
	}

	/**
	 * Checks whether the configuration file has a key
	 * 
	 * @param key
	 *            - key to check
	 * @return configuration file has key
	 */
	public boolean containsKey(String key) {
		return this.data.containsKey(key);
	}

	/**
	 * {@link ConcurrentHashMap#keySet()}
	 * @return
	 */
	public Set<String> keySet() {
		return this.data.keySet();
	}

	/**
	 * Set the default value of a key
	 * @param key - the key
	 * @param value - default value
	 */
	public void defaultValue(String key, Object value) {
		this.data.putIfAbsent(key, value);
	}

	/**
	 * Set multiple default values
	 * @param data - Map of default values
	 */
	public void defaultValues(HashMap<String,Object> data) {
		data.forEach(this::defaultValue);
	}
	
	/**
	 * Set the value of a key
	 * @param key - the key
	 * @param value - the value to set
	 */
	public void setValue(String key, Object value) {
		this.data.put(key,value);
	}
	
	public boolean checkType(String key, Class<?> t) {
		if(containsKey(key)) 
			return get(key).getClass().equals(t);
		return false;
	}

	/**
	 * Remove a value at a given key
	 * @param key - key to remove
	 */
	public void remove(String key) {
		this.data.remove(key);
	}
}
