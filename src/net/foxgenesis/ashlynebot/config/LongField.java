package net.foxgenesis.ashlynebot.config;

import java.util.function.Function;

import net.foxgenesis.discord.guild.JSONObjectAdv;
import sx.blah.discord.handle.obj.IGuild;

public class LongField extends ConfigField<Long> {
	
	public LongField(String name, boolean editable) {
		this(name,0,editable);
	}
	
	public LongField(String name, long def, boolean editable) {
		this(name, g -> def, editable);
	}
	
	public LongField(String name, Function<IGuild,Long> def, boolean editable) {
		super(name,def,editable);
	}

	@Override
	Long optFrom(JSONObjectAdv config,IGuild guild) {
		return config.optLong(this.name, this.def.apply(guild));
	}

	@Override
	Long from(JSONObjectAdv config) {
		return config.getLong(this.name);
	}

	@Override
	void set(JSONObjectAdv config, Long newState) {
		config.put(this.name, (long)newState);
	}
}
