package net.foxgenesis.ashlynebot.config;

import java.util.function.Function;

import net.foxgenesis.discord.guild.JSONObjectAdv;
import sx.blah.discord.handle.obj.IGuild;

public class BooleanField extends ConfigField<Boolean> {
	
	public BooleanField(String name, boolean editable) {
		this(name,g -> false,editable);
	}
	
	public BooleanField(String name, boolean def, boolean editable) {
		this(name, g -> def, editable);
	}
	
	public BooleanField(String name, Function<IGuild,Boolean> def, boolean editable) {
		super(name,def,editable);
	}

	@Override
	Boolean optFrom(JSONObjectAdv config, IGuild guild) {
		return isPresent(config) ? config.optBoolean(this.name) : this.def.apply(guild);
	}

	@Override
	Boolean from(JSONObjectAdv config) {
		return config.getBoolean(this.name);
	}

	@Override
	void set(JSONObjectAdv config, Boolean newState) {
		config.put(this.name, newState);
	}
	
	public boolean toggle(IGuild guild) {return toggle(ConfigField.data(guild),guild);}
	private boolean toggle(JSONObjectAdv config, IGuild guild) {
		boolean l = !optFrom(config,guild);
		set(config,l);
		return l;
	}
}
