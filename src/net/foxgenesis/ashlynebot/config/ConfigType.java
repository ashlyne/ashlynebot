package net.foxgenesis.ashlynebot.config;

public enum ConfigType {
	BOOLEAN,
	INTEGER,
	DOUBLE,
	FLOAT,
	BLOB,
	STRING
}
