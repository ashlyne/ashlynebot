package net.foxgenesis.ashlynebot.input;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import org.json.JSONObject;

import net.foxgenesis.ashlynebot.Bot;
import net.foxgenesis.discord.guild.JSONObjectAdv;
import net.foxgenesis.util.helper.JSONHelper;
import sx.blah.discord.handle.obj.IChannel;
import sx.blah.discord.handle.obj.IGuild;

@SuppressWarnings("nls")
public class InputManager extends Thread {
	private static final HashMap<String,ConsoleCommand> commands = new HashMap<>();

	static {
		registerConsoleCommand(ConsoleCommand.newCommand("help", (inputs,split) -> {
			StringBuilder b = new StringBuilder();
			b.append("Help:\n=====================================\n");
			commands.entrySet().stream().sorted((a,bc) -> a.getKey().compareTo(bc.getKey())).filter(
					n -> !n.getKey().equals("help")).forEachOrdered(k -> b.append(String.format("%s - %s\n", 
							k.getKey(), k.getValue().usage)));
			b.append("=====================================");
			System.out.println(b);
			return true;
		}));
		
		registerConsoleCommand(ConsoleCommand.newCommand("sysch", (inputs,split) -> {
			StringBuilder b = new StringBuilder();
			b.append("\n=====================================\n");
			Bot.getClient().getGuilds().forEach(guild -> b.append(String.format("%s[%s]\t\tD: %s\t\tS: %s\t\tO: %s#%s\n", 
					guild.getName(), guild.getStringID(), guild.getDefaultChannel().getName(), 
					guild.getSystemChannel() != null? guild.getSystemChannel().getName() : "N/A", guild.getOwner().getName(), guild.getOwner().getDiscriminator())));
			b.append("=====================================");
			System.out.println(b);
			return true;
		}));
		
		registerConsoleCommand(ConsoleCommand.newCommand("say", (inputs,split) -> {
			if(split != null && split.length == 2) {
				IGuild guild = Bot.getClient().getGuildByID(Long.parseLong(split[0]));
				if(guild == null)
					System.err.println("[Say]: Invalid Guild!");
				else {
					IChannel sysch = Bot.getData().getDataForGuild(guild).getDefaultChannel();
					if(sysch != null)
						sysch.sendMessage(split[1]);
					else System.err.println("Guild does not have a system channel!");
				}
				return true;
			}
			return false;
		}));
		
		registerConsoleCommand(ConsoleCommand.newCommand("defchan", (inputs,split) -> {
			List<IGuild> guilds = Bot.getClient().getGuilds().stream().filter(g -> g.getName().contains(inputs)).collect(Collectors.toList());
			if(guilds.size() == 1) {
				
			} else if(guilds.size() > 1) {
				System.err.println("More than one found!");
				return true;
			}
			return false;
		}));
		
		
		registerConsoleCommand(ConsoleCommand.newCommand("getcfg", (inputs,split) -> {
			if(split != null && split.length == 1) {
				IGuild guild = Bot.getClient().getGuildByID(Long.parseLong(split[0]));
				
				if(guild == null) {
					System.err.println("Invalid Guild ID");
					return true;
				}
				listOut(Bot.getData().getDataForGuild(guild).getConfig());
				return true;
			}
			return false;
		}));
		
		registerConsoleCommand(ConsoleCommand.newCommand("setcfg", (inputs,split) -> {
			if(split != null && split.length == 3) {
				IGuild guild = Bot.getClient().getGuildByID(Long.parseLong(split[0]));
				String key = split[1].toLowerCase();
				String val = split[2];
				boolean isNull = val.equalsIgnoreCase("null");
				
				JSONObjectAdv config = Bot.getData().getDataForGuild(guild).getConfig();
				
				if(isNull) {
					if(config.has(key)) {
						config.remove(key);
						System.out.println("Entry removed");
					}
					else
						System.err.println("No entry found with given key");
				} else {
					config.put(key, val);
					System.out.println("Entry added");
				}
				
				return true;
			}
			return false;
		}));
	}

	public static void registerConsoleCommand(ConsoleCommand c) {
		if(commands.containsKey(c.name))
			throw new UnsupportedOperationException("Command already exists!");
		commands.put(c.name, c);
	}
	
	
	/**
	 * List out a {@link JSONObject} in console
	 * @param obj - JSONObject to use
	 */
	private static void listOut(JSONObject obj) {
		Map<String,Object> map = JSONHelper.jsonToMap(obj);
		
		StringBuilder build = new StringBuilder();
		
		map.entrySet().stream().sorted((a,b) -> a.getKey().compareTo(b.getKey())).forEachOrdered(entry -> {
			build.append(String.format("%s\t\t\t%s\n", entry.getKey(), entry.getValue()));
		});
		
		System.out.println(String.format("===============================\n%s===========================", build.toString()));
	}

	//============================================================================================
	private boolean run = true;
	private static final Pattern pattern = Pattern.compile("([^\"]\\S*|\".+?\")\\s*");
	@Override
	public void run() {
		try(BufferedReader reader = new BufferedReader(new InputStreamReader(System.in))) {
			while(this.run) {
				while(!reader.ready());
				String line = null;
				while((line = reader.readLine()) != null) {
					String[] split = line.split(" ", 2);

					if(split.length > 0) {
						String command = split[0].trim();

						if(commands.containsKey(command)) {
							ConsoleCommand c = commands.get(command);
							
							List<String> list = new ArrayList<>();
							if(split.length > 1) {
								Matcher m = pattern.matcher(split[1].trim());
								while(m.find())
									list.add(m.group(1).replace("\"",""));
							}
							
							System.out.println("Executing Command: " + line);
							if(!c.accept(split.length > 1? split[1].trim() : null, 
									split.length > 1? list.toArray(new String[]{}) : null))
								System.err.println("Invalid Usage: " + c.usage);

						} else System.err.println("Unknown Command: " + line);
					}
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void cancel(){this.run = false;}
}
