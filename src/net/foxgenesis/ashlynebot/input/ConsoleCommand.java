package net.foxgenesis.ashlynebot.input;

import java.util.function.BiFunction;

@SuppressWarnings("nls")
public class ConsoleCommand {
	public final String name, usage;
	private BiFunction<String,String[],Boolean> command;

	private ConsoleCommand(String name) {
		this(name,null);
	}

	private ConsoleCommand(String name, String usage) {
		this.name = name;
		this.usage = usage;
	}

	public boolean accept(String inputs, String[] split) {
		if(this.command != null)
			return this.command.apply(inputs, split);
		throw new NullPointerException("Command does not contain executor");
	}

	public static ConsoleCommand newCommand(String name, BiFunction<String,String[],Boolean> command){
		return newCommand(name,command,"No help provided");
	}

	public static ConsoleCommand newCommand(String name, BiFunction<String,String[],Boolean> command, String usage) {
		ConsoleCommand c = new ConsoleCommand(name,usage);
		c.command = command;
		return c;
	}
}
