package net.foxgenesis.util;

import java.util.function.Supplier;

public class Waiter {
	private Thread t;

	public Waiter(Supplier<Boolean> isReady) {
		this.t = new Thread(() -> {while(!isReady.get());}, "Waiter Thread"); //$NON-NLS-1$
	}

	public void startWaiting() {
		this.t.start();
		try {
			this.t.join();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
}
