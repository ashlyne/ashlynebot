package net.foxgenesis.util;

@SuppressWarnings("nls")
public final class MethodTimer {
	public static double run(Runnable r) {
		long n = System.nanoTime();
		r.run();
		return (System.nanoTime() - n) / 1_000_000D;
	}
	
	public static double run(Runnable r, int times) {
		Averager s = new Averager();
		for(int i=0; i< times; i++) {
			System.out.println("===== Run #" + (i + 1) + "=====");
			s.add(run(r));
		}
		return s.getAverage();
	}
	
	public static String runFormatMS(Runnable r) {
		return runFormatMS(r,2);
	}
	
	public static String runFormatMS(Runnable r, int places) {
		return String.format("%." + places + "f ms", run(r));
	}
	
	public static String runFormatMS(Runnable r, int times, int places) {
		return String.format("%." + places + "f ms", run(r,times));
	}
	
	public static final class Averager {
		private long count = 0;
		private double sum = 0;
		
		public void add(double n) {
			this.sum+=n;
			this.count++;
		}
		public double getAverage() {
			return this.sum / this.count;
		}
	}
}
