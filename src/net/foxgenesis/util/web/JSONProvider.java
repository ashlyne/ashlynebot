package net.foxgenesis.util.web;

import javax.servlet.http.HttpServletRequest;

import org.json.JSONObject;

@FunctionalInterface
public interface JSONProvider {
	public JSONObject provide(HttpServletRequest request);
}
