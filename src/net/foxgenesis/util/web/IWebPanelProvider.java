package net.foxgenesis.util.web;

import javax.servlet.http.HttpServletRequest;

import org.json.JSONObject;

@FunctionalInterface
public interface IWebPanelProvider {
	public JSONObject getJSONForRequest(String target, HttpServletRequest request);
}
