package net.foxgenesis.util.web;

import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;

import org.json.JSONObject;

public class WebPanel implements IWebPanelData, IWebPanelProvider {
	private final HashMap<String, JSONProvider> data = new HashMap<>();
	private boolean allowed;
	
	public void setAllowed(boolean state) {
		this.allowed = state;
	}
	
	public boolean isAllowed() {
		return this.allowed;
	}

	@Override
	public JSONObject getJSONForRequest(String target, HttpServletRequest request) {
		return this.data.containsKey(target) ? this.data.get(target).provide(request) : null;
	}

	@Override
	public boolean addJSONAPI(String context, JSONProvider p) {
		if(this.data.containsKey(context))
			return false;
		System.out.println("[Web Panel]: Adding API for context [" + context + "]"); //$NON-NLS-1$ //$NON-NLS-2$
		this.data.put(context, p);
		return true;
	}

}
