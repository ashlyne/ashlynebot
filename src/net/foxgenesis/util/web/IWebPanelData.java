package net.foxgenesis.util.web;

@FunctionalInterface
public interface IWebPanelData {
	public boolean addJSONAPI(String context, JSONProvider p);
}
