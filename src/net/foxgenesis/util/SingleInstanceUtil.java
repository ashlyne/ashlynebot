package net.foxgenesis.util;

import java.io.IOException;
import java.net.ServerSocket;

public final class SingleInstanceUtil {
	private static ServerSocket socket;

	public static void waitAndGetLock(int amt) {
		int tries = 0;
		do {
			tries++;
			try {
				socket = new ServerSocket(8007);
				if(socket != null && socket.isBound())
					break;
			} catch (IOException e) {
				if(tries >= amt)
					throw new RuntimeException(String.format("Unable to bind to socket after %s tries.",amt)); //$NON-NLS-1$
				System.err.println("Unable to bind to socket. Trying again in 5 seconds."); //$NON-NLS-1$
				try {Thread.sleep(10000);} catch (InterruptedException e2) {}
			}
		} while(true);

		Runtime.getRuntime().addShutdownHook(new Thread(() -> {
			if(socket != null && socket.isBound()) {
				try {
					socket.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}));
	}
}
