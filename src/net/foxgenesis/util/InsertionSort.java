package net.foxgenesis.util;

import java.util.ArrayList;

public class InsertionSort<E extends Comparable<E>> extends ArrayList<E> {    
	private static final long serialVersionUID = 7976654194135120281L;

	@Override
	public synchronized boolean add(E e) {
		boolean r = super.add(e);
		if(r)
			sortArray();
		return r;
	}
	
	@Override
	public synchronized E remove(int i) {
		return super.remove(i);
	}
	
	@Override
	public synchronized boolean remove(Object e) {
		return super.remove(e);
	}
	
    private void sortArray(){                   
        for(int i=1;i<this.size();i++){
             
            E key =this.get(i);
             
            for(int j= i-1;j>=0;j--){
                if(key.compareTo(this.get(j)) > 0) {
                    // Shifting Each Element to its right as key is less than the existing element at current index
                    this.set(j+1,this.get(j));
                     
                    // Special case scenario when all elements are less than key, so placing key value at 0th Position
                    if(j==0){
                        this.set(0, key);
                    }
                }else{
                    // Putting Key value after element at current index as Key value is no more less than the existing element at current index
                    this.set(j+1,key);
                    break; // You need to break the loop to save un necessary iteration
                }
            }
        }       
    }
}