package net.foxgenesis.util;

import java.io.Serializable;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Random;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class TimeFilterSelector<T> implements Serializable {
	private static final long serialVersionUID = 4819016468741678721L;
	private static final Random rand = new Random();
	private HashMap<T,Long> data = new HashMap<>();
	private long maxTime;

	public TimeFilterSelector(long l){this.maxTime = l;}
	public TimeFilterSelector(long maxTime, Collection<T> e) {
		this(maxTime);
		addData(e);
	}

	public void addData(Collection<T> e) {
		e.stream().forEach(this::addData);
	}

	public void addData(T e) {
		this.data.put(e, System.currentTimeMillis());
	}

	public void clear() {
		this.data.clear();
	}

	public int size() {
		return this.data.size();
	}

	public long getMaxTime() {
		return this.maxTime;
	}

	public void setMaxTime(int maxTime) {
		this.maxTime = maxTime;
	}

	private List<T> getItems(Collection<T> w) {
		return getItems(w.stream()).collect(Collectors.toList());
	}
	
	private Stream<T> getItems(Stream<T> w) {
		long l = System.currentTimeMillis();
		return w.filter(entry -> {
			if(this.data.containsKey(entry))
				if(Math.abs(l - this.data.get(entry)) > this.maxTime)
					this.data.remove(entry);
				else return false;
			return true;
		});
	}

	public T getRandomItem(Collection<T> items) {
		List<T> l = getItems(items);
		T r = random(l);
		return r == null? random(items) : r;
	}
	
	public T getRandomItem(Stream<T> items) {
		T r = random(getItems(items));
		return r == null? random(items) : r;
	}
	
	@SuppressWarnings("unchecked")
	private T random(Collection<T> l) {
		if(l.size() > 0) {
			T e = (T) l.toArray()[rand.nextInt(l.size())];
			this.data.put(e, System.currentTimeMillis());
			return e;
		}
		return null;
	}
	
	private T random(Stream<T> l) {
		T e = l.findAny().orElse(null);
		if(e != null)
			this.data.put(e, System.currentTimeMillis());
		return e;
	}
	
	public Set<T> keySet() {
		return this.data.keySet();
	}
}
