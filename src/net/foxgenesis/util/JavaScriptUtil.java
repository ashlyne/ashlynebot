package net.foxgenesis.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import javax.script.Bindings;
import javax.script.ScriptContext;
import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;

import net.foxgenesis.ashlynebot.commands.GraphCommandPolar;

@SuppressWarnings("nls")
public final class JavaScriptUtil {
	private static ScriptEngine js;
	private static String lib = "";

	static {
		try(BufferedReader r = new BufferedReader(new InputStreamReader(GraphCommandPolar.class.getResourceAsStream("graph.js")))) {
			String line;
			while((line = r.readLine()) != null)
				lib+=line;

		} catch (IOException e) {
			e.printStackTrace();
		}

		js = new ScriptEngineManager().getEngineByName("javascript");
		Bindings bindings = js.getBindings(ScriptContext.ENGINE_SCOPE);
		bindings.remove("print");
		bindings.remove("load");
		bindings.remove("loadWithNewGlobal");
		bindings.remove("exit");
		bindings.remove("quit");
		addFunctions(js);
	}

	public static ScriptEngine getJSEngine() {
		return js;
	}

	public static ScriptEngine getSeperateJSEngine() {
		ScriptEngine js = new ScriptEngineManager().getEngineByName("javascript");
		Bindings bindings = js.getBindings(ScriptContext.ENGINE_SCOPE);
		bindings.remove("print");
		bindings.remove("load");
		bindings.remove("loadWithNewGlobal");
		bindings.remove("exit");
		bindings.remove("quit");
		addFunctions(js);
		return js;
	}

	private static void addFunctions(ScriptEngine js) {
		try {
			js.eval(new InputStreamReader(GraphCommandPolar.class.getResourceAsStream("imp.js")));
			Object j = js.eval(new InputStreamReader(GraphCommandPolar.class.getResourceAsStream("alb.min.js")));
			ScriptContext c = js.getContext();
			c.setAttribute("algebra", j, ScriptContext.GLOBAL_SCOPE);
			for(String al : new String[]{"sin","cos","tan","abs","pow","sqrt","acos","asin","atan","ceil","exp","floor","log","round"}) 
				c.setAttribute(al, js.eval(String.format("function %s(x) {return Math." + al + "(x);}",al)), ScriptContext.GLOBAL_SCOPE);
			c.setAttribute("pi", Math.PI, ScriptContext.GLOBAL_SCOPE);
			c.setAttribute("e", Math.E, ScriptContext.GLOBAL_SCOPE);
			js.setContext(c);
			js.eval(lib);
			//inv = (Invocable)js;
		} catch (ScriptException e) {
			e.printStackTrace();
		}
	}
}
