package net.foxgenesis.util.data;

import java.util.ArrayList;
import java.util.Collections;

public class LongList extends ArrayList<Long> {
	private static final long serialVersionUID = -8988657481755317025L;
	
	@Override
	public boolean add(Long l) {
		if(contains(l))
			return false;
		boolean a = super.add(l);
		if(!a)
			return false;
		Collections.sort(this);
		return true;
	}
	
	@Override
	public boolean remove(Object j) {
		if(!(j instanceof Long))
			return false;
		long l = (long)j;
		int index = Collections.binarySearch(this, l);
		if(index < 0)
			return false;
		super.remove(index);
		return true;
	}

	@Override
	public boolean contains(Object j) {
		if(!(j instanceof Long))
			return false;
		long l = (long)j;
		return Collections.binarySearch(this, l) >= 0;
	}
}
