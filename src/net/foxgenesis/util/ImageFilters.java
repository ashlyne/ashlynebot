package net.foxgenesis.util;

import java.awt.image.BufferedImage;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import com.jhlabs.image.*;

public final class ImageFilters {
	private static Map<String,AbstractBufferedImageOp> filters = new HashMap<>();
	private static final Class<?>[] ff = {
			ApplyMaskFilter.class,
			ArrayColormap.class,
			AverageFilter.class,
			BicubicScaleFilter.class,
			BlockFilter.class,
			BlurFilter.class,
			BorderFilter.class,
			BoxBlurFilter.class,
			BrushedMetalFilter.class,
			BumpFilter.class,
			CausticsFilter.class,
			CellularFilter.class,
			ChannelMixFilter.class,
			CheckFilter.class,
			ChromeFilter.class,
			CircleFilter.class,
			CompositeFilter.class,
			ContourFilter.class,
			ContrastFilter.class,
			ConvolveFilter.class,
			CropFilter.class,
			CrystallizeFilter.class,
			CurlFilter.class,
			DespeckleFilter.class,
			DiffuseFilter.class,
			DiffusionFilter.class,
			DilateFilter.class,
			DisplaceFilter.class,
			DissolveFilter.class,
			DitherFilter.class,
			EdgeFilter.class,
			EmbossFilter.class,
			EqualizeFilter.class,
			ErodeFilter.class,
			ExposureFilter.class,
			FadeFilter.class,
			FBMFilter.class,
			FeedbackFilter.class,
			FieldWarpFilter.class,
			FlareFilter.class,
			FlipFilter.class,
			FourColorFilter.class,
			GainFilter.class,
			GammaFilter.class,
			GaussianFilter.class,
			GlintFilter.class,
			GlowFilter.class,
			Gradient.class,
			GradientFilter.class,
			GradientWipeFilter.class,
			GrayFilter.class,
			GrayscaleColormap.class,
			GrayscaleFilter.class,
			HalftoneFilter.class,
			Histogram.class,
			HSBAdjustFilter.class,
			ImageCombiningFilter.class,
			InterpolateFilter.class,
			InvertAlphaFilter.class,
			InvertFilter.class,
			KaleidoscopeFilter.class,
			KeyFilter.class,
			LensBlurFilter.class,
			LevelsFilter.class,
			LifeFilter.class,
			LightFilter.class,
			LinearColormap.class,
			LookupFilter.class,
			MapFilter.class,
			MarbleFilter.class,
			MarbleTexFilter.class,
			MaskFilter.class,
			MaximumFilter.class,
			MedianFilter.class,
			MinimumFilter.class,
			MirrorFilter.class,
			MotionBlurFilter.class,
			MotionBlurOp.class,
			NoiseFilter.class,
			OctTreeQuantizer.class,
			OffsetFilter.class,
			OilFilter.class,
			OpacityFilter.class,
			OutlineFilter.class,
			PerspectiveFilter.class,
			PinchFilter.class,
			PixelUtils.class,
			PlasmaFilter.class,
			PointillizeFilter.class,
			PolarFilter.class,
			PosterizeFilter.class,
			QuantizeFilter.class,
			QuiltFilter.class,
			RaysFilter.class,
			ReduceNoiseFilter.class,
			RescaleFilter.class,
			RGBAdjustFilter.class,
			RippleFilter.class,
			RotateFilter.class,
			ScaleFilter.class,
			ShadeFilter.class,
			ShadowFilter.class,
			ShapeFilter.class,
			SharpenFilter.class,
			ShatterFilter.class,
			ShearFilter.class,
			SkeletonFilter.class,
			//SkyFilter.class,
			SmartBlurFilter.class,
			SmearFilter.class,
			SolarizeFilter.class,
			SparkleFilter.class,
			Spectrum.class,
			SpectrumColormap.class,
			SphereFilter.class,
			SplineColormap.class,
			StampFilter.class,
			SwimFilter.class,
			TextureFilter.class,
			ThresholdFilter.class,
			TileImageFilter.class,
			TwirlFilter.class,
			UnsharpFilter.class,
			VariableBlurFilter.class,
			WarpFilter.class,
			WarpGrid.class,
			WaterFilter.class,
			WeaveFilter.class,
			WoodFilter.class
	};

	static {
		Arrays.stream(ff).forEach(c -> {
			if(AbstractBufferedImageOp.class.isAssignableFrom(c)) {
				Class<? extends AbstractBufferedImageOp> cc = c.asSubclass(AbstractBufferedImageOp.class);
				try {
					filters.put(c.getSimpleName().toLowerCase().replaceAll("filter", ""), cc.newInstance()); //$NON-NLS-1$ //$NON-NLS-2$
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	public static Set<Entry<String,AbstractBufferedImageOp>> getFilters() {
		return filters.entrySet();
	}
	
	public static boolean hasFilter(String key) {
		return filters.containsKey(key);
	}
	
	public static BufferedImage filter(String key, BufferedImage image) {
		AbstractBufferedImageOp filter = filters.get(key);
		return filter == null? null : filter.filter(image, 
				filter.createCompatibleDestImage(image, image.getColorModel()));
	}
}
