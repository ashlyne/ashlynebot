package net.foxgenesis.util.helper;
import java.io.BufferedReader;

import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.HttpClients;

@SuppressWarnings("nls")
public final class WebHelper {
	private static final HttpClient httpclient = HttpClients.createDefault();
	
	public static HttpResponse sendPost(String url, List<NameValuePair> params) throws UnsupportedOperationException, IOException {
		if(params == null)
			params = new ArrayList<>(0);
		HttpPost httppost = new HttpPost(url);

		httppost.setEntity(new UrlEncodedFormEntity(params, "UTF-8"));

		System.out.println("{" + Thread.currentThread().getName()+ "}[WebHelper]: Sending POST to " + url + '\n');
		return httpclient.execute(httppost);
	}
	
	public static HttpResponse sendGet(String url) throws ClientProtocolException, IOException {
		HttpGet httpget = new HttpGet(url);
		httpget.setHeader("User-agent","AshlyneBot");
		System.out.printf("{%s}[WebHelper]: Sending GET to " + url + '\n',Thread.currentThread().getName());
		return httpclient.execute(httpget);
	}
	
	public static String readInputStream(HttpResponse h) throws UnsupportedOperationException, IOException {
		HttpEntity entity = h.getEntity();
		if (entity != null) {
			String output = "";
			try(BufferedReader in = new BufferedReader(new InputStreamReader(entity.getContent()))) {
				String line = "";
				while((line = in.readLine()) != null)
					output+=line;
			}
			return output;
		}
		return null;
	}
}
