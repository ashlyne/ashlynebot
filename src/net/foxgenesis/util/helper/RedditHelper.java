package net.foxgenesis.util.helper;

import java.io.IOException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Stream;

import org.json.JSONArray;
import org.json.JSONObject;

import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;

import sx.blah.discord.util.EmbedBuilder;

@SuppressWarnings("nls")
public class RedditHelper {
	public static final Predicate<RedditPost> PICTURES_ONLY = post -> post.url.matches(".*(png|jpg|jpeg|mp4|gif|gifv|webm)");
	public static final Predicate<RedditPost> NON_NSFW = post -> !post.nsfw;
	public static final Predicate<RedditPost> NON_STICKY = post -> !post.stickied;
	
	public final String subReddit;

	public RedditHelper(String subReddit) {
		this.subReddit = subReddit;
	}

	private JSONObject getData(int limit) {
		try {
			return Unirest.get("https://www.reddit.com/r/" + URLEncoder.encode(this.subReddit, "UTF-8") + ".json?limit=" + limit).asJson().getBody().getObject();
		} catch (IOException | UnirestException e) {
			e.printStackTrace();
			return null;
		}
	}

	public Stream<RedditPost> getPosts(int limit) {
		JSONObject o = getData(limit);
		if(o == null)
			throw new RedditMissingException();
		try {
			JSONArray a = o.getJSONObject("data").getJSONArray("children");
			List<RedditPost> l = new ArrayList<>(a.length());
			a.forEach(b -> l.add(new RedditPost(((JSONObject)b).getJSONObject("data"))));
			return l.parallelStream();
		} catch(Exception e) {
			e.printStackTrace();
			throw new RedditMissingException();
		}
	}
	
	public Stream<RedditPost> getPosts(int limit, Predicate<RedditPost> filter) {
		return getPosts(limit).filter(filter);
	}

	public static class RedditPost {
		public final JSONObject raw;
		public final String id,url;
		public final boolean stickied,nsfw,video;
		public final String extension;
		
		RedditPost(JSONObject raw) {
			this.raw = raw;
			this.id = raw.getString("id");
			this.stickied = raw.optBoolean("stickied");
			this.nsfw = raw.optBoolean("over18");
			this.video = raw.optBoolean("is_video");
			
			String tempurl = raw.getString("url");
			if(tempurl.endsWith(".gifv"))
				tempurl = tempurl.substring(0, tempurl.length()-1);
			this.url = tempurl;
			this.extension = this.url.substring(this.url.lastIndexOf('.')+1);
		}

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + ((this.id == null) ? 0 : this.id.hashCode());
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			RedditPost other = (RedditPost) obj;
			if (this.id == null) {
				if (other.id != null)
					return false;
			} else if (!this.id.equals(other.id))
				return false;
			return true;
		}

		@Override
		public String toString() {
			return String.format("RedditPost{stick=%s,id=%s,url=%s}",this.stickied,this.id,this.url);
		}
		
		public EmbedBuilder getEmbedObject() {
			return new EmbedBuilder().withImage(this.url).withUrl("http://www.reddit.com" + this.raw.getString("permalink"))
					.withTitle(this.raw.getString("title")).withFooterText(this.raw.getString("subreddit_name_prefixed")).withColor(0, 121, 211);
		}
	}
	
	
	public static class RedditMissingException extends RuntimeException {
		private static final long serialVersionUID = 2100927597913243140L;
		
	}
}
