package net.foxgenesis.util.helper;

import java.util.AbstractMap.SimpleEntry;
import java.util.Map.Entry;
import java.util.Random;
import java.util.stream.Stream;

public final class RandomHelper {
	public static <E> E getWeightedRandom(Stream<Entry<E, Double>> weights, Random random) {
	    return weights
	        .map(e -> new SimpleEntry<>(e.getKey(),-Math.log(random.nextDouble()) / e.getValue()))
	        .min((e0,e1)-> e0.getValue().compareTo(e1.getValue()))
	        .orElseThrow(IllegalArgumentException::new).getKey();
	}
}
