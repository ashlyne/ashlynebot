package net.foxgenesis.util.helper;

import static net.foxgenesis.ashlynebot.Bot.pushbullet;

import java.awt.Color;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.file.Files;
import java.rmi.server.UID;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.function.Consumer;
import java.util.stream.Collectors;

import javax.imageio.ImageIO;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.github.sheigutn.pushbullet.items.push.sendable.defaults.SendableNotePush;

import net.foxgenesis.ashlynebot.Bot;
import net.foxgenesis.discord.guild.GuildDataReadyEvent;
import net.foxgenesis.discord.timed.TimedEvent;
import net.foxgenesis.util.TimeFilterSelector;
import sx.blah.discord.api.events.EventSubscriber;
import sx.blah.discord.api.internal.json.objects.EmbedObject;
import sx.blah.discord.handle.obj.IChannel;
import sx.blah.discord.handle.obj.IEmoji;
import sx.blah.discord.handle.obj.IGuild;
import sx.blah.discord.handle.obj.IMessage;
import sx.blah.discord.handle.obj.IRole;
import sx.blah.discord.handle.obj.IUser;
import sx.blah.discord.handle.obj.IVoiceChannel;
import sx.blah.discord.handle.obj.Permissions;
import sx.blah.discord.util.EmbedBuilder;
import sx.blah.discord.util.RequestBuffer;
import sx.blah.discord.util.RequestBuffer.IVoidRequest;

public final class DiscordHelper {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(DiscordHelper.class);

	//--------------------------		MESSAGES		-------------------------------

	public static void splitToMessages(String input, int max, Consumer<String> s) {
		splitToMessages(input,max,"","",s); //$NON-NLS-1$ //$NON-NLS-2$
	}

	public static void splitToMessages(String input, Consumer<String> s) {
		splitToMessages(input,2000,"","",s); //$NON-NLS-1$ //$NON-NLS-2$
	}

	public static void splitToMessages(String input, int max, String prefix, String suffix, Consumer<String> s) {
		splitToMessages(input,max,prefix,suffix," ",s); //$NON-NLS-1$
	}

	public static void splitToMessages(String input, int max, String prefix, String suffix, String delim, Consumer<String> s) {
		if(input == null || input.isEmpty() || prefix == null || suffix == null)
			return;
		else if(input.length() <= max - (prefix.length() + suffix.length()))
			s.accept(prefix + input + suffix);
		else {
			int pos = input.substring(0,max - (prefix.length() + suffix.length())).lastIndexOf(delim);
			if(pos == -1) {
				LOGGER.error("Failed to split"); //$NON-NLS-1$
				pos = max - (prefix.length() + suffix.length());
			}
			s.accept(prefix + input.substring(0,pos) + suffix);
			splitToMessages(input.substring(pos),max,prefix,suffix,s);
		}
	}

	public static void splitToMessages(String input, int max, String prefix, String suffix, String delim, String fallBack, Consumer<String> s) {
		if(input == null || input.isEmpty() || prefix == null || suffix == null)
			return;
		else if(input.length() <= max - (prefix.length() + suffix.length()))
			s.accept(prefix + input + suffix);
		else {
			int pos = input.substring(0,max - (prefix.length() + suffix.length())).lastIndexOf(delim);
			if(pos == -1) {
				pos = input.substring(0,max - (prefix.length() + suffix.length())).lastIndexOf(fallBack);
				if(pos == -1) {
					LOGGER.error("Failed to split"); //$NON-NLS-1$
					pos = max - (prefix.length() + suffix.length());
				}
			}
			s.accept(prefix + input.substring(0,pos) + suffix);
			splitToMessages(input.substring(pos),max,prefix,suffix,s);
		}
	}

	//---------------------------------		MESSAGE EXTRAS		------------------------------------

	public static void addCustomEmojiReaction(IMessage message, String emoji) {
		IEmoji em = message.getGuild().getEmojiByName(emoji);
		if(em != null)
			message.addReaction(em);
	}

	public static EmbedObject embedPicture(String url) {
		System.out.println("Embeding Picture: " + url); //$NON-NLS-1$
		return new EmbedBuilder().withImage(url).build();
	}

	public static String captureInMarkdown(String in) {
		return captureInSyntax(in,"Markdown"); //$NON-NLS-1$
	}

	public static String captureInSyntax(String in, String syntax) {
		return "```" + syntax + System.lineSeparator() + in + System.lineSeparator() + "```"; //$NON-NLS-1$ //$NON-NLS-2$
	}

	public static void tryToDelete(IMessage msg) {
		try {
			msg.delete();
		} catch(Exception e){}
	}

	public static IEmoji getCustomEmoji(String name) {
		return Bot.getClient().getGuildByID(331689006888648705l).getEmojiByName(name);
	}

	private static final Color[] colors = {new Color(0x2196F3),new Color(0xf44336),new Color(0xFFEB3B)};
	public static final int INFO = 0, ERROR = 1, WARN = 2;

	public static EmbedBuilder outputMessageObj(String text, int type) {return new EmbedBuilder().withDesc(text).withColor(colors[type]);}
	public static EmbedBuilder outputMessageObj(String title, String text, int type) {return outputMessageObj(text,type).withTitle(title);}
	public static IMessage outputMessage(IMessage message, String text, int type) {return message.getChannel().sendMessage(outputMessage(text,type,message.getAuthor()));}
	public static void outputMessageDelete(IMessage message, String text, int type) {TimedMsgRemover.delete(outputMessage(message,text,type));}
	public static EmbedBuilder outputMessageObject(IUser user, String text, int type) {return outputMessageObj(user.mention(true) + ", " + text,type);} //$NON-NLS-1$
	public static EmbedObject outputMessage(String text, int type) { return outputMessageObj(text,type).build();}
	public static EmbedObject outputMessage(String text, int type, IUser user) {return outputMessageObject(user,text,type).build();}

	//-------------------------------		ROLES		--------------------------------------

	public static IRole getHighestRoleForUser(IUser user, IGuild guild) {
		//TODO: outdated. update this.
		if(guild == null)
			return null;
		List<IRole> r = user.getRolesForGuild(guild);
		if(r.isEmpty()) {
			ArrayList<IRole> roles = new ArrayList<>(guild.getRoles());
			if(roles.isEmpty()) {
				LOGGER.error("NO ROLES FOUND FOR ENTIRE GUILD!!!!!!"); //$NON-NLS-1$
				return null;
			}
			Collections.sort(roles, (a,b) -> Integer.compare(b.getPosition(), a.getPosition()));
			for(IRole role: roles) {
				if(role.getColor() == Color.BLACK)
					continue;
				List<IUser> users = guild.getUsersByRole(role);
				if(!users.contains(user))
					continue;
				return role;
			}
			LOGGER.error("USER HAS NO ROLES!!!!!!!!!!"); //$NON-NLS-1$
			return null;
		}
		return r.stream().filter(a -> !a.getColor().equals(Color.black))
				.reduce((a,b) -> a.getPosition() > b.getPosition()? a : b).orElse(null);
	}

	public static EmbedBuilder getDefaultUserEmbed(IUser user) {
		return new EmbedBuilder().withAuthorName(user.getName()).withAuthorIcon(user.getAvatarURL());
	}

	public static EmbedBuilder getDefaultRoleEmbed(IUser user, IGuild guild) {
		return getDefaultUserEmbed(user).withColor(user.getColorForGuild(guild));
	}

	//-------------------------------		CHANNELS		--------------------------------

	public static IVoiceChannel getVoiceChannelForUser(IUser user, IGuild guild) {
		return guild.getVoiceChannels().stream().filter(v -> v.getUsersHere().contains(user)).findAny().orElseGet(null);
	}

	//-------------------------------	 	PERMS 		------------------------------------

	public static boolean doesPermSetMatch(EnumSet<Permissions> set, int perms) {
		return Permissions.generatePermissionsNumber(set) == perms;
	}

	public static Collection<? extends Permissions> getMissingPerms(Collection<? extends Permissions> set, IGuild g) {
		return getPermsForGuild(g).stream().filter(p -> !set.contains(p)).collect(Collectors.toSet());
	}

	public static EnumSet<Permissions> getPermsForGuild(IGuild g) {
		EnumSet<Permissions> l = EnumSet.noneOf(Permissions.class);
		g.getRolesForUser(g.getClient().getOurUser()).forEach(role -> role.getPermissions().forEach(l::add));
		return l;
	}

	//------------------------------- 		ERRORS 		------------------------------

	public static void dumpErrorToChannel(Exception e, IChannel channel) {
		DiscordHelper.TimedMsgRemover.delete(channel.sendMessage(Messages.getString("DiscordHelper.0"))); //$NON-NLS-1$
		DiscordHelper.splitToMessages(dump(e), 2000, "```", "```", out -> RequestBuffer.request(() -> DiscordHelper.TimedMsgRemover.delete(channel.sendMessage(out)))); //$NON-NLS-1$ //$NON-NLS-2$
	}

	public static String dump(Exception e) {
		try {
			File f = new File("CrashDump.err"); //$NON-NLS-1$
			if(!f.exists())
				f.createNewFile();
			try(PrintStream p = new PrintStream(new FileOutputStream(f))) {
				e.printStackTrace(p);
			}
			String out = Files.readAllLines(f.toPath()).stream().reduce((a,b) -> a + '\n' + b).orElseGet(() -> Messages.getString("DiscordHelper.1")); //$NON-NLS-1$
			f.delete();
			return out;
		} catch(Exception e1) {
			return Messages.getString("DiscordHelper.1"); //$NON-NLS-1$
		}
	}

	//-----------------------------		UTILITY		----------------------------

	private static final Timer t = new Timer("Do Later"); //$NON-NLS-1$
	public static void doLater(Runnable r) { doLater(r,10); }
	public static void doLater(Runnable r, long delay) {
		t.schedule(new TimerTask() {@Override public void run() { r.run(); }}, delay);
	}

	public static void buffer(IVoidRequest... requests) {
		for(IVoidRequest request : requests)
			RequestBuffer.request(request);
	}

	public static void bufferOrdered(IVoidRequest... requests) throws InterruptedException, TimeoutException {
		for(IVoidRequest request : requests)
			RequestBuffer.request(request).get(1, TimeUnit.MINUTES);
	}

	public static BufferedImage getAvatar(IUser user) throws IOException {return getDiscordImage(new URL(user.getAvatarURL()));}
	public static BufferedImage getDiscordImage(URL url) throws IOException {
		HttpURLConnection con = (HttpURLConnection)url.openConnection();
		con.setRequestProperty(
				"User-Agent", //$NON-NLS-1$
				"Mozilla/5.0 (Macintosh; Intel Mac OS X 10_7_5) AppleWebKit/537.31 (KHTML, like Gecko) Chrome/26.0.1410.65 Safari/537.31"); //$NON-NLS-1$
		return ImageIO.read(con.getInputStream());
	}

	public static <T> TimeFilterSelector<T> getFilterForGuild(IGuild g, String name) {
		return getFilterForGuild(g,name,TimedEvent.MINUTE * 15);
	}

	@SuppressWarnings("unchecked")
	public static <T> TimeFilterSelector<T> getFilterForGuild(IGuild g, String name, long time) {
		ConcurrentHashMap<String, Object> data = Bot.getData().getDataForGuild(g).getTempData();
		if(!data.containsKey(name))
			data.put(name, new TimeFilterSelector<T>(time));
		Object j = data.get(name);
		return j instanceof TimeFilterSelector<?>? (TimeFilterSelector<T>) j : null;
	}

	//-----------------------------		UTILITY CLASSES		----------------------------

	public static final class TimedMsgRemover {
		public static final long DEFAULT_TIME = 15_000L;
		private static final ConcurrentHashMap<IMessage, Long> data = new ConcurrentHashMap<>();

		static {
			Timer t = new Timer("TimedMsgRemover"); //$NON-NLS-1$
			t.scheduleAtFixedRate(new TimerTask() {
				@Override
				public void run() {
					try {
						List<Entry<IMessage, Long>> i = data.entrySet().stream().filter(
								entry -> System.currentTimeMillis() - entry.getValue() > 0).collect(Collectors.toList());
						i.forEach(e -> RequestBuffer.request(() -> {
							e.getKey().delete();
							data.remove(e);
						}));
					} catch(Exception e) {
						pushbullet().push(new SendableNotePush("Error in message remover",DiscordHelper.dump(e))); //$NON-NLS-1$
					}
				}
			}, 1000,1000);
			onUnload(() -> {
				t.cancel();
				LOGGER.info("[TimedMsgRemover]: removing queued messages.."); //$NON-NLS-1$
				data.keySet().stream().forEach(msg -> RequestBuffer.request(msg::delete).get());
			});
		}

		public static void delete(IMessage msg, long time) {
			data.put(msg, System.currentTimeMillis() + time);
		}

		public static void delete(IMessage msg) {
			delete(msg,DEFAULT_TIME);
		}
	}

	public static final class TypingStatus {
		private static final ConcurrentHashMap<IChannel,List<UID>> data = new ConcurrentHashMap<>();

		public static void setTyping(IChannel c, UID id, boolean status) {
			if(status) {
				data.putIfAbsent(c, new ArrayList<>());
				List<UID> l = data.get(c);
				if(!l.contains(id)) {
					l.add(id);
					c.setTypingStatus(true);
				}
			} else {
				List<UID> l = data.get(c);
				if(l != null && l.contains(id)) {
					l.remove(id);
					if(l.isEmpty()) {
						data.remove(c);
						c.setTypingStatus(false);
					}
				}
			}
		}
	}

	public static final class BannedBotUsers {
		private static final String SQL_BAN_USER = "INSERT INTO `BanList` (`ID`, `Reason`, `Date`, `Creator`) VALUES (?, ?, ?, ?)"; //$NON-NLS-1$
		private static final String SQL_IS_BANNED = "SELECT * FROM BanList"; //$NON-NLS-1$
		private static final String SQL_UNBAN_USER = "DELETE FROM `BanList` WHERE `ID` LIKE ?"; //$NON-NLS-1$
		private static PreparedStatement SQL_BAN_USER_STATEMENT, SQL_UNBAN_USER_STATEMENT;
		private static final Map<Long,Ban> bans = new HashMap<>();

		private static void init() {
			try {
				LOGGER.debug("[BannedBotUsers]: Creating SQL statements..."); //$NON-NLS-1$
				SQL_BAN_USER_STATEMENT = Bot.SQL().prepareStatement(SQL_BAN_USER);
				SQL_UNBAN_USER_STATEMENT = Bot.SQL().prepareStatement(SQL_UNBAN_USER);
			} catch(Exception e) {
				pushbullet().push(new SendableNotePush("Error in BannedUsers",DiscordHelper.dump(e))); //$NON-NLS-1$
				e.printStackTrace();
			}

			LOGGER.debug("[BannedBotUsers]: Getting ban list.."); //$NON-NLS-1$
			try(Statement st = Bot.SQL().createStatement()) {
				try(ResultSet rs = st.executeQuery(SQL_IS_BANNED)) {
					while(rs.next()) {
						Ban b = new Ban(rs.getLong(1),rs.getString(2),rs.getDate(3),rs.getString(4));
						bans.put(b.id, b);
					}
				}
			} catch (SQLException e) {
				pushbullet().push(new SendableNotePush("Error in BannedUsers",DiscordHelper.dump(e))); //$NON-NLS-1$
				e.printStackTrace();
			}
		}

		public static boolean ban(IUser user, String reason, IUser creator) {
			if(!user.equals(user.getClient().getApplicationOwner())) {
				PreparedStatement st = SQL_BAN_USER_STATEMENT;
				Date d = new Date(System.currentTimeMillis());
				Ban b = new Ban(user.getLongID(),reason,d,creator.getName());
				try {
					LOGGER.debug("[BannedBotUsers]: Updating ban list..."); //$NON-NLS-1$
					st.setString(1, user.getStringID());
					st.setString(2, reason);
					st.setDate(3, d);
					st.setString(4, creator.getName());
					st.executeUpdate();
					bans.put(user.getLongID(), b);
				} catch(SQLException e) {
					pushbullet().push(new SendableNotePush("Error in BannedUsers",DiscordHelper.dump(e))); //$NON-NLS-1$
					e.printStackTrace();
				}
				return true;

			}
			return false;
		}

		public static boolean unban(IUser user) {
			PreparedStatement st = SQL_UNBAN_USER_STATEMENT;
			try {
				LOGGER.debug("[BannedBotUsers]: Updating ban list..."); //$NON-NLS-1$
				st.setString(1, user.getStringID());
				st.executeUpdate();
				bans.remove(user.getLongID());
				return true;
			} catch (SQLException e) {
				pushbullet().push(new SendableNotePush("Error in timed event",DiscordHelper.dump(e))); //$NON-NLS-1$
				e.printStackTrace();
				return false;
			}
		}

		public static Ban isBanned(IUser user) {
			return bans.get(user.getLongID());
		}

		public static final class Ban {
			public final long id;
			public final String creator, reason;
			public final Date date;

			public Ban(long id, String reason, Date date, String creator) {
				this.id = id;
				this.reason = reason;
				this.date = date;
				this.creator = creator;
			}
		}
	}

	public static final class CoolDown {
		private static final ConcurrentHashMap<Long,Long> data = new ConcurrentHashMap<>();
		private static final int COOLDOWN = 3000;

		public static boolean valid(IUser user) {return valid(user,COOLDOWN);}
		public static boolean valid(IUser user, long time) {
			long id = user.getLongID();
			if(data.containsKey(id)) {
				long t = data.get(id);
				if(System.currentTimeMillis() - t < time)
					return false;
			}
			data.put(id,System.currentTimeMillis());
			return true;
		}
	}

	@SuppressWarnings("unused")
	@EventSubscriber
	public static void dataReady(GuildDataReadyEvent e) {
		BannedBotUsers.init();
	}

	private static Consumer<Object> r = r -> {};
	private static void onUnload(Runnable run) {
		r = r.andThen(e -> run.run());
	}
	/**
	 * Unloads utilities<br>
	 * <b>NOT TO BE USED BY OTHER CLASSES<b>
	 */
	public static void unload() {
		r.accept(null);
	}

	//	public static final class AudioManager {
	//		private static final ConcurrentHashMap<Long,AudioThread> players = new ConcurrentHashMap<>();
	//		
	//		public static AudioThread getAudioThreadForGuild(IGuild guild) {
	//			long id = guild.getLongID();
	//			
	//			if(players.containsKey(id))
	//				return players.get(id);
	//			
	//			AudioThread th = new AudioThread(guild);
	//			players.put(id, th);
	//			return players.get(id);
	//		}
	//		
	//		private static class AudioThread {
	//			private boolean run = true;
	//			private final IGuild guild;
	//			
	//			public AudioThread(IGuild guild) {
	//				this.guild = guild;
	//			}
	//			
	//			public void start(IVoiceChannel channel) {
	//				
	//			}
	//		}
	//	}
}
