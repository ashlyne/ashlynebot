package net.foxgenesis.util.helper;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.nio.file.Files;
import java.util.List;
import java.util.function.Consumer;

@SuppressWarnings("nls")
public final class FileHelper {
	public static void writeToFile(File f, Consumer<BufferedWriter> c) throws IOException {
		try(BufferedWriter w = new BufferedWriter(new FileWriter(f))) {
			c.accept(w);
		}
	}
	
	public static void readLinesFromFile(File f, Consumer<String> c) throws IOException {
		try (BufferedReader r = Files.newBufferedReader(f.toPath())) {
			String line = "";
			while((line = r.readLine()) != null)
				c.accept(line);
		}
	}
	
	public static String readFromFile(File f) throws IOException {
		String output = "";
		try (BufferedReader r = Files.newBufferedReader(f.toPath())) {
			String line = "";
			while((line = r.readLine()) != null)
				output+=line;
		}
		return output;
	}
	
	public static List<String> readLinesFromFile(File f) throws IOException {
		return Files.readAllLines(f.toPath());
	}
	
	
	public static void saveToFile(File f, Consumer<ObjectOutputStream> c) throws IOException {
		try(ObjectOutputStream w = new ObjectOutputStream(new FileOutputStream(f))) {
			c.accept(w);
		}
	}
	
	public static void loadFromFile(File f, Consumer<ObjectInputStream> c) throws IOException {
		try(ObjectInputStream w = new ObjectInputStream(new FileInputStream(f))) {
			c.accept(w);
		}
	}
}
