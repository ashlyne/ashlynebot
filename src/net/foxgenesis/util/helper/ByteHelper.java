package net.foxgenesis.util.helper;

@SuppressWarnings("nls")
public final class ByteHelper {
	private static final long SIZE_KB = 1024;
	private static final long SIZE_MB = SIZE_KB * 1024;
	private static final long SIZE_GB = SIZE_MB * 1024;
	
	public static String toSizeString(long bytes) {
		String output = "";
		if(bytes >= SIZE_GB) {
			output = (bytes / SIZE_GB) + "GB";
			bytes %= SIZE_GB;
		}
		if(bytes > SIZE_MB) {
			output = output + " " +  (bytes / SIZE_MB) + "MB";
			bytes %= SIZE_MB;
		}
		if(bytes > SIZE_KB) {
			output = output + " " + (bytes / SIZE_KB) + "KB";
			bytes %= SIZE_KB;
		}
		return bytes > 0? output + " " + bytes + "bytes" : output;
	}
	
	public static String toShortSizeString(long bytes, int places) {
		if(bytes >= SIZE_GB) 
			return String.format("%." + places + "fGB",((double)bytes / SIZE_GB));
		else if(bytes >= SIZE_MB) 
			return String.format("%." + places + "fMB",((double)bytes / SIZE_MB));
		else if(bytes >= SIZE_KB) 
			return String.format("%." + places + "fKB",((double)bytes / SIZE_KB));
		return bytes + " bytes";
	}
	
	public static String toShortSizeString(long bytes) {
		return toShortSizeString(bytes,2);
	}
}
