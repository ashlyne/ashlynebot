package net.foxgenesis.util.helper;

import org.apache.http.HttpResponse;

@SuppressWarnings("nls")
public final class AnimePlanetHelper {
	private static final String PROFILE_URL = "https://www.anime-planet.com/users/";

	public static final AnimeProfile getProfile(String user) {
		try {
			HttpResponse res = WebHelper.sendGet(PROFILE_URL + user);
			if(res.getStatusLine().getStatusCode() != 200)
				return null;
			
			String in = WebHelper.readInputStream(res);
			int index = in.indexOf("loa-labels pure-g\">");
			
			if(index != -1) {
				in = in.substring(index + ("loa-labels pure-g\">".length()));
				String[] l = in.split("<li class=\"pure-1-6\">",7);
				
				double hours = 0;
				for(int i=0; i<l.length; i++) {
					String e = l[i];
					if(e.isEmpty())
						continue;
					e = e.substring(0, e.indexOf('<'));
					int ll = Integer.parseInt(e);
					switch(i - 1) {
					case 0:
						hours += ll * 0.0166667;
						break;
					case 1:
						hours += ll;
						break;
					case 2:
						hours += ll * 24;
						break;
					case 3:
						hours += ll * 168;
						break;
					case 4:
						hours += ll * 730.001;
						break;
					case 5:
						hours+= ll * 8760.00240024;
						break;
					}
				}
				
				in = l[6];
				in = in.substring(in.indexOf("slCount") + "slCount".length());
				l = in.split("slCount", 6);
				
				int[] watches = new int[6];
				
				for(int i=0; i<l.length; i++) {
					String e = l[i];
					watches[i] = Integer.parseInt(e.substring(2,e.indexOf('<')));
				}
				in = l[5];
				in = in.substring(in.indexOf("totalEps") + "totalEps".length() + 2);
				int totalEp = Integer.parseInt(in.substring(0, in.indexOf('<')).replaceAll(",", ""));
				return new AnimeProfile(user,hours,watches,totalEp);
			}
		} catch(Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	public static final boolean isAnimeProfileValid(String user) {
		try {
			HttpResponse res = WebHelper.sendGet(PROFILE_URL + user);
			int code = res.getStatusLine().getStatusCode();
			return code == 200;
		} catch(Exception e) {
			return false;
		}
	}

	public static class AnimeProfile {
		public final String user;
		public final double hours;
		public final int watched,watching,wantToWatch,stalled,dropped,noWatch,totalEp;

		private AnimeProfile(String user, double hours, int[] w, int totalEp) {
			this.user = user;
			this.hours = hours;
			this.totalEp = totalEp;
			if(w.length != 6)
				throw new ArrayIndexOutOfBoundsException("Invalid length provided during construction");
			this.watched = w[0];
			this.watching = w[1];
			this.wantToWatch = w[2];
			this.stalled = w[3];
			this.dropped = w[4];
			this.noWatch = w[5];
		}
	}
}
