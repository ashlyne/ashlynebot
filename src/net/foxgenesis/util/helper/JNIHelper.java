package net.foxgenesis.util.helper;

import java.nio.file.Path;

@SuppressWarnings("nls")
public final class JNIHelper {
	public static void appendLibraryPath(Path p) {
		logPath();
		String libpath = System.getProperty("java.library.path");
    	libpath = libpath + ';' + p.toAbsolutePath();
    	System.out.println("[JNIHelper]: Appending to library path: " + p.toAbsolutePath());
    	System.setProperty("java.library.path",libpath);
    	logPath();
	}
	
	public static void setLibraryPath(Path p) {
		logPath();
    	System.out.println("[JNIHelper]: Setting the library path: " + p.toAbsolutePath());
    	System.setProperty("java.library.path",p.toAbsolutePath().toString());
    	logPath();
	}
	
	private static void logPath() {
		System.out.printf("[JNIHelper]: Current library path -> %s\n", System.getProperty("java.library.path"));
	}
}
