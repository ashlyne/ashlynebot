package net.foxgenesis.util.helper;

import java.util.regex.Pattern;

@SuppressWarnings("nls")
public final class StringHelper {
	public static final Pattern NON_ASCII = Pattern.compile("[^\\x00-\\x7F].+");
	public static final Pattern NON_UNICODE = Pattern.compile("\\p{C}");
	
	public static String removeAllNonAlphaNumeric(String in) {
		return in.replaceAll("[^A-Za-z0-9]", "");
	}
	
	public static String replaceAllWhiteSpace(String in) {
		return in.replaceAll("\t"," ").replaceAll("\n", "").replaceAll(" ", "_");
	}
	
	public static String removeNonNumbers(String in) {
		return in.replaceAll("[^0-9]", "");
	}
	
	public static String removeNonASCII(String in) {
		return in.replaceAll("[^\\x00-\\x7F]", "");
	}
	
	public static String removeNonAlphaNumbericBut(String in, String leave) {
		return in.replaceAll("[^A-Za-z0-9" + leave + "]", "");
	}
	
	public static boolean containsNonASCII(String in) {
		return NON_ASCII.matcher(in).matches() || NON_ASCII.matcher(in).find() || NON_UNICODE.matcher(in).matches() || NON_UNICODE.matcher(in).find() || in.contains("�");
	}
}
