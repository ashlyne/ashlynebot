package net.foxgenesis.util;

import java.awt.Color;
import java.awt.Graphics;

public final class TextGraphicUtil {
	@SuppressWarnings("unused")
	public static void draw3D(String in, int x, int y, Color top, Color side, Color main, Graphics g) {
		for (int i = 0; i < 5; i++) {
			g.setColor(top);
			g.drawString(in, east(x, i), north(south(y, i), 1));
			g.setColor(side);
			g.drawString(in, west(east(x, i), 1), south(y, i));
		}
		g.setColor(Color.yellow);
		g.drawString(in, east(x, 5), south(y, 5));
	}
	
	public static void drawOutline(String in, int x, int y, Color main, Color stroke, Graphics g) {
		drawOutline(in,x,y,1,main,stroke,g);
	}
	public static void drawOutline(String in, int x, int y, int width, Color main, Color stroke, Graphics g) {
		g.setColor(stroke);
		g.drawString(in, west(x, width), north(y, width));
		g.drawString(in, west(x, width), south(y, width));
		g.drawString(in, east(x, width), north(y, width));
		g.drawString(in, east(x, width), south(y, width));
		g.setColor(main);
		g.drawString(in, x, y);
	}
	private static int north(int p, int distance) {
		return (p - distance);
	}
	private static int south(int p, int distance) {
		return (p + distance);
	}
	private static int east(int p, int distance) {
		return south(p,distance);
	}
	private static int west(int p, int distance) {
		return north(p,distance);
	}
}
