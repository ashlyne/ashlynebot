package net.foxgenesis.util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map.Entry;

public class MinMaxMed extends ArrayList<Number> {
	private static final long serialVersionUID = 4202513325948255148L;
	
	@Override
	public boolean add(Number t) {
		boolean b = super.add(t);
		if(!b)
			return false;
		this.sort(null);
		return true;
	}
	public double getAverage() {
		return (double) stream().reduce((a,b) -> a.doubleValue() + b.doubleValue()).orElse(0) / size();
	}
	
	public Number getMin() {
		return stream().reduce((a,b) -> Math.min(a.doubleValue(),b.doubleValue())).orElse(0);
	}
	
	public Number getMax() {
		return stream().reduce((a,b) -> Math.max(a.doubleValue(),b.doubleValue())).orElse(0);
	}
	
	public Entry<Number,Integer> getMost() {
		HashMap<Number,Integer> g = getKeyCount();
		return g.entrySet().stream().reduce((a,b) -> a.getValue() > b.getValue() ? a : b).get();
	}
	
	public HashMap<Number,Integer> getKeyCount() {
		HashMap<Number,Integer> g = new HashMap<>();
		forEach(n -> g.put(n, g.containsKey(n) ? g.get(n)+1 : 1));
		return g;
	}
	
	public Number getMid() {
		if(isEmpty())
			return 0;
		else if(size() == 1) 
			return get(0);
		else if((size() & 1) == 0) 
			return (get(size() / 2 - 1).doubleValue() + get(size() / 2).doubleValue()) / 2D;
		else 
			return get(size() / 2);
	}
	
	@Override
	public String toString() {
		return String.format("Min: %.2f\nMax: %.2f\nMean: %.2f\nMid: %.2f\nMost: %s\nCount: %s\n",  //$NON-NLS-1$
				getMin().doubleValue(), getMax().doubleValue(), getAverage(), getMid().doubleValue(), getMost(), getKeyCount());
	}
}
