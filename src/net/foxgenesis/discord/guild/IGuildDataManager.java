package net.foxgenesis.discord.guild;

import java.io.Serializable;

import sx.blah.discord.handle.obj.IGuild;

public interface IGuildDataManager {
	public IGuildData getDataForGuild(IGuild guild);
	public <T extends Serializable> ObjectKey<T> getObject(String name, T def);
	public boolean isReady();
}
