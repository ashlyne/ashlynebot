package net.foxgenesis.discord.guild;

import java.util.concurrent.ConcurrentHashMap;

import sx.blah.discord.handle.obj.IChannel;
import sx.blah.discord.handle.obj.IGuild;

public interface IGuildData {
	public IGuild getGuild();
	public JSONObjectAdv getConfig();
	public IChannel getDefaultChannel();
	public ConcurrentHashMap<String, Object> getTempData();
}
