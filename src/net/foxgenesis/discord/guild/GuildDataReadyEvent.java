package net.foxgenesis.discord.guild;

import sx.blah.discord.api.events.Event;
import sx.blah.discord.handle.obj.IGuild;

public class GuildDataReadyEvent extends Event {
	private final IGuildDataManager m;
	
	GuildDataReadyEvent(IGuildDataManager m) {
		this.m = m;
	}
	
	public IGuildData get(IGuild guild) {
		return this.m.getDataForGuild(guild);
	}
}
