package net.foxgenesis.discord.guild;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;

import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import sx.blah.discord.handle.obj.IChannel;
import sx.blah.discord.handle.obj.IGuild;

public class GuildData implements IGuildData {
	private static final Logger SQLLOGGER = LoggerFactory.getLogger("SQLInfo"); //$NON-NLS-1$
	
	private final IGuild guild;
	private final ConcurrentHashMap<String,Object> temp = new ConcurrentHashMap<>();
	private final ConcurrentHashMap<String,Object> storage = new ConcurrentHashMap<>();
	private JSONObjectAdv data;
	private final GuildDataManager m;

	//private static final String GET_CONFIG = "Select `Config` From `GuildData` where `GuildID` = %s"; //$NON-NLS-1$
	private static final String UPDATE_CONFIG_REMOVE = "JSON_REMOVE(Config,'$.\"%1$s\"')"; //$NON-NLS-1$
	private static final String UPDATE_CONFIG_SET = "JSON_SET(Config,'$.\"%1$s\"',%2$s)"; //$NON-NLS-1$
	private static final String UPDATE_COLUMN = "UPDATE `GuildData` SET `%1$s`= %2$s WHERE `GuildID` = %3$s"; //$NON-NLS-1$


	GuildData(IGuild guild, GuildDataManager m) {
		this.guild = guild;
		this.m = m;
	}

	@Override
	public IGuild getGuild() {
		return this.guild;
	}

	@Override
	public JSONObjectAdv getConfig() {
		return this.data;
	}

	@Override
	public ConcurrentHashMap<String, Object> getTempData() {
		return this.temp;
	}

	public StorageHandler getStorage(StorageKey s) {
		if(this.storage.containsKey(s.name))
			return new StorageHandler(this.storage.get(s.name), data -> {
				this.storage.put(s.name, data);
				pushUpdate(s.name,data instanceof JSONObject? "'" + data + "'" : data);	 //$NON-NLS-1$ //$NON-NLS-2$
			});
		return null;
	}

	private void pushUpdate(String name, Object data) {
		try(Statement st = this.m.sql.getConnection().createStatement()) {
			String query = String.format(UPDATE_COLUMN,name,data,this.guild.getStringID());
			SQLLOGGER.debug(GuildDataManager.UPDATE_MARKER,"PushUpdate -> %s",query); //$NON-NLS-1$
			st.execute(query);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	private void pushJSONUpdate(String name, Object data, boolean remove) {
		try(Statement st = this.m.sql.getConnection().createStatement()) {
			String query = String.format(UPDATE_COLUMN, "Config",remove ? String.format(UPDATE_CONFIG_REMOVE, name) : String.format(UPDATE_CONFIG_SET,name,data),this.guild.getStringID()); //$NON-NLS-1$
			SQLLOGGER.debug(GuildDataManager.UPDATE_MARKER,"PushUpdate -> %s",query); //$NON-NLS-1$
			try(ResultSet s = st.executeQuery(query)) {
				setData(s);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	synchronized void delete() throws Exception {
		this.temp.clear();
		this.storage.clear();
	}

	@Override
	public IChannel getDefaultChannel() {
		return this.guild.getChannelByID(this.data.getLong("default_channel")); //$NON-NLS-1$
	}

	private void setData(ResultSet s) throws SQLException {
		if(s.isBeforeFirst())
			if(!s.next())
				throw new NullPointerException("NO CONFIG IN RESULT SET!"); //$NON-NLS-1$
		final String jsonString = s.getString("Config"); //$NON-NLS-1$
		this.data = new JSONObjectAdv(jsonString,this::pushJSONUpdate);
		this.data.putIfAbsent("default_channel", this.getGuild().getDefaultChannel().getLongID()); //$NON-NLS-1$
		SQLLOGGER.debug("SetData <- " + jsonString); //$NON-NLS-1$
	}

	void setData(ResultSet s, List<StorageKey> st) throws SQLException {
		setData(s);
		st.forEach(storage -> {
			Object j = null;
			try {
				switch(storage.r) {
				case DATE:
					j = s.getDate(storage.name);
					break;
				case DEC: case DOUBLE_PRECISION:
					j = s.getDouble(storage.name);
					break;
				case FLOAT:
					j = s.getFloat(storage.name);
					break;
				case INT:
					j = s.getInt(storage.name);
					break;
				case VARCHAR: case CHAR:
					j = s.getString(storage.name);
					if(s.wasNull())
						j = ""; //$NON-NLS-1$
					break;
				case JSON:
					String out = s.getString(storage.name);
					j = new JSONObject(s.wasNull()? "{}":out); //$NON-NLS-1$
					break;
				case SMALLINT:
					j = s.getShort(storage.name);
					break;
				case TIME:
					j = s.getTime(storage.name);
					break;
				case TIMESTAMP:
					j = s.getTimestamp(storage.name);
					break;
				case BLOB:
					break;
				default:
					System.err.println("***** THIS DATATYPE IS NOT SUPPORTED! [" + storage.name + " | " + storage.type + "] *******"); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
					break;
				}
			} catch(Exception e) {
				e.printStackTrace();
			}
			if(j != null)
				this.storage.put(storage.name, j);
		});
	}
}
