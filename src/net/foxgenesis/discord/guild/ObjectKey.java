package net.foxgenesis.discord.guild;

import java.io.Serializable;
import java.util.function.Function;

import net.foxgenesis.util.function.TriConsumer;

public class ObjectKey<T extends Serializable> {
	public final long id;
	public final String name;
	private final TriConsumer<Long,String,T> update;
	private final Function<Long,T> read;
	public T object;
	
	public ObjectKey(long id, String name, T object, TriConsumer<Long,String,T> update, Function<Long,T> read) {
		this.id = id;
		this.name = name;
		this.object = object;
		this.update = update;
		this.read = read;
	}
	
	public synchronized void update() {
		this.update.accept(this.id, this.name, this.object);
	}
	
	public synchronized T read() {
		return this.object = this.read.apply(this.id);
	}
}
