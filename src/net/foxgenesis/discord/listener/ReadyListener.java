package net.foxgenesis.discord.listener;

import net.foxgenesis.ashlynebot.events.BotReadyEvent;

public interface ReadyListener {
	public void onReadyEvent(BotReadyEvent e);
}
