package net.foxgenesis.discord.timed;

import sx.blah.discord.handle.obj.IGuild;

@SuppressWarnings("nls")
public abstract class TimedEvent {
	public static final long MILLISECOND = 1, SECOND = 1_000, MINUTE = 60_000, HOUR = 3_600_000, DAY = 86_400_000;
	
	public final long delay, period;
	public final boolean repeat, useGuilds;
	private final String name;
	
	public TimedEvent(String name, long delay) {
		this(name,delay,delay,false,false);
	}
	
	public TimedEvent(String name, long delay, long period) {
		this(name,delay,period,false,false);
	}
	
	public TimedEvent(String name, long delay, long period, boolean repeat) {
		this(name,delay,period,repeat,false);
	}
	
	public TimedEvent(String name, long delay, long period, boolean repeat, boolean useGuilds) {
		this.delay = delay;
		this.period = period;
		this.repeat = repeat;
		this.useGuilds = useGuilds;
		this.name = name;
	}
	
	public abstract void run(IGuild guild);
	public abstract boolean shouldStop();
	public String getName() {return this.name;}
	
	@Override
	public String toString() {
		return String.format("TimedEvent{name=%s, delay=%s, period=%s, repeat=%b, useGuilds=%b}", 
				this.name, this.delay, this.period, this.repeat, this.useGuilds);
	}
}
