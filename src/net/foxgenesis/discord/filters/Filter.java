package net.foxgenesis.discord.filters;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target(value = {ElementType.TYPE})
@Retention(value = RetentionPolicy.RUNTIME)
public @interface Filter {
	public String name();
	public boolean defaultState() default false;
	public boolean allowBotMessages() default false;
}
