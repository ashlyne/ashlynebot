package net.foxgenesis.discord.filters;
import sx.blah.discord.handle.obj.IMessage;

public interface ITextFilter {
	public void handle(IMessage message);
	public boolean isAllowed(IMessage message);
}
