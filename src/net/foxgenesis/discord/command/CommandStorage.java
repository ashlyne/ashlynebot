package net.foxgenesis.discord.command;

import net.foxgenesis.ashlynebot.Bot;
import net.foxgenesis.discord.guild.GuildData;
import net.foxgenesis.discord.guild.SQLDataType;
import net.foxgenesis.discord.guild.StorageHandler;
import net.foxgenesis.discord.guild.StorageKey;
import sx.blah.discord.handle.obj.IMessage;

public abstract class CommandStorage implements IPlusCommand {
	public final StorageKey storage;
	
	public CommandStorage(String name, String type, SQLDataType r) {
		this.storage = new StorageKey(name,type,r);
	}
	
	public CommandStorage(String name, SQLDataType r) {
		this.storage = new StorageKey(name,r.name,r);
	}
	
	@Override
	public final void handleCommand(IMessage message, String inputs) throws Exception {
		GuildData d = (GuildData) Bot.getData().getDataForGuild(message.getGuild());
		StorageHandler hand = d.getStorage(this.storage);
		handleCommand(message,inputs,hand);
	}
	
	protected abstract void handleCommand(IMessage message, String inputs, StorageHandler handler) throws Exception;
}
