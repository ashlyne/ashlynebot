package net.foxgenesis.discord.command;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target(value = {ElementType.TYPE})
@Retention(value = RetentionPolicy.RUNTIME)
public @interface Command {
	public String name();
	public String help() default "No help provided";
	public String helpParams() default "";
	public boolean isHidden() default false;
	public boolean isOwnerCommand() default false;
	public boolean isSuperCommand() default false;
	public boolean allowAdminUse() default true;
	public boolean cooldown() default false;
	public long cooldownTime() default 3_000;
	public boolean isNSFW() default false;
}
