package net.foxgenesis.discord.command;

import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Supplier;

import sx.blah.discord.handle.obj.IMessage;

public abstract class GuildObjectCommand<E> extends CommandObjectStorage<ConcurrentHashMap<Long,E>>{

	private final Supplier<E> def;
	public GuildObjectCommand(String name, Supplier<E> def) {
		super(name, new ConcurrentHashMap<Long,E>());
		this.def = def;
	}

	@Override
	public final void handleCommand(IMessage message, String inputs) throws Exception {
		ConcurrentHashMap<Long,E> map = this.obj.object;
		long id = message.getGuild().getLongID();
		if(map.containsKey(id))
			handleCommand(message,inputs,map.get(id));
		else {
			E de = this.def.get();
			map.put(id, de);
			handleCommand(message,inputs,de);
		}
			
	}
	
	protected final void update() {
		this.obj.update();
	}
	
	protected abstract void handleCommand(IMessage message, String inputs, E object);
}
