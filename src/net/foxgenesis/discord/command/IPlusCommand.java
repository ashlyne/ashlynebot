package net.foxgenesis.discord.command;
import sx.blah.discord.handle.obj.IMessage;

@SuppressWarnings("nls")
public interface IPlusCommand {
	public void handleCommand(IMessage message, String inputs) throws Exception;
	public default String getDetailedHelpMessage() {
		return "No help provided. Sorry.";
	}
}
