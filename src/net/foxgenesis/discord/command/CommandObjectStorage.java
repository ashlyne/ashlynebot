package net.foxgenesis.discord.command;

import java.io.Serializable;

import net.foxgenesis.ashlynebot.Bot;
import net.foxgenesis.discord.guild.ObjectKey;

public abstract class CommandObjectStorage<T extends Serializable> implements IPlusCommand {
	protected final ObjectKey<T> obj;

	public CommandObjectStorage(String name, T def) {
		this.obj = Bot.getData().getObject(name, def);
	}
}
