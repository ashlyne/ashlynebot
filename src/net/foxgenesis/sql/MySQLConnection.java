package net.foxgenesis.sql;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

@SuppressWarnings("nls")
public class MySQLConnection {
	
	private Connection con;
	private final String url;
	private final String username,password;
	
	public MySQLConnection(String ip, int port, String username, String password) throws ClassNotFoundException, SQLException {
		this(ip,port,username,password,"");
	}
	
	public MySQLConnection(String ip, int port, String username, String password, String database) throws ClassNotFoundException, SQLException {
		Class.forName("com.mysql.jdbc.Driver");  
		this.url = String.format("jdbc:mysql://%s:%d/%s?autoreconnect=true",ip,port,database);
		this.username = username;
		this.password = password;
		setConnection();
	}
	
	public Connection getConnection() {
		try {
			if(this.con.isClosed()) {
				setConnection();
			}
		} catch (SQLException e) {}
		return this.con;
	}
	
	private synchronized void setConnection() throws SQLException {
		this.con=DriverManager.getConnection(this.url,this.username,this.password);
	}
}
